--[[--
v3DToFile.fuse
Writes PointCloud3D data from the Fusion 3D node-graph to a file.

Usage
Connect a PointCloud3D node's output connection directly to the v3DToFile node:
PointCloud3D.Output -> v3DToFile.Input

Todo
- Add export support for Camera3D, Transform3D, and Locator3D nodes
- Add a Scale control to adjust unit size from mm, cm, m, km
- Offset the exported point sample locations based upon the 3D node's built-in Transform control page translation, rotation, and scale settings
--]]--

-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "v3DToFile"
DATATYPE = "DataType3D"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_SinkTool, {
    REGID_DataType = DATATYPE,
    REGID_InputDataType = DATATYPE,
    REGS_Category = "Kartaverse\\Vonk Ultra\\3D\\IO",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Writes PointCloud3D data from the Fusion 3D node-graph to a file.",
    REGS_OpIconString = FUSE_NAME,
    REG_OpNoMask = true,
    REG_NoBlendCtrls = true,
    REG_NoObjMatCtrls = true,
    REG_NoMotionBlurCtrls = true,
    REG_NoPreCalcProcess = true,
    -- Should the current time setting be cached?
    REG_TimeVariant = true,
    REG_Unpredictable = true,
    -- Icon shown in the "Select Tool" dialog and the "Tile Picture"
    REGS_IconID = "Icons.Tools.Icons.RunCommand",
})

function Create()
    InInput = self:AddInput("Input 3D", "Input", {
        LINKID_DataType = "DataType3D",
        INPID_InputControl = "ImageControl",
        LINK_Main = 1,
    })

    ShowSource = self:AddInput("Show 3D Source", "ShowSource", {
        INPID_InputControl =  "ButtonControl",
        INP_External = false,
        BTNCS_Execute = "comp:SetActiveTool(tool.Input:GetConnectedOutput():GetTool())",
    })

    InSeparator = self:AddInput("UISeparator1", "UISeparator1", {
        IC_Visible = true,
        INPID_InputControl = "SeparatorControl",
        INP_External = false,
    })

    InFormat = self:AddInput('Point Cloud Format', 'Format', {
        INPID_InputControl = 'ComboControl',
        INP_DoNotifyChanged = true,
        INP_External = false,
        INP_Default = 0,
        INP_Integer = true,
        ICD_Width = 1,
        {CCS_AddString = 'XYZ ASCII (.xyz)'},
        {CCS_AddString = 'PLY ASCII (.ply)'},
        {CCS_AddString = 'PIXAR USDA ASCII (.usda)'},
        LINK_ForceSave = true,
    })

    InFile = self:AddInput("Filename" , "Filename" , {
        LINKID_DataType = "Text",
        INPID_InputControl = "FileControl",
        FC_IsSaver = true,
        FC_ClipBrowse = false,
        LINK_Visible = true,
        FCS_FilterString =  "XYZ (*.xyz)|*.xyz|PLY ASCII (*.ply)|*.ply|PIXAR USDA ASCII (*.usda)|*.usda|Text (*.txt)|Files (*.*)|*.*|",
        LINK_Main = 2
    })

    InSeparator = self:AddInput("UISeparator2", "UISeparator2", {
        IC_Visible = true,
        INPID_InputControl = "SeparatorControl",
        INP_External = false,
    })

    InShowInput = self:AddInput("Show 3D Input", "ShowInput", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 1.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    InShowFileInput = self:AddInput("Show File Input", "ShowFileInput", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    OutOutput = self:AddOutput("Output", "Output", {
        LINKID_DataType = "DataType3D",
        LINK_Main = 1,
    })
end

function NotifyChanged(inp, param, time)
    if inp == InShowInput then
        local visible
        if param.Value == 1.0 then visible = true else visible = false end

        InInput:SetAttrs({LINK_Visible = visible})
    end
    if inp == InShowFileInput then
        local visible
        if param.Value == 1.0 then visible = true else visible = false end

        InFile:SetAttrs({LINK_Visible = visible})
    end
end

function Process(req) 
    local inp = InInput:GetValue(req)
    local format = InFormat:GetValue(req).Value

    local rel_path = InFile:GetValue(req).Value
    local abs_path = self.Comp:MapPath(rel_path)

    local frame = req.Time

    -- Based upon Cryptomatte for Fusion 8 upstream connection scanning code
    local fusion = Fusion("localhost", 0, bmd.getappuuid())
    local comp = fusion.CurrentComp
    local tool = comp:FindTool(self.Name)
    local input = tool:FindMainInput(1)
    if input ~= nil then
        local input_tool = input:GetConnectedOutput():GetTool()
        if input_tool ~= nil then
            -- print("[Connection] " .. tostring(self.Name) .. "." .. tostring(input.Name) .. " -> " .. tostring(input_tool.Name))
            local script = [=[
-- Pixar USD ASCII Export Settings:
-- USD ASCII scene scale = CM
local metersPerUnit = 0.01
-- Maya default style Y-axis Up-coordinate system
local upAxis = 'Y'

local fusion = Fusion("localhost", 0, bmd.getappuuid())
local comp = fusion.CurrentComp

local nodeName = "]=] .. input_tool.Name .. [=["
local tool = comp:FindTool(nodeName)
local toolAttrs = tool:GetAttrs()
local nodeType = toolAttrs.TOOLS_RegID
local frame = "]=] .. frame .. [=["
local format = ]=] .. format .. [=[

local pointcloudFile = [[]=] .. abs_path .. [=[]]

local directory = pointcloudFile:match("(.*[/\\])")
if bmd.fileexists(directory) == false then
    bmd.createdir(directory)
    -- print('[Created Directory]', folder)
end

outFile, err = io.open(pointcloudFile,'w')
if err then
    print('[v3DToFile] [Error opening file for writing] ' .. tostring(pointcloudFile))
    return
end

-- print('[v3DToFile] [Frame] ' .. tostring(frame))
-- print('[v3DToFile] [Writing] ' .. tostring(nodeName) .. ' [Type] ' .. tostring(nodeType))
-- print('[v3DToFile] [File] "' .. tostring(pointcloudFile) .. '"')

-- print('[v3DToFile] [Format Number] "' .. tostring(format) .. '"')
if format == 0 then
    -- print('[v3DToFile] [Format] "XYZ ASCII (.xyz)"')
elseif format == 1 then
    -- print('[v3DToFile] [Format] "PLY ASCII (.ply)"')
    -- Write a ply ASCII header entry
    outFile:write('ply\n')
    outFile:write('format ascii 1.0\n')
    outFile:write('comment Created by Vonk Ultra\n')
    outFile:write('comment Created ' .. tostring(os.date('%Y-%m-%d %I:%M:%S %p')) .. '\n')
    outFile:write('obj_info Generated by Vonk Ultra!\n')
    outFile:write('element vertex ' .. tostring(vertexCount) .. '\n')
    outFile:write('property float x\n')
    outFile:write('property float y\n')
    outFile:write('property float z\n')
    outFile:write('end_header\n')
elseif format == 2 then
    -- print('[v3DToFile] [Format] "PIXAR USDA ASCII (.usda)"')
    -- Write a PIXAR USD ASCII header entry
    outFile:write('#usda 1.0\n')
    outFile:write('(\n')
    outFile:write('\tdefaultPrim = "persp"\n')
    outFile:write('\tdoc = """Generated from Composed Stage of root layer ' .. tostring(pointcloudFile) .. '"""\n')
    outFile:write('\tmetersPerUnit = ' .. tostring(metersPerUnit) .. '\n')
    outFile:write('\tupAxis = "' .. tostring(upAxis) .. '"\n')
    outFile:write(')\n')
    outFile:write('\n')
    outFile:write('def Xform "PointCloudGroup" (\n')
    outFile:write('\tkind = "assembly"\n')
    outFile:write(')\n')
    outFile:write('{\n')
end

local tbl = comp:CopySettings(tool)
-- print('[Node Settings Table]')
-- dump(tbl)
-- dump(tbl['Tools'])

if tbl and tbl['Tools'] and tbl['Tools'][nodeName] and tbl['Tools'][nodeName]['Positions'] then
    -- Grab the positions Lua table elements
    local positionsTable = tbl['Tools'][nodeName]['Positions'] or {}
    local positionsElements = tonumber(table.getn(positionsTable))
    -- dump(positionsTable)

    -- Handle array off by 1
    vertexCount = 0
    if positionsTable[0] then
        vertexCount = tonumber(positionsElements + 1)
    end

    -- Scan through the positions table
    for i = 0, positionsElements do
        -- Check if there are 5+ elements are in the positions table element. We only need 4 of those elements at this time.
        local tableElements = table.getn(positionsTable[i] or {})
        if tableElements >= 4 then
            local x, y, z, name = positionsTable[i][1], positionsTable[i][2], positionsTable[i][3], positionsTable[i][4]
            -- print('[' .. tostring(i + 1) .. '] [' .. tostring(name) .. '] [XYZ] ' .. tostring(x) .. ' ' .. tostring(y) .. ' ' .. tostring(z))
            if format == 0 then
                -- XYZ Point Cloud
                outFile:write(tostring(x) .. ' ' .. tostring(y) .. ' ' .. tostring(z) .. '\n')
            elseif format == 1 then
                -- PLY Point Cloud
                -- Add a trailing space before the newline character
                outFile:write(tostring(x) .. ' ' .. tostring(y) .. ' ' .. tostring(z) .. ' ' .. '\n')
            elseif format == 2 then
                -- USD ASCII Point Cloud
                outFile:write('\n')
                outFile:write('\tdef Xform "locator' .. tostring(i + 1) .. '"\n')
                outFile:write('\t{\n')
                outFile:write('\t\tdouble3 xformOp:translate = (' .. tostring(x) .. ', ' .. tostring(y) .. ', ' .. tostring(z) .. ')\n')
                outFile:write('\t\tuniform token[] xformOpOrder = ["xformOp:translate"]\n')
                outFile:write('\t}\n')
            else
                print('[v3DToFile] [Error Selecting Point Cloud Format]')
            end
        else
            print('[Error][PointCloud3D Positions] Not enough table elements. Only ' .. tostring(tableElements) .. ' were found. 5 are expected.')
        end
    end

    if format == 2 then
        -- Write out the USD ASCII footer
        outFile:write('}\n')
    end

    outFile:write('\n')
    outFile:close()
else
    print('[Node Settings Table] Empty Table')
end
]=]

            -- print(script)
            self.Comp:Execute(script)
        end
    end

    OutOutput:Set(req, inp)
end

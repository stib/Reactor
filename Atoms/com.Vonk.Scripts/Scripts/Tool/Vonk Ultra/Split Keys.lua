--[[--
Split Keys 2023-10-02 02.04 PM

This tool script automatically extracts Vonk data node key/value pairs into separate nodes.

The following fuse node types are supported by the Split Keys script:

CSV Data Nodes:
- vNumberFromCSV (Input Connection Only)
- vTextFromCSV (Input Connection Only)
- vTextFromFile (Output Connection Only as CSV)

ScriptVal Data Nodes:
- vScriptValViewer
- vScriptValGetToTable
- vScriptValGetElementToTable
- vScriptValDoString
- vScriptValDoStringMultiplex
- vScriptValFromMetadata
- vScriptValGetToText (Input Connection Only)
- vScriptValGetToNumber (Input Connection Only)
- vScriptValToText (Input Connection Only)
- vScriptValFromJSON (Output Connection Only)

OCA Data Nodes (Also ScriptVals):
- ocaInfo
- ocaLayer
- ocaMerge
- ocaScript
- ocaSaver
- ocaSwitch 
- ocaRender (Input Connection Only)
- ocaFrame (Output Connection Only)
- ocaStage (Output Connection Only)
- ocaText (Output Connection Only)
- ocaWireless (Output Connection Only)
- ocaLoader (Output Connection Only)

The following JSON node types are not supported (yet):
- vJSONGet
- vJSONFromFile
- vJSONGetElement
- vJSONFromNet
- vJSONDoString
- vJSONViewer
- vScriptValToJSON
- vArrayGet

Usage:
Select a CSV node like "vTextFromFile", "vNumberFromCSV", or "vTextFromCSV" in the flow. Then run the Split Keys script. A dialog will appear.

The "Split Keys At" combo box menu allows you to choose if the splitting happens to the Key/Value pair data stream present at the input connection or the output connection on the "Get" node.

The "Select All", "None", and "Invert" buttons allow you to toggle the active/inactive state checkbox for the Keys that will be processed. This allows you to narrow things down to the exact data channels you want to work with.

The Tree view lists all of the keys that can be accessed by the selected node. There is a checkbox in the first column of the Tree that allows you to select which keys are used by the script. You can also click on the text for the Key name to toggle the checkbox.

The "Node Type" column text for each entry is clickable. This allows you to customize the data type that will be used when a specific Vonk "Get" node is generated. For CSV nodes this allows you to select if you want to access a text or number data type output.

The "Node Build Direction" control allows you to define if you want the new nodes to be inserted in a specific position in the flow relative to the selected node. Options are "Down", "Left", "Up", or "Right".

Clicking the "OK" button will cause the selected keys to be extracted via individual "Get" based Vonk nodes that are added to the flow. After the nodes are added to the comp, they will be set as the active selection in the flow so you can drag the nodes where you want the nodes to be positioned.

Dev Todo:
- Add tooltip text
- Add JSON/vArray support
- For JSONs that have several numbered table fields, add a vJSONGetElement node to allow reading those elements
- Detect a ScriptVal Lua table based 4x4 transform matrix and insert a vonk matrix node to that output
- Enable uiTree extend selection so clicking on the "node type" heading batch toggles the setting for all selected rows.

--]]--

-- Limit the maximum number of nodes to be generated during a split operation
-- split_limit = 64
split_limit = 200

tool = tool or comp.ActiveTool

local vtextutils = require("vtextutils")
local jsonutils = require("vjsonutils")
local arrayutils = require("varrayutils")

function SplitKeys(array, split, buildDirection)
	print("\n--------------------------------------------------------------------------------------------")
	print("Vonk | Split Keys")
	print("--------------------------------------------------------------------------------------------\n")
	print("[Split Keys At] ", split)
	--dump(array)

	local array_size = table.getn(array)
	local selectionList = {}
	
	-- Read the selected node's position in the flow view
	local flow = comp.CurrentFrame.FlowView
	local originX, originY = flow:GetPos(tool)
	local shiftX, shiftY = 0,0
	
	-- buildDirection 0 = down, 1 = right, 2 = up, 3 = left
	--local buildDirection = 0

	comp:StartUndo("Split Keys")
	if tool.ID:match("^Fuse.vNumberFromCSV") or tool.ID:match("^Fuse.vTextFromCSV") or tool.ID:match("^Fuse.vTextFromFile") then
		if array_size >= split_limit then
			print(string.format("[Split Keys] [%d Key Splitting Limit Reached] [Keys Present] %d", split_limit, array_size))
		else
			for k, v in ipairs(array) do
				-- Deselect all nodes
				comp.CurrentFrame.FlowView:Select()

				-- Create a node for each key/value pair
				if buildDirection == 0 then
					-- Down
					shiftX = 0
					shiftY = shiftY + 1
				elseif buildDirection == 1 then
					-- Right
					shiftX = shiftX + 1
					shiftY = 0
				elseif buildDirection == 2 then
					-- Up
					shiftX = 0
					shiftY = shiftY - 1
				elseif buildDirection == 3 then
					-- Left
					shiftX = shiftX - 1
					shiftY = 0
				end
				local newTool = comp:AddTool(v.node, originX + shiftX, originY + shiftY)
				-- local newTool = comp:AddTool(v.node, -32768, -32768)

				-- Track the newly created tools
				table.insert(selectionList, newTool)

				-- Set the keys
				newTool:SetInput("Column", tonumber(v.value), comp.CurrentTime)

				local ignore = 1 
				if not tool.ID:match("^Fuse.vTextFromFile") then
					tool:GetInput("IgnoreHeaderRow", comp.CurrentTime)
				end
				newTool:SetInput("IgnoreHeaderRow", ignore, comp.CurrentTime)

				-- Rename the node
				local NewName = "csv_" .. tostring(v.key or "")
				newTool:SetAttrs({TOOLS_Name = NewName})

				-- Connect the nodes
				-- Input re-connection code from hos_Macro2Group.lua
				if split == "Output Connection" then
					-- Output Connection (vTextFromFile)
					newTool:ConnectInput("Input", tool["Output"])
				else
					-- Input Connection
						for j, input in pairs(tool:GetInputList()) do
						if input:GetAttrs().INPB_Connected then
							if newTool:GetInputList()[j] ~= nil then
								if newTool:GetInputList()[j]:GetAttrs().INPB_Connected then
								else
									newTool:GetInputList()[j]:ConnectTo(input:GetConnectedOutput())
									if(newTool:GetInputList()[j]:GetAttrs().INPB_Connected) then
										--print(string.format("[Split Keys] [Input Reconnected] %30s.%s", newTool.Name, input:GetConnectedOutput().Name))
									end
								end
							else
								print(string.format("[Split Keys] [Input Reconnection Error] %30s.%s", newTool.Name, input:GetConnectedOutput().Name))
							end
						end
					end
				end
				print(string.format("[Node Name] %20s [Reg ID] %15s [Heading Value] %10s (%s)", newTool.Name, newTool.ID , v.key, tonumber(v.value)))
			end
		end
	elseif tool.ID:match("^Fuse.vScriptValGetToText") or tool.ID:match("^Fuse.vScriptValGetToTable") or tool.ID:match("^Fuse.vScriptValGetToNumber") or tool.ID:match("^Fuse.vScriptValGetElementToTable") or tool.ID:match("^Fuse.vScriptValViewer") or tool.ID:match("^Fuse.vScriptValFromJSON") or tool.ID:match("^Fuse.vScriptValToText") or tool.ID:match("^Fuse.vScriptValDoString") or tool.ID:match("^Fuse.vScriptValDoStringMultiplex") or tool.ID:match("^Fuse.vScriptValFromMetadata") or tool.ID:match("^Fuse.ocaFrame") or tool.ID:match("^Fuse.ocaLayer") or tool.ID:match("^Fuse.ocaStage") or tool.ID:match("^Fuse.ocaText") or tool.ID:match("^Fuse.ocaSwitch") or tool.ID:match("^Fuse.ocaWireless") or tool.ID:match("^Fuse.ocaLoader") or tool.ID:match("^Fuse.ocaSaver") or tool.ID:match("^Fuse.ocaInfo") or tool.ID:match("^Fuse.ocaMerge") or tool.ID:match("^Fuse.ocaRender") or tool.ID:match("^Fuse.ocaScript") then
		if array_size >= split_limit then
			print(string.format("[Split Keys] [%d Key Splitting Limit Reached] [Keys Present] %d", split_limit, array_size))
		else
			-- Process Key/Value Pairs
			for k, v in ipairs(array) do
				-- Deselect all nodes
				comp.CurrentFrame.FlowView:Select()

				-- Create a node for each key/value pair
				if buildDirection == 0 then
					-- Down
					shiftX = 0
					shiftY = shiftY + 1
				elseif buildDirection == 1 then
					-- Right
					shiftX = shiftX + 1
					shiftY = 0
				elseif buildDirection == 2 then
					-- Up
					shiftX = 0
					shiftY = shiftY - 1
				elseif buildDirection == 3 then
					-- Left
					shiftX = shiftX - 1
					shiftY = 0
				end
				local newTool = comp:AddTool(v.node, originX + shiftX, originY + shiftY)

				-- Track the newly created tools
				table.insert(selectionList, newTool)

				-- Set the key
				newTool:SetInput("Key", v.key, comp.CurrentTime)

				-- Rename the node
				local NewName = "scriptval_" .. tostring(v.key or "")
				if not (newTool.ID:match("^Fuse.vScriptValViewer") or newTool.ID:match("^Fuse.vScriptValGetElementToTable") or newTool.ID:match("^Fuse.vScriptValToText") or newTool.ID:match("^Fuse.vScriptValDoString") or newTool.ID:match("^Fuse.vScriptValDoStringMultiplex") or newTool.ID:match("^Fuse.vScriptValFromMetadata") or newTool.ID:match("^Fuse.ocaFrame") or newTool.ID:match("^Fuse.ocaLayer") or newTool.ID:match("^Fuse.ocaStage") or newTool.ID:match("^Fuse.ocaText") or newTool.ID:match("^Fuse.ocaSwitch") or newTool.ID:match("^Fuse.ocaWireless") or newTool.ID:match("^Fuse.ocaLoader") or newTool.ID:match("^Fuse.ocaSaver") or newTool.ID:match("^Fuse.ocaInfo") or newTool.ID:match("^Fuse.ocaMerge") or newTool.ID:match("^Fuse.ocaRender") or newTool.ID:match("^Fuse.ocaSwitch") or newTool.ID:match("^Fuse.ocaScript")) then
					newTool:SetAttrs({TOOLS_Name = NewName})
				end

				-- Connect the nodes
				-- Input re-connection code from hos_Macro2Group.lua
				if split == "Output Connection" then
					-- Output Connection
					if tool.ID:match("^Fuse.vScriptValFromJSON") then
						newTool:ConnectInput("ScriptVal", tool["ScriptVal"])
					else
						newTool:ConnectInput("ScriptVal", tool["Output"])
					end
				else
					-- Input Connection
						for j, input in pairs(tool:GetInputList()) do
						if input:GetAttrs().INPB_Connected then
							if newTool:GetInputList()[j] ~= nil then
								if newTool:GetInputList()[j]:GetAttrs().INPB_Connected then
								else
									newTool:GetInputList()[j]:ConnectTo(input:GetConnectedOutput())
									if(newTool:GetInputList()[j]:GetAttrs().INPB_Connected) then
										--print(string.format("[Split Keys] [Input Reconnected] %30s.%s", newTool.Name, input:GetConnectedOutput().Name))
									end
								end
							else
								print(string.format("[Split Keys] [Input Reconnection Error] %30s.%s", newTool.Name, input:GetConnectedOutput().Name))
							end
						end
					end
				end
				print(string.format("[Node Name] %30s [Value] %30s [Reg ID] %30s", newTool.Name, v.key, newTool.ID))
			end
		end
	end

	-- Select the Nodes
	for i, sel in pairs(selectionList) do
		comp.CurrentFrame.FlowView:Select(sel)
	end

	print("--------------------------------------------------------------------------------------------\n")
	comp:EndUndo()
end

function RefreshTree(itm, split)
	itm.Tree.UpdatesEnabled = false
	itm.Tree.SortingEnabled = false

	itm.Tree:Clear()

	-- Add a header row.
	hdr = itm.Tree:NewItem()
	hdr.Text[0] = ""
	hdr.Text[1] = "Key"
	hdr.Text[2] = "Node Type"
	itm.Tree:SetHeaderItem(hdr)

	-- Number of columns in the Tree list
	itm.Tree.ColumnCount = 3

	-- Resize the Columns
	itm.Tree.ColumnWidth[0] = 50
	itm.Tree.ColumnWidth[1] = 150
	itm.Tree.ColumnWidth[2] = 250

	-- Add an new row entries to the list
	if tool.ID:match("^Fuse") then
		if tool.ID:match("^Fuse.vNumberFromCSV") or tool.ID:match("^Fuse.vTextFromCSV") or tool.ID:match("^Fuse.vTextFromFile") then
			-- Process CSV data
			local data
			local col = 1
			local ignore = 1

			if split == "Output Connection" then
				-- Output Connection (for vTextFromFile)
				data = tool["Output"][comp.CurrentTime] or {}
			else
				data = tool:GetInput("Input", comp.CurrentTime)
				col = tool:GetInput("Column")
				ignore = tool:GetInput("IgnoreHeaderRow", comp.CurrentTime)
			end

			local id = tool.ID
			if tool.ID == "Fuse.vTextFromFile" then
				id = "Fuse.vNumberFromCSV"
			end

			local row = 1
			local pattern = "(.-),"

			-- A lazy workaround for greedy Lua pattern matching of the last comma in a CSV line
			local line = tostring(textutils.ReadLineIgnoreComments(data, row, "#")) .. ","

			local array = textutils.split(line, pattern)
			local array_size = table.getn(array)
			
			if array_size >= split_limit then
				print(string.format("[Split Keys] [%d Key Splitting Limit Reached] [Keys Present] %d", split_limit, array_size))
			else
				for key, value in ipairs(array) do
					it = itm.Tree:NewItem()
					it.Text[1] = value
					it.Text[2] = id
					it:SetData(0, "UserRole", key)
					it.CheckState[0] = "Checked"
					it.Flags = {ItemIsSelectable = true, ItemIsEnabled = true, ItemIsUserCheckable = true}
					itm.Tree:AddTopLevelItem(it)
				end
			end
		--elseif tool.ID:match("^Fuse.vScriptValGetToText") or tool.ID:match("^Fuse.vScriptValGetToTable") or tool.ID:match("^Fuse.vScriptValGetToNumber") then
		elseif tool.ID:match("^Fuse.vScriptValGetToText") or tool.ID:match("^Fuse.vScriptValGetToTable") or tool.ID:match("^Fuse.vScriptValGetToNumber") or tool.ID:match("^Fuse.vScriptValGetElementToTable") or tool.ID:match("^Fuse.vScriptValViewer") or tool.ID:match("^Fuse.vScriptValFromJSON") or tool.ID:match("^Fuse.vScriptValToText") or tool.ID:match("^Fuse.vScriptValDoString") or tool.ID:match("^Fuse.vScriptValDoStringMultiplex") or tool.ID:match("^Fuse.vScriptValFromMetadata") or tool.ID:match("^Fuse.ocaFrame") or tool.ID:match("^Fuse.ocaLayer") or tool.ID:match("^Fuse.ocaStage") or tool.ID:match("^Fuse.ocaText") or tool.ID:match("^Fuse.ocaSwitch") or tool.ID:match("^Fuse.ocaWireless") or tool.ID:match("^Fuse.ocaLoader") or tool.ID:match("^Fuse.ocaSaver") or tool.ID:match("^Fuse.ocaInfo") or tool.ID:match("^Fuse.ocaMerge") or tool.ID:match("^Fuse.ocaRender") or tool.ID:match("^Fuse.ocaScript") then
			-- Process ScriptVal data
			local tbl = {}

			local id = tool.ID
			if tool.ID:match("^Fuse.vScriptValViewer") or tool.ID:match("^Fuse.vScriptValGetElementToTable") or tool.ID:match("^Fuse.vScriptValFromJSON") or tool.ID:match("^Fuse.vScriptValDoString") or tool.ID:match("^Fuse.vScriptValDoStringMultiplex") or tool.ID:match("^Fuse.vScriptValFromMetadata") or tool.ID:match("^Fuse.ocaFrame") or tool.ID:match("^Fuse.ocaLayer") or tool.ID:match("^Fuse.ocaStage") or tool.ID:match("^Fuse.ocaText") or tool.ID:match("^Fuse.ocaSwitch") or tool.ID:match("^Fuse.ocaWireless") or tool.ID:match("^Fuse.ocaLoader") or tool.ID:match("^Fuse.ocaSaver") or tool.ID:match("^Fuse.ocaInfo") or tool.ID:match("^Fuse.ocaMerge") or tool.ID:match("^Fuse.ocaRender") or tool.ID:match("^Fuse.ocaRender") or tool.ID:match("^Fuse.ocaScript") then
				id = "Fuse.vScriptValGetToTable"
			end

			if tool.ID:match("^Fuse.vScriptValFromJSON") then
				-- Output Connection
				tbl = tool["ScriptVal"][comp.CurrentTime] or {}
			elseif (tool.ID:match("^Fuse.vScriptValDoStringMultiplex") or tool.ID:match("^Fuse.ocaMerge")) and split == "Input Connection" then
				tbl = tool:GetInput("ScriptVal1", comp.CurrentTime) or {}
			elseif tool.ID:match("^Fuse.ocaSwitch") and split == "Input Connection" then
				tbl = tool:GetInput("Input1", comp.CurrentTime) or {}
			elseif tool.ID:match("^Fuse.ocaStage") and split == "Input Connection" then
				tbl = tool:GetInput("layer1", comp.CurrentTime) or {}
			elseif tool.ID:match("^Fuse.ocaLayer") and split == "Input Connection" then
				tbl = tool:GetInput("frames", comp.CurrentTime) or {}
			elseif split == "Output Connection" then
				-- Output Connection
				tbl = tool["Output"][comp.CurrentTime] or {}
			elseif split == "Input Connection" then
				-- Input Connection
				tbl = tool:GetInput("ScriptVal", comp.CurrentTime) or {}
			else
				-- Fallback to the Input Connection
				tbl = tool:GetInput("ScriptVal", comp.CurrentTime) or {}
			end
			--dump(tbl)

			local lua_array = arrayutils(tbl)
			local table_keys = {}
			local array_keys = arrayutils(table_keys)

			local hasElement = 0
			for array_key in lua_array:Keys() do
				--print(array_key, " [Type] ", type(array_key))
				if type(array_key) == "number" then
					-- Is there a number datatype in the keys that would indicate a GetElement node is required?
					hasElement = hasElement + 1
					if hasElement <= 1 then
						--array_keys:Push("#")
						array_keys:Push(tostring(array_key))
					end
				else
					array_keys:Push(array_key)
				end
			end
			local array_size = table.getn(array_keys)

			-- Sort the key alphabetically
			table.sort(array_keys)
			--dump(array_keys)

			if array_size >= split_limit then
				print(string.format("[Split Keys] [%d Key Splitting Limit Reached] [Keys Present] %d", split_limit, array_size))
			else
				-- Process Key/Value Pairs
				for key, value in ipairs(array_keys) do
					it = itm.Tree:NewItem()
					it.Text[1] = value
					if string.match(value, "^%d+") then
					--if string.match(value, "%d+") and split == "Input Connection" then
						it.Text[2] = "Fuse.vScriptValGetElementToTable"
					else
						it.Text[2] = id
					end

					it:SetData(0, "UserRole", key)
					it.CheckState[0] = "Checked"
					it.Flags = {ItemIsSelectable = true, ItemIsEnabled = true, ItemIsUserCheckable = true}
					itm.Tree:AddTopLevelItem(it)
				end
			end
		end
	end

	itm.Tree.SortingEnabled = true
	itm.Tree.UpdatesEnabled = true
end

function Main()
	if not tool then
		print("[Split Keys] Please select a node before running this script.")
		return
	end

	local ui = fu.UIManager
	local disp = bmd.UIDispatcher(ui)
	local width,height = 520,400

	win = disp:AddWindow({
		ID = "SplitKeys",
		TargetID = "SplitKeys",
		WindowTitle = "Split Keys",
		Geometry = {100, 100, width, height},
		Spacing = 10,

		ui:VGroup{
			ID = "root",
			-- Add your GUI elements here:
			ui:HGroup{
				Weight = 0.01,
				ui:Label{
					Weight = 0.01,
					ID = "SplitAtLabel",
					Text = "Split Keys At",
					Alignment = {
						-- AlignHCenter = true,
						AlignTop = true,
					},
				},
				ui:ComboBox{
					ID = "SplitAtCombo",
					Text = "Split At",
				},
			},
			ui:HGroup{
				Weight = 0.01,
				ui:Label{
					ID = "TitleLabel",
					Text = "Select the keys you would like to include. Click on Node Type to cycle the node generated.",
					Alignment = {
						AlignHCenter = true,
						AlignTop = true,
					},
				},
			},
			ui:HGroup{
				Weight = 0.01,
				ui:Button{
					Weight = 0.01,
					ID = "SelectAllButton",
					Text = "All",
					MinimumSize = {40, 24},
				},
				ui:Button{
					Weight = 0.01,
					ID = "SelectNoneButton",
					Text = "None",
					MinimumSize = {40, 24},
				},
				ui:Button{
					Weight = 0.01,
					ID = "SelectInvertButton",
					Text = "Invert",
					MinimumSize = {40, 24},
				},
			},
			ui:HGroup{
				Weight = 1,
				ui:Tree{
					ID = "Tree",
					SortingEnabled = true,
					--HeaderHidden = true,
					--SelectionMode = "ExtendedSelection",
					Events = {
						CurrentItemChanged = true,
						ItemChanged = true,
						ItemClicked = true
					},
				},
			},
			ui:HGroup{
				Weight = 0.01,
				ui:Label{
					Weight = 0.01,
					ID = "BuildDirectionLabel",
					Text = "Node Build Direction",
					Alignment = {
						-- AlignHCenter = true,
						AlignTop = true,
					},
				},
				ui:ComboBox{
					ID = "BuildDirectionCombo",
					Text = "Node Build Direction",
				},
			},
			ui:HGroup{
				Weight = 0.01,
				ui:Button{
					ID = "CancelButton",
					Text = "Cancel",
				},
				ui:Button{
					ID = "OKButton",
					Text = "OK",
				},
			},
		},
	})

	-- The window was closed
	function win.On.SplitKeys.Close(ev)
		disp:ExitLoop()
	end

	-- Add your GUI element based event functions here:
	itm = win:GetItems()

	-- What node connection point should be the basis for the splitting?
	if tool.ID:match("^Fuse.vNumberFromCSV") or tool.ID:match("^Fuse.vTextFromCSV") then
		itm.SplitAtCombo:AddItem("Input Connection")
	elseif tool.ID:match("^Fuse.vTextFromFile") then
		itm.SplitAtCombo:AddItem("Output Connection")
	elseif tool.ID:match("^Fuse.vScriptValFromJSON") then
		itm.SplitAtCombo:AddItem("Output Connection")
	elseif tool.ID:match("^Fuse.vScriptValFromMetadata") then
		itm.SplitAtCombo:AddItem("Output Connection")
	elseif tool.ID:match("^Fuse.vScriptValToText") then
		itm.SplitAtCombo:AddItem("Input Connection")
	elseif tool.ID:match("^Fuse.vScriptValGetToNumber") then
		itm.SplitAtCombo:AddItem("Input Connection")
	elseif tool.ID:match("^Fuse.vScriptValGetToText") then
		itm.SplitAtCombo:AddItem("Input Connection")
	elseif tool.ID:match("^Fuse.vScriptValToNumber") then
		itm.SplitAtCombo:AddItem("Input Connection")
	elseif tool.ID:match("^Fuse.ocaSaver") or tool.ID:match("^Fuse.ocaInfo") or tool.ID:match("^Fuse.ocaMerge") or tool.ID:match("^Fuse.ocaSwitch") or tool.ID:match("^Fuse.ocaScript") or tool.ID:match("^Fuse.ocaStage") then
		itm.SplitAtCombo:AddItem("Input Connection")
		itm.SplitAtCombo:AddItem("Output Connection")
		itm.SplitAtCombo.CurrentIndex = 1
	elseif tool.ID:match("^Fuse.ocaFrame") or tool.ID:match("^Fuse.ocaText") or tool.ID:match("^Fuse.ocaWireless") or tool.ID:match("^Fuse.ocaLoader") then
		itm.SplitAtCombo:AddItem("Output Connection")
	elseif tool.ID:match("^Fuse.ocaRender") then
		itm.SplitAtCombo:AddItem("Input Connection")
	elseif tool.ID:match("^Fuse.vScriptValDoStringMultiplex") then
		itm.SplitAtCombo:AddItem("Input Connection")
		itm.SplitAtCombo:AddItem("Output Connection")
		-- Pre-select the input Connection item
		itm.SplitAtCombo.CurrentIndex = 0
	else
		itm.SplitAtCombo:AddItem("Input Connection")
		itm.SplitAtCombo:AddItem("Output Connection")
		-- Pre-select the Output Connection item
		itm.SplitAtCombo.CurrentIndex = 1
	end

	-- Add node creation build direction entries to the ComboBox control
	itm.BuildDirectionCombo:AddItem("Down")
	itm.BuildDirectionCombo:AddItem("Right")
	itm.BuildDirectionCombo:AddItem("Up")
	itm.BuildDirectionCombo:AddItem("Left")

	-- The dialog was cancelled
	function win.On.CancelButton.Clicked(ev)
		disp:ExitLoop()
	end

	-- Process the selection
	function win.On.OKButton.Clicked(ev)
		local keyTbl = {}

		for i = 0, itm.Tree:TopLevelItemCount() - 1 do
			local item = itm.Tree:TopLevelItem(i)
			if item.Hidden == false then
				if item.CheckState[0] == "Checked" then
					table.insert(keyTbl, {key = item.Text[1], value = item:GetData(0, "UserRole"), node = item.Text[2]}) 
				end
			end
		end

		win:Hide()
		SplitKeys(keyTbl, itm.SplitAtCombo.CurrentText, itm.BuildDirectionCombo.CurrentIndex)
		disp:ExitLoop()
	end

	-- A Tree view row was clicked on
	function win.On.Tree.ItemClicked(ev)
		if ev.item and ev.column == 1 then
			-- Key column 1 clicked
			if ev.item.CheckState[0] == "Checked" then
				-- Remove the item
				ev.item.CheckState[0] = "Unchecked"
			elseif ev.item.CheckState[0] == "Unchecked" then
				-- Add the item
				ev.item.CheckState[0] = "Checked"
			end
		elseif ev.item and ev.column == 2 then
			-- Node Type column 2 clicked
			if tool.ID:match("^Fuse.vNumberFromCSV") or tool.ID:match("^Fuse.vTextFromCSV") or tool.ID:match("^Fuse.vTextFromFile") then
				if ev.item.Text[2] == "Fuse.vNumberFromCSV" then
					ev.item.Text[2] = "Fuse.vTextFromCSV"
				elseif ev.item.Text[2] == "Fuse.vTextFromCSV" then
					ev.item.Text[2] = "Fuse.vNumberFromCSV"
				end
			elseif tool.ID:match("^Fuse.vScriptValGetToText") or tool.ID:match("^Fuse.vScriptValGetToTable") or tool.ID:match("^Fuse.vScriptValGetToNumber") or tool.ID:match("^Fuse.vScriptValGetElementToTable") or tool.ID:match("^Fuse.vScriptValViewer") or tool.ID:match("^Fuse.vScriptValFromJSON") or tool.ID:match("^Fuse.vScriptValToText") or tool.ID:match("^Fuse.vScriptValDoString") or tool.ID:match("^Fuse.vScriptValDoStringMultiplex") or tool.ID:match("^Fuse.vScriptValFromMetadata") then
				if ev.item.Text[2] == "Fuse.vScriptValGetToTable" then
					ev.item.Text[2] = "Fuse.vScriptValGetToText"
				elseif ev.item.Text[2] == "Fuse.vScriptValGetToText" then
					ev.item.Text[2] = "Fuse.vScriptValGetToNumber"
				elseif ev.item.Text[2] == "Fuse.vScriptValGetToNumber" then
					ev.item.Text[2] = "Fuse.vScriptValGetElementToTable"
				elseif ev.item.Text[2] == "Fuse.vScriptValGetElementToTable" then
					ev.item.Text[2] = "Fuse.vScriptValViewer"
				elseif ev.item.Text[2] == "Fuse.vScriptValViewer" then
					ev.item.Text[2] = "Fuse.vScriptValToText"
				elseif ev.item.Text[2] == "Fuse.vScriptValToText" then
					ev.item.Text[2] = "Fuse.vScriptValGetToTable"
				else
					ev.item.Text[2] = "Fuse.vScriptValGetToTable"
				end
			elseif tool.ID:match("^Fuse.ocaFrame") or tool.ID:match("^Fuse.ocaLayer") or tool.ID:match("^Fuse.ocaStage") or tool.ID:match("^Fuse.ocaText") or tool.ID:match("^Fuse.ocaSwitch") or tool.ID:match("^Fuse.ocaWireless") or tool.ID:match("^Fuse.ocaLoader") or tool.ID:match("^Fuse.ocaSaver") or tool.ID:match("^Fuse.ocaInfo") or tool.ID:match("^Fuse.ocaMerge") or tool.ID:match("^Fuse.ocaInfo") or tool.ID:match("^Fuse.ocaRender") or tool.ID:match("^Fuse.ocaScript") then
				if ev.item.Text[2] == "Fuse.vScriptValGetToTable" then
					ev.item.Text[2] = "Fuse.vScriptValGetToText"
				elseif ev.item.Text[2] == "Fuse.vScriptValGetToText" then
					ev.item.Text[2] = "Fuse.vScriptValGetToNumber"
				elseif ev.item.Text[2] == "Fuse.vScriptValGetToNumber" then
					ev.item.Text[2] = "Fuse.vScriptValGetElementToTable"
				elseif ev.item.Text[2] == "Fuse.vScriptValGetElementToTable" then
					ev.item.Text[2] = "Fuse.ocaInfo"
				elseif ev.item.Text[2] == "Fuse.ocaInfo" then
					ev.item.Text[2] = "Fuse.vScriptValViewer"
				elseif ev.item.Text[2] == "Fuse.vScriptValViewer" then
					ev.item.Text[2] = "Fuse.vScriptValToText"
				elseif ev.item.Text[2] == "Fuse.vScriptValToText" then
					ev.item.Text[2] = "Fuse.vScriptValGetToTable"
				else
					ev.item.Text[2] = "Fuse.vScriptValGetToTable"
				end
			end
		end
	end

	-- The Select "All" button was pressed
	function win.On.SelectAllButton.Clicked(ev)
		-- print("[Select All]")

		for i = 0, itm.Tree:TopLevelItemCount() - 1 do
			local item = itm.Tree:TopLevelItem(i)

			if item.Hidden == false then
				-- Add the item
				item.CheckState[0] = "Checked"
			end
		end
	end

	-- The Select "None" button was pressed
	function win.On.SelectNoneButton.Clicked(ev)
		-- print("[Select None]")

		for i = 0, itm.Tree:TopLevelItemCount() - 1 do
			local item = itm.Tree:TopLevelItem(i)
			if item.Hidden == false then
				-- Remove the item
				item.CheckState[0] = "Unchecked"
			end
		end
	end

	-- The Select "Invert" button was pressed
	function win.On.SelectInvertButton.Clicked(ev)
		-- print("[Select Invert]")

		for i = 0, itm.Tree:TopLevelItemCount() - 1 do
			local item = itm.Tree:TopLevelItem(i)
			if item.Hidden == false then
				if item.CheckState[0] == "Checked" then
					-- Remove the item
					item.CheckState[0] = "Unchecked"
				elseif item.CheckState[0] == "Unchecked" then
					-- Add the item
					item.CheckState[0] = "Checked"
				end
			end
		end
	end

	-- The "Split At" Input Connection/Output Connection ComboBox was changed
	function win.On.SplitAtCombo.CurrentIndexChanged(ev)
		RefreshTree(itm, itm.SplitAtCombo.CurrentText)
	end

	RefreshTree(itm, itm.SplitAtCombo.CurrentText)

	-- The app:AddConfig() command that will capture the "Control + W" or "Control + F4" hotkeys so they will close the window instead of closing the foreground composite.
	app:AddConfig("SplitKeys", {
		Target {
			ID = "SplitKeys",
		},

		Hotkeys {
			Target = "SplitKeys",
			Defaults = true,
			
			CONTROL_W = "Execute{cmd = [[app.UIManager:QueueEvent(obj, 'Close', {})]]}",
			CONTROL_F4 = "Execute{cmd = [[app.UIManager:QueueEvent(obj, 'Close', {})]]}",
			ESCAPE = "Execute{cmd = [[app.UIManager:QueueEvent(obj, 'Close', {})]]}",
		},
	})

	-- Make the OK button the default input
	itm.OKButton:SetFocus("OtherFocusReason")

	win:Show()
	disp:RunLoop()
	win:Hide()
	
	app:RemoveConfig("SplitKeys")
end


Main()
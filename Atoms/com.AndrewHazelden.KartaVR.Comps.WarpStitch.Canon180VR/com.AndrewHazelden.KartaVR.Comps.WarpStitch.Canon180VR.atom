Atom {
	Name = "KartaVR Example | Canon 180VR Comp",
	Category = "Kartaverse/KartaVR/Comps",
	Author = "Andrew Hazelden",
	Version = 5.7,
	Date = {2023, 12, 25},
	Description = [[<h1>Canon 180VR Stitching Comp</h1>

<p>This example processes Canon EOS 180VR footage filmed with a Canon EOS R5 Camera Body and a Canon RF 5.2mm Dual Stereo Fisheye 190&deg; Lens.</p>

<p>The fisheye imagery is warped by the Kartaverse WarpStitch Fuse, then passed into a Combiner node to place the left and right eye view images into a side-by-side 180&deg;x180&deg; cropped LatLong VR180 output. A reframed "flat" view of the imagery can be previewed at the end of the comp with the help of the Kartaverse Reframe360Ultra node.</p>

<h1>Example Footage License</h1>

<p>The Canon EOS 180VR media used in this example composite features Keeley Ann Turner sitting in a DeLorean car at CES 2022. The footage was photographed by Hugh Hou (<a href="https://www.youtube.com/hughhoufilm">https://www.youtube.com/hughhoufilm</a>). This content is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.</p>
]],
	Deploy = {
		"Comps/Kartaverse/WarpStitch/WarpStitch Canon 180VR RF 5.2mm Dual Fisheye Stereo/Docs/Anaglyph.png",
		"Comps/Kartaverse/WarpStitch/WarpStitch Canon 180VR RF 5.2mm Dual Fisheye Stereo/Docs/Canon RF52.png",
		"Comps/Kartaverse/WarpStitch/WarpStitch Canon 180VR RF 5.2mm Dual Fisheye Stereo/Docs/Combiner.png",
		"Comps/Kartaverse/WarpStitch/WarpStitch Canon 180VR RF 5.2mm Dual Fisheye Stereo/Docs/EOS Anaglyph.png",
		"Comps/Kartaverse/WarpStitch/WarpStitch Canon 180VR RF 5.2mm Dual Fisheye Stereo/Docs/Reactor Atom Package Small.png",
		"Comps/Kartaverse/WarpStitch/WarpStitch Canon 180VR RF 5.2mm Dual Fisheye Stereo/Docs/Reactor Atom Package.png",
		"Comps/Kartaverse/WarpStitch/WarpStitch Canon 180VR RF 5.2mm Dual Fisheye Stereo/Docs/Reframe Versions.png",
		"Comps/Kartaverse/WarpStitch/WarpStitch Canon 180VR RF 5.2mm Dual Fisheye Stereo/Docs/Rotate Sphere Expressions.png",
		"Comps/Kartaverse/WarpStitch/WarpStitch Canon 180VR RF 5.2mm Dual Fisheye Stereo/Docs/WarpStitch Canon 180VR Small.png",
		"Comps/Kartaverse/WarpStitch/WarpStitch Canon 180VR RF 5.2mm Dual Fisheye Stereo/Docs/WarpStitch Canon 180VR.png",
		"Comps/Kartaverse/WarpStitch/WarpStitch Canon 180VR RF 5.2mm Dual Fisheye Stereo/Media/CC BY-SA 4.0 Media License.txt",
		"Comps/Kartaverse/WarpStitch/WarpStitch Canon 180VR RF 5.2mm Dual Fisheye Stereo/Media/Canon_RF_5.2mm_Dual_Fisheye_Stereo_DeLorean_Car.jpg",
		"Comps/Kartaverse/WarpStitch/WarpStitch Canon 180VR RF 5.2mm Dual Fisheye Stereo/Readme.htm",
		"Comps/Kartaverse/WarpStitch/WarpStitch Canon 180VR RF 5.2mm Dual Fisheye Stereo/Render/EOS_Anaglyph_VR180.0000.jpg",
		"Comps/Kartaverse/WarpStitch/WarpStitch Canon 180VR RF 5.2mm Dual Fisheye Stereo/Render/EOS_SBS_VR180.0000.jpg",
		"Comps/Kartaverse/WarpStitch/WarpStitch Canon 180VR RF 5.2mm Dual Fisheye Stereo/WarpStitch Canon 180VR.comp",
		"Comps/Kartaverse/WarpStitch/WarpStitch Canon 180VR RF 5.2mm Dual Fisheye Stereo/WarpStitch Canon 180VR.htm",
	},
}

# Savitzky Golay Smoothing Filters

Written By: Srecko Zrilic  
Written On: May 06th, 2008  

## Savitzky Golay Smoothing Value Filter

This tool smoothes value data

![Point Filter](Images/Savitzky_Golay_Smoothing_Value_Filter.png)

### Usage

**First option:** select this tool to animate any number input, then animate the "Value Input" with any other number modifier Second option: insert this tool to an already animated number input via the "Insert" submenu of the input control context menu.

**Important:** The most important parameter is the Sample Spacing which actually determines what is smoothed and what is not. The smaller value the less smoothing or the more following the curve. For sample spacing == 0, no smoothing at all, whereas for very large values of sample spacing, approximation is big, hence the result is more incorrect. By tweaking this parameter, we can select which frequency and upper of a curve noise is to be smoothed. Should never be zero.

The parameters left sample count and right sample count are made to be equal for this fuse and they are equal to the sample width divided by sample spacing. The default for sample width, left and right sample count is 5, and for sample spacing is 1.

## Savitzky Golay Smoothing Point Filter

This tool smoothes point data

![Point Filter](Images/Savitzky_Golay_Smoothing_Point_Filter.png)

### Usage

**First option:** Select this tool to animate any point input, then animate the "Point Input" with any other point modifier.

**Second option:** Insert this tool to an already animated point input via the "Insert" submenu of the input control context menu.

Known issues: A path connected to the fuse can not be edited by dragging the crosshair nor by typing in the fields within the input that the fuse is connected to, but rather by typing in the fields of the fuse input the path is connected to.

The path can be edited by any other way including the 'drag time - click' operation in ClickAppend mode, or by giving a focus to the path and dragging/adding any point including that one sitting on the crosshair.

Important: The most important parameter is the Sample Spacing which actually determines what is smoothed and what is not. The smaller value the less smoothing or the more following the curve. For sample spacing == 0, no smoothing at all, whereas for very large values of sample spacing, approximation is big, hence the result is more incorrect.

By tweaking this parameter, we can select which frequency and upper of a curve noise is to be smoothed. Should never be zero.

The parameters left sample count and right sample count are made to be equal for this fuse and they are equal to the sample width divided by sample spacing.

The default for sample width, left and right sample count is 5, and for sample spacing is 1.


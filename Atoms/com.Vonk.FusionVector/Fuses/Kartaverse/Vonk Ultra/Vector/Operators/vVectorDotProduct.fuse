-- ============================================================================
-- modules
-- ============================================================================
local matrixutils = self and require("vmatrixutils") or nil
local matrix = self and require("matrix") or nil

-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vVectorDotProduct"
DATATYPE = "Text"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = DATATYPE,
    REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\Vector\\Operators",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Adds two vectors.",
    REGS_OpIconString = FUSE_NAME
})

function Create()
    -- [[ Creates the user interface. ]]
    InVector1 = self:AddInput("Vector1", "Vector1", {
        LINKID_DataType = "Text",
        INPID_InputControl = "TextEditControl",
        TEC_Wrap = true,
        LINK_Main = 1
    })

    InVector2 = self:AddInput("Vector2", "Vector2", {
        LINKID_DataType = "Text",
        INPID_InputControl = "TextEditControl",
        TEC_Wrap = true,
        LINK_Main = 2
    })

    InShowInput = self:AddInput("Show Input", "ShowInput", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 1.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })
    
    OutDotProduct = self:AddOutput("Output", "Output", {
        LINKID_DataType = "Number",
        LINK_Main = 1
    })
end

function NotifyChanged(inp, param, time)
    --[[
        Handles all input control events.

        :param inp: Input that triggered a signal.
        :type inp: Input

        :param param: Parameter object holding the (new) value.
        :type param: Parameter

        :param time: Current frame number.
        :type time: float
    ]]
    if inp == InShowInput then
        local visible
        if param.Value == 1.0 then visible = true else visible = false end
        InVector1:SetAttrs({LINK_Visible = visible})
        InVector2:SetAttrs({LINK_Visible = visible})
    end
end

function Process(req)
    -- [[ Creates the output. ]]
    local vec1_str = InVector1:GetValue(req).Value
    local vec2_str = InVector2:GetValue(req).Value

    -- vector from interchangeable string
    local vec1 = matrixutils.matrix_from_string(vec1_str)
    local vec2 = matrixutils.matrix_from_string(vec2_str)

    -- dot product
    -- https://www.symbolab.com/solver/vector-dot-product-calculator
    local vec_result = vec1[1][1] * vec2[1][1] + vec1[1][2] * vec2[1][2] + vec1[1][3] * vec2[1][3]

    local out = Number(vec_result)

    OutDotProduct:Set(req, out)
end

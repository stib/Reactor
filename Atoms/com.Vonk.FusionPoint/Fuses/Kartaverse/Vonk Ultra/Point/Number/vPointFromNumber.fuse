-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vPointFromNumber"
DATATYPE = "Point"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = DATATYPE,
    REGID_InputDataType = "Number",
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\Point\\Number",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Return a Fusion Point object from two numbers.",
    REGS_OpIconString = FUSE_NAME,
})

function Create()
    -- [[ Creates the user interface. ]]
    InNumberX = self:AddInput("Number X", "NumberX", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ScrewControl",
        -- INPID_InputControl = "SliderControl",
        INP_MinScale = -100,
        INP_MaxScale = 100,
        INP_Default = 0,
        IC_Steps = 201,
        INP_MinAllowed = -1e+38,
        INP_MaxAllowed = 1e+38,
        -- INP_MinAllowed = -1000000,
        -- INP_MaxAllowed = 1000000,
        LINK_Main = 1,
    })
    
    InNumberY = self:AddInput("Number Y", "NumberY", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ScrewControl",
        -- INPID_InputControl = "SliderControl",
        INP_MinScale = -100,
        INP_MaxScale = 100,
        INP_Default = 0,
        IC_Steps = 201,
        INP_MinAllowed = -1e+38,
        INP_MaxAllowed = 1e+38,
        -- INP_MinAllowed = -1000000,
        -- INP_MaxAllowed = 1000000,
        LINK_Main = 2,
    })

    InShowInput = self:AddInput("Show Input", "ShowInput", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    OutPoint = self:AddOutput("Output", "Output", {
        LINKID_DataType = "Point",
        INPID_InputControl = "OffsetControl",
        LINK_Main = 1
    })
end

function NotifyChanged(inp, param, time)
    --[[
        Handles all input control events.

        :param inp: Input that triggered a signal.
        :type inp: Input

        :param param: Parameter object holding the (new) value.
        :type param: Parameter

        :param time: Current frame number.
        :type time: float
    ]]
    if inp == InShowInput then
        local visible
        if param.Value == 1.0 then visible = true else visible = false end
        InNumberX:SetAttrs({LINK_Visible = visible})
        InNumberY:SetAttrs({LINK_Visible = visible})
    end
end

function Process(req)
    -- [[ Creates the output. ]]
    local numX = tonumber(InNumberX:GetValue(req).Value)
    local numY = tonumber(InNumberY:GetValue(req).Value)

    local point = Point(numX, numY)

    OutPoint:Set(req, point)
end

-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vPointCreateRandom"
DATATYPE = "Point"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = DATATYPE,
    -- REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\Point\\Create",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Create a Fusion Point object with a random position.",
    REGS_OpIconString = FUSE_NAME,
    REG_TimeVariant = true, -- required to disable caching of the current time parameter
    REGB_Temporal = true, -- ensures reliability in Resolve 15
})

function Create()
    -- [[ Creates the user interface. ]]
    InSeedX = self:AddInput("SeedX", "SeedX", {
        LINKID_DataType = "Number",
        INPID_InputControl = "SliderControl",
        TEC_Lines = 1,
        INP_MinScale = -100,
        INP_MaxScale = 100,
        INP_MinAllowed = -1e+38,
        INP_MaxAllowed = 1e+38,
        -- INP_MinAllowed = -1000000,
        -- INP_MaxAllowed = 1000000,
        INP_Default = 0,
        INP_Integer = true,
        LINK_Main = 1,
    })
    
    InSeedY = self:AddInput("SeedY", "SeedY", {
        LINKID_DataType = "Number",
        INPID_InputControl = "SliderControl",
        TEC_Lines = 1,
        INP_MinScale = -100,
        INP_MaxScale = 100,
        INP_MinAllowed = -1e+38,
        INP_MaxAllowed = 1e+38,
        -- INP_MinAllowed = -1000000,
        -- INP_MaxAllowed = 1000000,
        INP_Default = 0,
        INP_Integer = true,
        LINK_Main = 2,
    })

    InUpper = self:AddInput("Upper", "Upper", {
        LINKID_DataType = "Number",
        INPID_InputControl = "SliderControl",
        TEC_Lines = 1,
        INP_MinScale = -100,
        INP_MaxScale = 100,
        INP_MinAllowed = -1e+38,
        INP_MaxAllowed = 1e+38,
        -- INP_MinAllowed = -1000000,
        -- INP_MaxAllowed = 1000000,
        INP_Default = 1,
        LINK_Main = 3,
        INP_Integer = true,
    })

    InLower = self:AddInput("Lower", "Lower", {
        LINKID_DataType = "Number",
        INPID_InputControl = "SliderControl",
        TEC_Lines = 1,
        INP_MinScale = -100,
        INP_MaxScale = 100,
        INP_MinAllowed = -1e+38,
        INP_MaxAllowed = 1e+38,
        -- INP_MinAllowed = -1000000,
        -- INP_MaxAllowed = 1000000,
        INP_Default = 0,
        LINK_Main = 4,
        INP_Integer = true,
    })

    OutPoint = self:AddOutput("Output", "Output", {
        LINKID_DataType = "Point",
        INPID_InputControl = "OffsetControl",
        LINK_Main = 1
    })
end

function NotifyChanged(inp, param, time)
    --[[
        Handles all input control events.

        :param inp: Input that triggered a signal.
        :type inp: Input

        :param param: Parameter object holding the (new) value.
        :type param: Parameter

        :param time: Current frame number.
        :type time: float
    ]]
    if inp == InShowInput then
        local visible
        if param.Value == 1.0 then visible = true else visible = false end

        InSeedX:SetAttrs({LINK_Visible = visible})
        InSeedY:SetAttrs({LINK_Visible = visible})
        InUpper:SetAttrs({LINK_Visible = visible})
        InLower:SetAttrs({LINK_Visible = visible})
    end
end

function Process(req)
    -- [[ Creates the output. ]]
    local seed_x = InSeedX:GetValue(req).Value
    local seed_y = InSeedY:GetValue(req).Value
    local range_upper = InUpper:GetValue(req).Value
    local range_lower = InLower:GetValue(req).Value

    math.randomseed(seed_x)
    local outX = math.random(range_lower, range_upper)
    
    math.randomseed(seed_y)
    local outY = math.random(range_lower, range_upper)

    out = Point(outX, outY)

    OutPoint:Set(req, out)
end


{[[Did you know a Fusion Studio Dongle will allow you to not only run the award winning compositor, Fusion 9, with its rich feature set tailored to VFX workflow integration, but also DaVinci Resolve 15? In addition, you get built-in ProRes encoding on Windows, for your transcoding needs. All of this for the price of a Resolve license which includes, well, only Resolve.

-- Unknown Genius]]}

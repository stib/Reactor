{[[You can read the current Fusion composite's frame rate setting in a Lua script using:

frameRate = comp:GetPrefs("Comp.FrameFormat.Rate")
print(frameRate)

-- Andrew Hazelden]]}

{[[To avoid visible banding when displaying a low-contrasty gradient, make sure the image is 32 bit float, then RMB on the Display View, select General Options > Dither.

-- Gregory Chalenko]]}

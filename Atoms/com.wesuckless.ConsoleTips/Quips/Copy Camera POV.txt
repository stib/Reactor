{[[Fusion's Perspective camera view navigates differently from actual Camera objects. But you can use the perspective camera to navigate and then copy the PoV to whichever camera you like. It's actually a great way to work with multiple cameras.

-- Pieter Van Houte]]}

{[[Text("Text" .. CustomTool.Setup1.Value)

Text([text]) means, show the data as a text. Like point controllers use Point([X,Y])

"Text" is text that will be shown. The "" shows that it's text inside and not code
.. will add the next part to the text.

"Text" .. " here" would be the same as "Text here"

CustomTool1.Setup1.Value takes the value/content of CustomTool1.Setup1

-- Jacob Danell]]}

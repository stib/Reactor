{[[You can add comments to the expression fields of a Custom Tool by prefacing them with #. But everything after the # becomes a comment, even if it's on a new line, so put your actual expression first: c1 + n1 #Brightness

-- Bryan Ray]]}

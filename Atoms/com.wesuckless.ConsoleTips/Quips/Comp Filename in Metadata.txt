{[[I highly recommend adding Text(comp.FileName) to the metadata of your images before your savers. So useful for figuring out what file made what image you are looking at days, weeks, months from now.

-- Chad Capeland]]}

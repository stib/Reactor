{[[Here's a workaround to reliably lock a Disk Cache in Fusion:

1. Add a merge node to a heavy tool that's going to be cached.
2. Do a cache to disk for this Merge node. (You can change DiskCaches Path Map in settings to something appropriate)
3. Disconnect all nodes before cached Merge and add a BG node instead.
4. Profit!

-- Alex Bogomolov]]}

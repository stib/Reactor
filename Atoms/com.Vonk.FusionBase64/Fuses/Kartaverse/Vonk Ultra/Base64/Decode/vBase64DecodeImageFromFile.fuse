-- ============================================================================
-- modules
-- ============================================================================
local base64utils = self and require("vbase64utils") or nil

-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vBase64DecodeImageFromFile"
DATATYPE = "Text"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = DATATYPE,
    REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\Base64\\Decode",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Base64 decodes an image from a file.",
    REGS_OpIconString = FUSE_NAME
})

function Create()
    -- [[ Creates the user interface. ]]
    InFile = self:AddInput("Input", "Input", {
        LINKID_DataType = "Text",
        INPID_InputControl = "FileControl",
        FC_IsSaver = false,
        FC_ClipBrowse = false,
        FCS_FilterString =  "Any Filetype (*.*)|*.*",
        LINK_Main = 1
    })

--    InViewFile = self:AddInput('View File', 'View File', {
--        LINKID_DataType = 'Number',
--        INPID_InputControl = 'ButtonControl',
--        INP_DoNotifyChanged = true,
--        INP_External = false,
--        ICD_Width = 1,
--        INP_Passive = true,
--        IC_Visible = true,
--        BTNCS_Execute = [[
---- check if a tool is selected
--local selectedNode = tool or comp.ActiveTool
--if selectedNode then
--    local filename = comp:MapPath(selectedNode:GetInput('Input'))
--    if filename then
--        if bmd.fileexists(filename) then
--            bmd.openfileexternal('Open', filename)
--        else
--            print('[View File] File does not exist yet:', filename)
--        end
--    else
--        print('[View File] Filename is nil. Possibly the text is sourced from an external text input connection.')
--    end
--else
--    print('[View File] Please select the node.')
--end
--        ]],
--    })

    InShowInput = self:AddInput("Show Input", "ShowInput", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    OutData = self:AddOutput("Output" , "Output" , {
        LINKID_DataType = "Image",
        LINK_Main = 1
    })
end

function NotifyChanged(inp, param, time)
    --[[
        Handles all input control events.

        :param inp: Input that triggered a signal.
        :type inp: Input

        :param param: Parameter object holding the (new) value.
        :type param: Parameter

        :param time: Current frame number.
        :type time: float
    ]]
    if inp == InShowInput then
        if param.Value == 1.0 then
            InFile:SetAttrs({LINK_Visible = true})
        else
            InFile:SetAttrs({LINK_Visible = false})
        end
    end
end

function Process(req)
    -- Read the filename
    local rel_path = InFile:GetValue(req).Value
    local abs_path = self.Comp:MapPath(rel_path)
    local base64_str = base64utils.read_file(abs_path, "r")

    -- print(abs_path, "\n", base64_str)

    -- [[ Creates the output. ]]
    local nodeName = self.Name
    local folder = self.Comp:MapPath('Temp:/Vonk/')
    local path = self.Comp:MapPath(folder .. tostring(nodeName)  .. '_' .. tostring(bmd.createuuid())..'.json')
    if bmd.fileexists(folder) == false then
        bmd.createdir(folder)
    end

    -- Write the decoded image to a temporary file
    local data = base64utils.base64decode(base64_str)
    base64utils.write_file(path, data)

    -- Fusion 16.1+
    local clip = Clip(path, false)

    -- Fusion 16.1+
    clip:Open()
    out = clip:GetFrame(0)
    clip:Close()

    if not clip then
        error("[Base 64 Image Error] Image is nil")
    end

    -- Push the result to the node's output connection
    OutData:Set(req, out)

    -- Remove the temp image
    os.remove(path)
end

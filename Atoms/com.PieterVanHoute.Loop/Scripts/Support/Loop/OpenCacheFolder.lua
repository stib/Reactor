--[[

Suck Less Loop

-------------------------------------------------------------------
Copyright (c) 2021,  Pieter Van Houte
<pieter[at]secondman[dot]com>
-------------------------------------------------------------------

--]]

require "SuckLessDialogs"

local comp = fusion.CurrentComp
local currentsolver = comp:GetToolList(true)

if #currentsolver == 1 and (currentsolver[1]:GetAttrs().TOOLS_RegID == "GroupOperator" or currentsolver[1]:GetAttrs().TOOLS_RegID == "MacroOperator") then -- improve this check

	local path = currentsolver[1].CacheFolder[fu.TIME_UNDEFINED]
	if path == "" then
		SuckLessMsgDialog("Suck More!", "No Cache Folder is defined in Loop Out node.", "OK")
		return
	end
	
	--dump(app:GetPrefs("Global.Paths.Map"))
	
	-- MapPath resolves any existing path map shortcuts to the full path on disk
	path = app:MapPath(path)
	
	if bmd.direxists(path) == true then
		bmd.openfileexternal("Open", path)
		print ("Opening path in external file requester")
	else
		SuckLessMsgDialog("Suck More!", "Invalid path specified.", "OK")
	end
	
	bmd.openfileexternal("Open", path)
	
else
	SuckLessMsgDialog("Suck More!", "Please make sure the Loop Out node is selected before pressing the button.", "OK")
	return
end
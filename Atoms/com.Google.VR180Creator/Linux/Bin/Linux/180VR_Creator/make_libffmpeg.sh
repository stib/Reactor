#!/bin/bash
# Builds a target libffmpeg.so that combines libav libraries into one shared
# library. Should be run from ffmpeg src root directory.
#
# This script only runs on Linux and OSX.
# The Windows binary is cross-compiled on Linux.
#
# See LICENSES.json for the relevant licensing information.
set -o errexit
set -o nounset
set -o xtrace

readonly ROOT="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
readonly FFMPEG_VERSION='4.0.2'
readonly X264_DIR="$ROOT/x264"
readonly X264_COMMIT='7d0ff22e8c96de126be9d3de4952edd6d1b75a8c'
readonly CROSS_PREFIX='x86_64-w64-mingw32-'

GCC_BIN='gcc'
PKG_CONFIG_BIN='pkg-config'
WINDOWS=false

OSX_HOST=false
if [[ "$(uname)" == "Darwin" ]]; then
  OSX_HOST=true
  NUM_CORES="$(sysctl -n hw.ncpu)"
else
  # Assume linux
  NUM_CORES="$(nproc)"
fi

OPTS=(
  '--enable-pic'
  '--enable-ffplay'
  "--extra-cflags=-I$ROOT/output/include"
  "--extra-ldflags=-L$ROOT/output/lib"
)
FILES=(
  libavformat/libavformat.a
  libavcodec/libavcodec.a
  libswresample/libswresample.a
  libswscale/libswscale.a
  libavutil/libavutil.a
)

function print_usage_die {
  2>&1 echo 'Usage: make_libffmpeg.sh [clean|win]*'
  exit 1
}

function checkcommand {
  if ! which "$1" &>/dev/null; then
    echo "Required command not found: $1"
    exit 1
  fi
}

# Parse/Validate arguments
while (( "$#" > 0 )); do
  arg="$1"
  shift
  if [[ "$arg" == 'clean' ]]; then
    rm -rf output libffmpeg.* ffmpeg* "$X264_DIR"
    echo 'All artifacts deleted'
    exit 0
  elif [[ "$arg" == 'win' ]]; then
    WINDOWS=true
    # Install: mingw-w64 mingw-w64-tools yasm
    GCC_BIN="${CROSS_PREFIX}gcc"
    PKG_CONFIG_BIN="${CROSS_PREFIX}pkg-config"
    checkcommand yasm
    checkcommand gendef
  else
    print_usage_die
  fi
done

checkcommand cmake
checkcommand curl
checkcommand git
checkcommand "$GCC_BIN"
checkcommand "$PKG_CONFIG_BIN"
checkcommand nasm


# Download FFmpeg
if ! ls -d */ | grep '^ffmpeg-'; then
  echo "Fetching FFmpeg source..."
  curl -O "https://www.ffmpeg.org/releases/ffmpeg-${FFMPEG_VERSION}.tar.xz"
  curl -O "https://www.ffmpeg.org/releases/ffmpeg-${FFMPEG_VERSION}.tar.xz.asc"

  if ! gpg --verify ffmpeg-${FFMPEG_VERSION}.tar.xz.asc ffmpeg-${FFMPEG_VERSION}.tar.xz; then
    echo "gpg verification failed. If you do not have the FFmpeg key" \
         " installed, you may need to run:" \
         " gpg --search-keys B4322F04D67658D8"
    exit 1
  fi
  tar -xf ffmpeg-${FFMPEG_VERSION}.tar.xz
fi
FFMPEG_DIR="$ROOT/$(ls -d */ | grep ^ffmpeg-)"

if [[ ! -d "$X264_DIR" ]]; then
  echo "Fetching x264 source..."
  git clone 'https://git.videolan.org/git/x264.git'
fi
cd "$X264_DIR"
echo "Building libx264..."
git reset --hard "$X264_COMMIT"
make distclean  || true
XOPTS=('--disable-gpl'
       '--enable-pic'
       '--enable-shared'
       '--enable-static'
       "--prefix=$ROOT/output")
if $WINDOWS; then
  XOPTS+=('--host=x86_64-mingw32'
          "--cross-prefix=$CROSS_PREFIX")
fi
./configure "${XOPTS[@]}"
make -j "$NUM_CORES"
make install
OPTS+=('--enable-libx264')
FILES+=("$X264_DIR/libx264.a")

cd "$FFMPEG_DIR"
if [[ ! -e .patched ]]; then
  echo "Applying FFMPEG.PATCH..."
  patch -t -p0 < ../FFMPEG.PATCH
fi
# Move libx264 to non-gpl license list, since we have a commercial license
if $OSX_HOST; then
    sed -i '' 's#^[[:space:]]*libx264$##' configure
    sed -i '' 's#^[[:space:]]*ladspa$#ladspa libx264#' configure
else
    sed -i 's#^[[:space:]]*libx264$##' configure
    sed -i 's#^[[:space:]]*ladspa$#ladspa libx264#' configure
fi

echo "Building FFmpeg libraries..."
make distclean  || true
if $WINDOWS; then
  OPTS+=('--arch=x86_64'
         '--target-os=mingw32'
         "--cross-prefix=$CROSS_PREFIX")
fi
PKG_CONFIG_PATH="$ROOT/output/lib/pkgconfig" ./configure "${OPTS[@]}"
make -j "$NUM_CORES"

if $OSX_HOST; then  # OSX host, OSX binary
    "$GCC_BIN" -shared -fPIC -o libffmpeg.dylib -undefined dynamic_lookup -Wl,-all_load "${FILES[@]}"
    mv libffmpeg.dylib ../
elif $WINDOWS; then  # Linux host, Windows binary
  "$GCC_BIN" -shared -fPIC -o ffmpeg.dll -Wl,--strip-all -Wl,-Bsymbolic \
      -Wl,--whole-archive "${FILES[@]}" -Wl,--no-whole-archive \
      -lws2_32 -lbcrypt -lole32 -lsecur32
  # Build .lib file needed by windows linker
  gendef ffmpeg.dll
  "${CROSS_PREFIX}dlltool" -d ffmpeg.def -l ffmpeg.lib
  mv ffmpeg.{dll,lib} ../
else  # Linux host, Linux binary
  "$GCC_BIN" -shared -fPIC -o libffmpeg.so -Wl,--strip-all -Wl,-Bsymbolic \
      -Wl,--whole-archive "${FILES[@]}" -Wl,--no-whole-archive
  mv libffmpeg.so ../
fi

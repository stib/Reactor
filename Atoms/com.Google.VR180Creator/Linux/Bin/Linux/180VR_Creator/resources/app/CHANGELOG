VR180 Creator Changelog

Version 4.0.0
=============
New Features:
- Support for stitching video from the Z CAM K1 Pro
- Oculus Go support (at 5760x2880 resolution in Left/Right)
- The application now remembers the last used settings
Bugs Fixed:
- Improved color range handling for certain video formats

Version 3.0.1
=============
Bugs Fixed:
- Added support for Unicode file names on Windows

Version 3.0.0
=============
New Features:
- Windows version now available
- VR180 Photos: Added support for photo conversion to 360 top/bottom equirectangular

Version 2.0.0
=============
New Features:
- VR180 Photos: Support for splitting and merging photos for editing
- Added localization support for 13 languages
- Support for Final Cut Pro X: https://support.google.com/vr180/answer/9047826
- 8K output resolution support for videos
- Support for video files that only contain Spherical Metadata V1 (including Nikon Keymission 360)

Bugs Fixed:
- Fixed crash in "Prepare for publishing" for videos containing PCM audio (e.g. Final Cut Pro ProRes output).

Version 1.1.0
=============
New Features
- DNxHR codec option is now available in "Convert for editing".

Bugs Fixed:
- "Prepare for publishing" now works for injecting metadata into Cineform and DNxHR videos.
- Transcoding now works for source videos with pixel formats other than yuv420p.
- "Convert for editing" now works to read VR180 metadata from ProRes, DNxHR, and Cineform videos (mov and mp4 formats).
- The presence of certain data tracks no longer causes "Prepare for publishing" to fail.
- Data tracks are now preserved in "Convert for editing".

Version 1.0.0
=============
Initial release

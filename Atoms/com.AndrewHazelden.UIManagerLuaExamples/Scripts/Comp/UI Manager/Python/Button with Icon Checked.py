# This script attaches an image to a ui:Button with the help of a ui:Icon resource.
# The "Checked" version of the script shows buttons that can be toggled on/off.

ui = fu.UIManager
disp = bmd.UIDispatcher(ui)

dlg = disp.AddWindow({'WindowTitle': 'Button With Icon', 'ID': 'MyWin', 'Geometry': [100, 100, 500, 200],},[
	ui.VGroup({'Spacing': 0,},[
		# Add your GUI elements here:
		ui.HGroup({},[
			# Add foru buttons that have an icon resource attached and no border shading
			ui.Button({
				'ID': 'IconButton1',
				'Flat': True,
				'IconSize': [64, 64],
				'MinimumSize': [64, 64],
				'Flat': True,
				'Icon': ui.Icon({'File': 'Scripts:/Comp/UI Manager/fusion-logo.png'}),
				'Checkable': True,
			}),
			ui.Button({
				'ID': 'IconButton2',
				'Flat': True,
				'IconSize': [64, 64],
				'MinimumSize': [64, 64],
				'Flat': True,
				'Icon': ui.Icon({'File': 'Scripts:/Comp/UI Manager/fusion-logo.png'}),
				'Checkable': True,
			}),
			ui.Button({
				'ID': 'IconButton3',
				'Flat': True,
				'IconSize': [64, 64],
				'MinimumSize': [64, 64],
				'Flat': True,
				'Icon': ui.Icon({'File': 'Scripts:/Comp/UI Manager/fusion-logo.png'}),
				'Checkable': True,
			}),
			ui.Button({
				'ID': 'IconButton4',
				'Flat': True,
				'IconSize': [64, 64],
				'MinimumSize': [64, 64],
				'Flat': True,
				'Icon': ui.Icon({'File': 'Scripts:/Comp/UI Manager/fusion-logo.png'}),
				'Checkable': True,
			}),
		]),
		# Add a button with an icon and a text label. 
		# The Text label on the button uses the Droid Sans Mono font at 24 px in size
		ui.Button({
			'ID': 'IconTextButton',
			'Text': '\tClickable Button',
			'Font': ui.Font({
				'Family': 'Droid Sans Mono',
				'PixelSize': 24,
				'MonoSpaced': True,
			}),
			'Flat': True,
			'IconSize': [64, 64],
			'MinimumSize': [64, 64],
			'Margin': 10,
			'Checkable': True,
			'Icon': ui.Icon({'File': 'Scripts:/Comp/UI Manager/fusion-logo.png'}),
		}),
	]),
])

itm = dlg.GetItems()

# The window was closed
def _func(ev):
	disp.ExitLoop()
dlg.On.MyWin.Close = _func

# Add your GUI element based event functions here:
def _func(ev):
	state = itm['IconTextButton'].Checked
	print('[Button State] ', state)
	# disp.ExitLoop()
dlg.On.IconTextButton.Clicked = _func

def _func(ev):
	state = itm['IconButton1'].Checked
	print('[Button State] ', state)
	# disp.ExitLoop()
dlg.On.IconButton1.Clicked = _func

def _func(ev):
	state = itm['IconButton2'].Checked
	print('[Button State] ', state)
	# disp.ExitLoop()
dlg.On.IconButton2.Clicked = _func

def _func(ev):
	state = itm['IconButton3'].Checked
	print('[Button State] ', state)
	# disp.ExitLoop()
dlg.On.IconButton3.Clicked = _func

def _func(ev):
	state = itm['IconButton4'].Checked
	print('[Button State] ', state)
	# disp.ExitLoop()
dlg.On.IconButton4.Clicked = _func

dlg.Show()
disp.RunLoop()
dlg.Hide()

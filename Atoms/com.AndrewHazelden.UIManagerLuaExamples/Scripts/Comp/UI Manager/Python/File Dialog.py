# File Dialog.py 2022-06-16
# by Andrew Hazelden <andrew@andrewhazelden.com>

# Display a file dialog using Python + UI Manager. This is an alternative to relying on a legacy AskUser dialog which only works in the Fusion page inside of Resolve.


ui = fu.UIManager
disp = bmd.UIDispatcher(ui)

dlg = disp.AddWindow({'WindowTitle': 'Open File Dialog', 'ID': 'MyWin', 'Geometry': [100, 100, 500, 75],},[
	ui.VGroup({'Spacing': 0,},[
		# Add your GUI elements here:
		ui.HGroup({'Weight': 0.0,},[
			ui.Label({'ID': 'Label', 'Text': 'Filename', 'Weight': 0.1}),
			ui.LineEdit({'ID': 'FileLineTxt', 'Text': '', 'PlaceholderText': 'Please Enter a filepath', 'Weight': 0.9}),
			ui.Button({'ID': 'BrowseButton', 'Text': 'Browse', 'Geometry': [0, 0, 30, 50], 'Weight': 0.1}),
		]),
		ui.VGap(),
		ui.HGroup({'Weight': 0.1},[
			ui.Button({'ID': 'OpenButton', 'Text': 'Open File', 'Geometry': [0, 0, 30, 50], 'Weight': 0.1}),
		]),
	]),
])

itm = dlg.GetItems()

# The window was closed
def _func(ev):
	disp.ExitLoop()
dlg.On.MyWin.Close = _func

# Add your GUI element based event functions here:
def _func(ev):
	print('[Open File] Button Clicked')
	disp.ExitLoop()
dlg.On.OpenButton.Clicked = _func

def _func(ev):
	selectedPath = fu.RequestFile()
	if selectedPath:
		itm['FileLineTxt'].Text = str(selectedPath)
dlg.On.BrowseButton.Clicked = _func

dlg.Show()
disp.RunLoop()
dlg.Hide()

# Expand relative filepaths using the Fusion based "MapPath" function:
filepath = app.MapPath(itm['FileLineTxt'].Text or '')
# Alternatively you could expand comp file specific PathMaps using:
# filepath = comp.MapPath(itm['FileLineTxt'].Text)

print('\n\n[Open File]', filepath)

ui = fu.UIManager
disp = bmd.UIDispatcher(ui)

dlg = disp.AddWindow({"WindowTitle": "Tree", "ID": "MyWin", "Geometry": [100, 100, 430, 700], "Spacing": 0,},[
	ui.VGroup({"ID": "root",},[
		ui.Tree({
			"ID": "Tree",
			"SortingEnabled": True,
			"Events": {
				"CurrentItemChanged": True,
				"ItemActivated": True,
				"ItemClicked": True,
				"ItemDoubleClicked": True,
			},
		}),
	]),
])

itm = dlg.GetItems()

# The window was closed
def _func(ev):
	disp.ExitLoop()
dlg.On.MyWin.Close = _func

# Add your GUI element based event functions here:

# Add a header row
hdr = itm["Tree"].NewItem()

hdr.Text[0] = ""
hdr.Text[1] = "Column A"
hdr.Text[2] = "Column B"
hdr.Text[3] = "Column C"
hdr.Text[4] = "Column D"
hdr.Text[5] = "Column E"
itm["Tree"].SetHeaderItem(hdr)

# Number of columns in the Tree list
itm["Tree"].ColumnCount = 5

# Resize the Columns
itm["Tree"].ColumnWidth[0] = 100
itm["Tree"].ColumnWidth[1] = 75
itm["Tree"].ColumnWidth[2] = 75
itm["Tree"].ColumnWidth[3] = 75
itm["Tree"].ColumnWidth[4] = 75
itm["Tree"].ColumnWidth[5] = 75

# Add an new row entries to the list
for row in range(1,50):
	itRow = itm["Tree"].NewItem()

	# .format is used to create a leading zero padded row number like "Row A01" or "Row B01".
	itRow.Text[0] = "Row {0:02d}".format(row)
	
	itRow.Text[1] = "A {0:02d}".format(row)
	itRow.Text[2] = "B {0:02d}".format(row)
	itRow.Text[3] = "C {0:02d}".format(row)
	itRow.Text[4] = "D {0:02d}".format(row)
	itRow.Text[5] = "E {0:02d}".format(row)

	itm["Tree"].AddTopLevelItem(itRow)

# A Tree view row was clicked on
def _func(ev):
	print("[Single Clicked] " + str(ev["item"].Text[0]))
dlg.On.Tree.ItemClicked = _func

# A Tree view row was double clicked on
def _func(ev):
	print("[Double Clicked] " + str(ev["item"].Text[0]))
dlg.On.Tree.ItemDoubleClicked = _func

dlg.Show()
disp.RunLoop()
dlg.Hide()

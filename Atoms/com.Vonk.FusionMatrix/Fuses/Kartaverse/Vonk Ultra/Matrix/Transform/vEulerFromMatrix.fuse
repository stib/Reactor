-- ============================================================================
-- constants
-- ============================================================================
DATATYPE = "Text"
-- Don't know the proper format
-- REG_VERSION =
-- Menu structure it'll be displayed in
REGS_CATEGORY = "Kartaverse\\Vonk Ultra\\Matrix\\Transform"
-- Used to appear in "About" menu
REGS_COMPANY = "Vonk"
-- Used to appear in "About" menu
REGS_HELPTOPIC = "https://gitlab.com/AndrewHazelden/Vonk/"
-- Name displayed in menu
REGS_NAME = "vEulerFromMatrix"
-- Description displayed in "About" menu
REGS_OPDESCRIPTION = "Converting a matrix to Euler angles."
-- Name inbetween round brackets
REGS_OPICONSTRING = "EulerFromMatrix"
-- Name as it will appear in the raw comp file
TOOLS_REGID = "vEulerFromMatrix"
INP1_ICD_WIDTH = 0.25
INP1_MAXSCALE = 5

-- ============================================================================
-- modules
-- ============================================================================
local matrix = self and require("matrix") or nil
local matrixutils = self and require("vmatrixutils") or nil

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(TOOLS_REGID, CT_Tool, {
    --REG_Fuse_NoEdit = true,
    --REG_Fuse_NoReload = true,
    --REG_Fuse_TilePic = fuse_pic,
    --REG_NoBlendCtrls = true,
    REG_NoCommonCtrls = true,
    --REG_NoMotionBlurCtrls = true,
    REG_NoPreCalcProcess = true,  -- call Process for precalc requests (instead of PreCalcProcess)
    REG_OpNoMask = true,
    --REG_SupportsDoD = true,   -- this tool supports DoD
    --REG_TimeVariant = true,
    --REG_Unpredictable = true, -- this tool shall never be cached
    --REGID_DataType      = DATATYPE,
    --REGID_InputDataType = DATATYPE,
    REGS_Category = REGS_CATEGORY,
    REGS_Company = REGS_COMPANY,
    REGS_HelpTopic = REGS_HELPTOPIC,
    REGS_Name = REGS_NAME,
    REGS_OpDescription = REGS_OPDESCRIPTION,
    REGS_OpIconString = REGS_OPICONSTRING,
})

function Create()
    -- there is no sense in having a picker in this context
    self:BeginControlNest("Matrices (Txt)", "MatricesTxt", true, {LBLC_PickButton = false})

     InMatrix1 = self:AddInput("Input", "Input", {
        LINKID_DataType = "Text",
        INPID_InputControl = "TextEditControl",
        --TEC_Lines = 1,
        TEC_Wrap = true,
        --TEC_ReadOnly = false,
        LINK_Main = 1
    })

    self:EndControlNest()

    -- there is no sense in having a picker in this context
    -- these should be updated with each new calculation. Doesn't work as it should right now.
    self:BeginControlNest( "Translation", "Translation", true, {LBLC_PickButton = false})
        -- -- This doesn't do anything yet. Not applicable at this stage though.
        -- RotOrder = self:AddInput( "Rotation Order" , "RotOrder" , {
            -- LINKID_DataType = "Number",
            -- INPID_InputControl = "MultiButtonControl",
            -- INP_Default = 0,
            -- { MBTNC_AddButton = " XYZ " , },
            -- { MBTNC_AddButton = " XZY " , },
            -- { MBTNC_AddButton = " YXZ " , },
            -- { MBTNC_AddButton = " YZX " , },
            -- { MBTNC_AddButton = " ZXY " , },
            -- { MBTNC_AddButton = " ZYX " , },
            -- --INP_DoNotifyChanged = true,
        -- } )

        RotateX = self:AddInput("X Rotation", "Rotate.X", {
            LINKID_DataType = "Number",
            INP_Passive = true,
            INPID_InputControl = "ScrewControl",
            INP_Integer = false,
        })

        RotateY = self:AddInput("Y Rotation", "Rotate.Y", {
            LINKID_DataType = "Number",
            INP_Passive = true,
            INPID_InputControl = "ScrewControl",
            INP_Integer = false,
        })

        RotateZ = self:AddInput("Z Rotation", "Rotate.Z", {
            LINKID_DataType = "Number",
            INP_Passive = true,
            INPID_InputControl = "ScrewControl",
            INP_Integer = false,
        })
    self:EndControlNest()

    OutRotationX = self:AddOutput("X Rotation", "Rotate.X", {
        LINKID_DataType = "Number",
        LINK_Main = 1,
    })

    OutRotationY = self:AddOutput("Y Rotation", "Rotate.Y", {
        LINKID_DataType = "Number",
        LINK_Main = 2,
    })

    OutRotationZ = self:AddOutput("Z Rotation", "Rotate.Z", {
        LINKID_DataType = "Number",
        LINK_Main = 3,
    })
end

-- =============================================================================
-- utils
-- =============================================================================

-- =============================================================================
-- main
-- =============================================================================
-- function NotifyChanged(inp, param, time)
-- end

-- function Quaternion:Set(x,y,z,w)
	-- self.x = x or 0
	-- self.y = y or 0
	-- self.z = z or 0
	-- self.w = w or 0
-- end

-- function Quaternion:SetNormalize()
	-- local n = self.x * self.x + self.y * self.y + self.z * self.z + self.w * self.w

	-- if n ~= 1 and n > 0 then
		-- n = 1 / sqrt(n)
		-- self.x = self.x * n
		-- self.y = self.y * n
		-- self.z = self.z * n
		-- self.w = self.w * n
	-- end
-- end

-- local function MatrixToQuaternion(rot, quat)
    -- local trace = rot[1][1] + rot[2][2] + rot[3][3]

    -- if trace > 0 then
        -- local s = sqrt(trace + 1)
        -- quat.w = 0.5 * s
        -- s = 0.5 / s
        -- quat.x = (rot[3][2] - rot[2][3]) * s
        -- quat.y = (rot[1][3] - rot[3][1]) * s
        -- quat.z = (rot[2][1] - rot[1][2]) * s--]]
        -- quat:SetNormalize()
    -- else
        -- local i = 1
        -- local q = {0, 0, 0}

        -- if rot[2][2] > rot[1][1] then
            -- i = 2
        -- end

        -- if rot[3][3] > rot[i][i] then
            -- i = 3
        -- end

        -- local j = _next[i]
        -- local k = _next[j]

        -- local t = rot[i][i] - rot[j][j] - rot[k][k] + 1
        -- local s = 0.5 / sqrt(t)
        -- q[i] = s * t
        -- local w = (rot[k][j] - rot[j][k]) * s
        -- q[j] = (rot[j][i] + rot[i][j]) * s
        -- q[k] = (rot[k][i] + rot[i][k]) * s

        -- quat:Set(q[1], q[2], q[3], w)
        -- quat:SetNormalize()
    -- end
-- end

-- function Process(req)
    -- local mx_str = InMatrix1:GetValue(req).Value

    -- -- matrix from interchangeable string
    -- local mx = matrixutils.matrix_from_string(mx_str)

    -- -- We will compute the Euler angle values in radians
    -- -- and store them here :
    -- local h = 0
    -- local p = 0
    -- local b = 0
    -- local euler_x = 0
    -- local euler_y = 0
    -- local euler_z = 0
    -- local sp = mx[3][1]

    -- if math.abs( sp ) > 0.9999 then
        -- h = math.pi + math.asin( mx[3][1] )
        -- p = math.atan2( mx[3][2] / math.cos( h ) , mx[3][3] / math.cos( h ) )
        -- b = math.atan2( mx[2][1] / math.cos( h ) , mx[1][1] / math.cos( h ) )
    -- else
        -- b = 0
        -- if sp >= 1 then
            -- h = math.pi * 0.5
            -- p = b + math.atan2( mx[1][2] , mx[1][3] )
        -- else
            -- h = math.pi * -0.5
            -- p = -b + math.atan2( -mx[1][2] , -mx[1][3] )
        -- end
    -- end

    -- euler_x = math.deg(p)
    -- euler_y = math.deg(h)
    -- euler_z = math.deg(b)

    -- RotateX:SetSource(Number(euler_x), 0)
    -- OutRotationX:Set(req, euler_x)

    -- RotateY:SetSource(Number(euler_y), 0 )
    -- OutRotationY:Set(req, euler_y)

    -- RotateZ:SetSource(Number(euler_z), 0)
    -- OutRotationZ:Set(req, euler_z)

-- end

function Process(req)
    local mx_str = InMatrix1:GetValue(req).Value

    -- matrix from interchangeable string
    local mx = matrixutils.matrix_from_string(mx_str)

    -- We will compute the Euler angle values in radians
    -- and store them here :
    local h = 0
    local p = 0
    local b = 0
    local euler_x = 0
    local euler_y = 0
    local euler_z = 0

    -- Extract pitch from mx[3][2], being careful for domain errors with
    -- math.asin( ). We could have values slightly out of range due to
    -- floating point arithmetic.
    local sp = -mx[3][2]
    if sp <= -1.0 then
        p = -1.570796 -- -pi / 2
    elseif sp >= 1.0 then
        p = 1.570796 --  pi / 2
    else
        p = math.asin(sp)
    end

    -- Check for the Gimbal lock case , giving a slight tolerance
    -- for numerical imprecision
    if math.abs(sp) > 0.9999 then
        -- We are looking straight up or down .
        -- Slam bank to zero and just set heading
        b = 0.0
        h = math.atan2(-mx[1][3], mx[1][1])
    else
        -- Compute heading from mx[1][3] and mx[3][3]
        h = math.atan2(mx[3][1], mx[3][3])
        -- Compute bank from mx[2][1] and mx[2][2]
        b = math.atan2(mx[1][2], mx[2][2])
    end

    euler_x = math.deg(p)
    euler_y = math.deg(h)
    euler_z = math.deg(b)

    RotateX:SetSource(Number(euler_x), 0)
    OutRotationX:Set(req , euler_x)

    RotateY:SetSource(Number(euler_y), 0)
    OutRotationY:Set(req , euler_y)

    RotateZ:SetSource(Number(euler_z), 0)
    OutRotationZ:Set(req , euler_z)
end
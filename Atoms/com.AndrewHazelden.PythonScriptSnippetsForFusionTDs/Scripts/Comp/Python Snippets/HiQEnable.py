# You can enable the High Quality (HiQ) rendering mode using:

comp = fu.CurrentComp
comp.SetAttrs({'COMPB_HiQ' : True})

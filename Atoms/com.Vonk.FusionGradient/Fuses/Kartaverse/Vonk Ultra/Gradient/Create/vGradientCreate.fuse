-- Tip: If you turn on the vGradientCreate "Show Input" connection you can use
-- this node as a viewer to preview the output from a vGradientFromArray node.

-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vGradientCreate"
DATATYPE = "Gradient"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass("vGradientCreate", CT_Tool, {
    REGID_DataType = DATATYPE,
    REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\Gradient\\Create",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Create a Fusion Gradient object.",
    REGS_OpIconString = FUSE_NAME,
    -- Icon shown in the "Select Tool" dialog and the "Tile Picture"
    REGS_IconID = "Icons.Tools.Icons.Background",
})

function Create()
    self:RemoveControlPage("Controls")
    self:AddControlPage("Color", {CTID_DIB_ID  = "Icons.Tools.Tabs.Color"})

    InGradient = self:AddInput("Gradient", "Gradient", {
        LINKID_DataType = "Gradient",
        INPID_InputControl = "GradientControl",
        LINK_Main = 1
    })

    InInterpolation = self:AddInput("Gradient Interpolation Method", "GradientInterpolationMethod", {
        LINKID_DataType = "FuID",
        INPID_InputControl = "MultiButtonIDControl",
        { MBTNC_AddButton = "RGB", MBTNCID_AddID = "RGB", },
        { MBTNC_AddButton = "HLS", MBTNCID_AddID = "HLS", },
        { MBTNC_AddButton = "HSV", MBTNCID_AddID = "HSV", },
        { MBTNC_AddButton = "LAB", MBTNCID_AddID = "LAB", },
        MBTNC_StretchToFit = true,
        INP_DoNotifyChanged = true,
        INPID_DefaultID = "HLS",
    })

    InShowInput = self:AddInput("Show Input", "ShowInput", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    OutGradient = self:AddOutput("Output", "Output", {
        LINKID_DataType = "Gradient",
        LINK_Main = 1
    })
end

function NotifyChanged(inp, param, time)
    --[[
        Handles all input control events.

        :param inp: Input that triggered a signal.
        :type inp: Input

        :param param: Parameter object holding the (new) value.
        :type param: Parameter

        :param time: Current frame number.
        :type time: float
    ]]
    if inp == InInterpolation then
        InGradient:SetAttrs({GRDC_ColorSpace = param.Value})
    elseif inp == InShowInput then
        local visible
        if param.Value == 1.0 then visible = true else visible = false end
        InGradient:SetAttrs({LINK_Visible = visible})
    end
end

function Process(req) 
    local gradient = InGradient:GetValue(req)
    OutGradient: Set(req, gradient)
end

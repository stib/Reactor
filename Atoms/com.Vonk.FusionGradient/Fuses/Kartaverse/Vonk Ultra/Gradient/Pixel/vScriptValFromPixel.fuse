
-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vScriptValFromPixel"
DATATYPE = "ScriptVal"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = DATATYPE,
    REGID_InputDataType = "Image",
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\Gradient\\Pixel",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Return a ScriptVal with the RGB color from pixels in an image.",
    REGS_OpIconString = FUSE_NAME,
    -- Should the current time setting be cached?
    REG_TimeVariant = true,
    REG_Unpredictable = true,
    -- Icon shown in the "Select Tool" dialog and the "Tile Picture"
    REGS_IconID = "Icons.Tools.Icons.RunCommand",
})

function Create()
    self:RemoveControlPage("Controls")
    self:AddControlPage("Color", {CTID_DIB_ID  = "Icons.Tools.Tabs.Color"})

    InImage = self:AddInput("Input", "Input", {
        LINKID_DataType = "Image",
        LINK_Main = 1,
    })

    InSamplingMode = self:AddInput('Sampling Mode', 'SamplingMode', {
        LINKID_DataType     = 'Number',
        INPID_InputControl  = 'ComboControl',
        INP_Default         = 0,
        INP_Integer         = true,
        ICD_Width           = 1.0,
        IC_ControlPage = -1,
        CC_LabelPosition    = 'Horizontal',
        INP_DoNotifyChanged = true,
        {CCS_AddString     = 'Pixel'},
        {CCS_AddString     = 'Scanline Horizontal'},
        {CCS_AddString     = 'Scanline Vertical'},
        {CCS_AddString     = 'Rectangle'},
    })

    -- Pixel Sampling
    InPixel = self:AddInput("Pixel", "Pixel", {
        LINKID_DataType = "Point",
        LINK_Main = 2,
        INPID_InputControl = "OffsetControl",
        INPID_PreviewControl = 'CrosshairControl',
        INP_DoNotifyChanged = true,
        INP_DefaultX = 0.5,
        INP_DefaultY = 0.5,
    })

    InSamples = self:AddInput('Samples', 'Samples',{
        LINKID_DataType = 'Number',
        INPID_InputControl = 'SliderControl',
        INP_MinScale = 1.0,
        INP_MaxScale = 128.0,
        INP_MinAllowed = 1.0,
        INP_Default = 16.0,
        INP_Integer = true,
    })

    -- Rectangular Sampling
    InRectangularNest = self:BeginControlNest('Rectangular ', 'RectangularNest', true, {})
        InRectangularCenter = self:AddInput('Center', 'RectangularCenter', {
            LINKID_DataType = 'Point',
            INPID_InputControl = 'OffsetControl',
            INPID_PreviewControl = 'CrosshairControl',
            INP_DoNotifyChanged = true,
        })
        InRectangularWidth = self:AddInput('Width', 'RectangularWidth',{
            LINKID_DataType = 'Number',
            INPID_InputControl = 'SliderControl',
            INPID_PreviewControl = 'RectangleControl',
            PC_ControlGroup = 2,
            PC_ControlID = 0,
            INP_MinScale = 0.0,
            INP_MaxScale = 1.0,
            INP_MinAllowed = 0.0,
            INP_MaxAllowed = 1.0,
            INP_Default = 1.0,
        })
        InRectangularHeight = self:AddInput('Height', 'RectangularHeight',{
            LINKID_DataType = 'Number',
            INPID_InputControl = 'SliderControl',
            INPID_PreviewControl = 'RectangleControl',
            PC_ControlGroup = 2,
            PC_ControlID = 1,
            INP_MinScale = 0.0,
            INP_MaxScale = 1.0,
            INP_MinAllowed = 0.0,
            INP_MaxAllowed = 1.0,
            INP_Default = 1.0,
        })
        InHorizontalSamples = self:AddInput('Horizontal Samples', 'HorizontalSamples',{
            LINKID_DataType = 'Number',
            INPID_InputControl = 'SliderControl',
            INP_MinScale = 1.0,
            INP_MaxScale = 128.0,
            INP_Default = 16.0,
            INP_Integer = true,
        })
        InVerticalSamples = self:AddInput('Vertical Samples', 'VerticalSamples',{
            LINKID_DataType = 'Number',
            INPID_InputControl = 'SliderControl',
            INP_MinScale = 1.0,
            INP_MaxScale = 128.0,
            INP_Default = 16.0,
            INP_Integer = true,
        })
    self:EndControlNest()

    -- Rectangle
    InRectangularWidth:SetAttrs({
        RCP_Center = InRectangularCenter,
    })
    InRectangularHeight:SetAttrs({
        RCP_Center = InRectangularCenter,
    })

    InShowInput = self:AddInput("Show Input", "ShowInput", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    OutScriptVal = self:AddOutput("ScriptVal", "ScriptVal", {
        LINKID_DataType = "ScriptVal",
        LINK_Main = 1
    })
end

function NotifyChanged(inp, param, time)
    --[[
        Handles all input control events.

        :param inp: Input that triggered a signal.
        :type inp: Input

        :param param: Parameter object holding the (new) value.
        :type param: Parameter

    ]]
    if inp == InShowInput then
        local visible
        if param.Value == 1.0 then visible = true else visible = false end
        InPixel:SetAttrs({LINK_Visible = visible})
    elseif inp == InSamplingMode then
        if param.Value == 0 then
            -- Pixel
            InPixel:SetAttrs({IC_Visible = true, PC_Visible = true})
            InSamples:SetAttrs({IC_Visible = false, PC_Visible = false})

            InRectangularNest:SetAttrs({IC_Visible = false})
            InRectangularCenter:SetAttrs({IC_Visible = false, PC_Visible = false})
            InRectangularWidth:SetAttrs({IC_Visible = false, PC_Visible = false})
            InRectangularHeight:SetAttrs({IC_Visible = false, PC_Visible = false})
            InHorizontalSamples:SetAttrs({IC_Visible = false, PC_Visible = false})
            InVerticalSamples:SetAttrs({IC_Visible = false, PC_Visible = false})
        elseif param.Value == 1 or param.Value == 2 then
            -- Scanline horizontal or vertical
            InPixel:SetAttrs({IC_Visible = true, PC_Visible = true})
            InSamples:SetAttrs({IC_Visible = true, PC_Visible = true})

            InRectangularNest:SetAttrs({IC_Visible = false})
            InRectangularCenter:SetAttrs({IC_Visible = false, PC_Visible = false})
            InRectangularWidth:SetAttrs({IC_Visible = false, PC_Visible = false})
            InRectangularHeight:SetAttrs({IC_Visible = false, PC_Visible = false})
            InHorizontalSamples:SetAttrs({IC_Visible = false, PC_Visible = false})
            InVerticalSamples:SetAttrs({IC_Visible = false, PC_Visible = false})
        elseif param.Value == 3 then
            -- Rectangle
            InPixel:SetAttrs({IC_Visible = false, PC_Visible = false})
            InSamples:SetAttrs({IC_Visible = false, PC_Visible = false})

            InRectangularNest:SetAttrs({IC_Visible = true})
            InRectangularCenter:SetAttrs({IC_Visible = true, PC_Visible = true})
            InRectangularWidth:SetAttrs({IC_Visible = true, PC_Visible = true})
            InRectangularHeight:SetAttrs({IC_Visible = true, PC_Visible = true})
            InHorizontalSamples:SetAttrs({IC_Visible = true, PC_Visible = true})
            InVerticalSamples:SetAttrs({IC_Visible = true, PC_Visible = true})
        end
    end
end

--function CheckRequest(req)
--   if (req:GetPri() == 0 ) then
--      req:ClearInputFlags( InForeground , REQF_PreCalc)
--   end
--end

function Process(req)
    local tbl = {}
    local mode = InSamplingMode:GetValue(req).Value
    local img = InImage:GetValue(req)

    --if not req:IsPreCalc() then
        if mode == 0 then
            -- point sampling
            local point = InPixel:GetSource(req.Time)

            local x = point.X * img.Width
            local y = point.Y * img.Height

            local color = Pixel()
            sx = math.min((math.max(x, 0)), img.Width - 1)
            sy = math.min((math.max(y, 0)), img.Height - 1)
            img:GetPixel(sx, sy, color)
            table.insert(tbl, {P = tonumber(1), R = tonumber(color.R), G = tonumber(color.G), B = tonumber(color.B), A = tonumber(color.A)})
        elseif mode == 1 then
            -- Scanline horizontal sampling
            local point = InPixel:GetSource(req.Time)
            local samples = InSamples:GetValue(req).Value
            local step = math.ceil((img.Width - 1) / samples)

            local y = point.Y * img.Height
            y = math.min((math.max(y, 0)), img.Height - 1)

            local counter = 0
            for x = 0, (img.Width - 1), step do
                counter = counter + 1
                local color = Pixel()
                sx = math.min((math.max(x, 0)), img.Width - 1)
                sy = math.min((math.max(y, 0)), img.Height - 1)
                img:GetPixel(sx, sy, color)
                table.insert(tbl, {P = tonumber((counter - 1) / samples), R = tonumber(color.R), G = tonumber(color.G), B = tonumber(color.B), A = tonumber(color.A)})
            end
        elseif mode == 2 then
            -- Scanline vertical sampling
            local point = InPixel:GetSource(req.Time)
            local samples = InSamples:GetValue(req).Value
            local step = math.ceil((img.Height - 1) / samples)

            local x = point.X * img.Width
            x = math.min((math.max(x, 0)), img.Width - 1)

            local counter = 0
            for y = 0, (img.Height - 1), step do
                counter = counter + 1
                local color = Pixel()
                sx = math.min((math.max(x, 0)), img.Width - 1)
                sy = math.min((math.max(y, 0)), img.Height - 1)
                img:GetPixel(sx, sy, color)
                table.insert(tbl, {P = tonumber((counter - 1) / samples), R = tonumber(color.R), G = tonumber(color.G), B = tonumber(color.B), A = tonumber(color.A)})
            end
        elseif mode == 3 then
            local rectCenter = InRectangularCenter:GetSource(req.Time)
            local rectWidth = InRectangularWidth:GetValue(req).Value
            local rectHeight = InRectangularHeight:GetValue(req).Value
            local hSamples = InHorizontalSamples:GetValue(req).Value
            local vSamples = InVerticalSamples:GetValue(req).Value

            -- Center in pixels
            rCX = math.min((math.max(rectCenter.X * img.Width, 0)), img.Width - 1)
            rCY = math.min((math.max(rectCenter.Y * img.Height, 0)), img.Height - 1)
            rWidth = rectWidth * (img.Width - 1)
            rHeight = rectHeight * (img.Height - 1)

            rTopLeft = {
                X = rCX - (rWidth * 0.5),
                Y = rCY - (rHeight * 0.5)
            }
            rTopRight = {
                X = rCX + (rWidth * 0.5),
                Y = rCY - (rHeight * 0.5)
            }
            rBottomLeft = {
                X = rCX - (rWidth * 0.5),
                Y = rCY + (rHeight * 0.5)
            }
            rBottmRight = {
                X = rCX + (rWidth * 0.5),
                Y = rCY + (rHeight * 0.5)
            }

            local hStep = math.ceil((rTopRight.X - rTopLeft.X) / hSamples)
            local vStep = math.ceil((rBottomLeft.Y - rTopLeft.Y) / vSamples)

            local totalColors = 0
            for x = rTopLeft.X, rTopRight.X, hStep do
                for y = rTopLeft.Y, rBottomLeft.Y, vStep do
                    totalColors = totalColors + 1
                end
            end

            local counter = 0
            for x = rTopLeft.X, rTopRight.X, hStep do
                for y = rTopLeft.Y, rBottomLeft.Y, vStep do
                    counter = counter + 1

                    local color = Pixel()
                    sx = math.min((math.max(x, 0)), img.Width - 1)
                    sy = math.min((math.max(y, 0)), img.Height - 1)
                    img:GetPixel(sx, sy, color)

                    table.insert(tbl, {P = tonumber((counter - 1) / totalColors), R = tonumber(color.R), G = tonumber(color.G), B = tonumber(color.B), A = tonumber(color.A)})
                end
            end
        end
    --end


    OutScriptVal:Set(req, ScriptValParam(tbl))
end
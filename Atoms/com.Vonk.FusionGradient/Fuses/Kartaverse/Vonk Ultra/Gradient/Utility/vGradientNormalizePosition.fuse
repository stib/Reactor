-- ============================================================================
-- modules
-- ============================================================================

-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vGradientNormalizePosition"
DATATYPE = "Gradient"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = DATATYPE,
    REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\Gradient\\Utility",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Normalizes the position of colors in a Gradient to a uniform spacing from 0-1.",
    REGS_OpIconString = FUSE_NAME,
    -- Icon shown in the "Select Tool" dialog and the "Tile Picture"
    REGS_IconID = "Icons.Tools.Icons.Background",
})

function Create()
    -- [[ Creates the user interface. ]]
    self:RemoveControlPage("Controls")
    self:AddControlPage("Color", {CTID_DIB_ID  = "Icons.Tools.Tabs.Color"})

    InGradient = self:AddInput("Gradient", "Gradient", {
        LINKID_DataType = "Gradient",
        LINK_Main = 1
    })

    InShowDump = self:AddInput("Show Dump", "ShowDump", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    OutGradient = self:AddOutput("Output", "Output", {
        LINKID_DataType = "Gradient",
        LINK_Main = 1
    })
end

function Process(req)
    -- [[ Creates the output. ]]
    local gradient = InGradient:GetValue(req)
    local show_dump = InShowDump:GetValue(req).Value
    local tbl = {}

    -- Get the number of color entries in the gradient
    local totalColors = gradient:GetColorCount() - 1

    for index = 0, totalColors, 1 do
        local color = gradient:GetColor(index)
        table.insert(tbl, {P = tonumber(index / totalColors), R = tonumber(color.R), G = tonumber(color.G), B = tonumber(color.B), A = tonumber(color.A)})
        -- dump({P = tonumber(index / totalColors), R = tonumber(color.R), G = tonumber(color.G), B = tonumber(color.B), A = tonumber(color.A)})
    end

    if show_dump == 1 then
        print("\n----------------------")
        print("[" .. tostring(self.Name) .. "]")
        local txt_str = bmd.writestring(tbl)
        dump(txt_str)
    end

    local gradient = Gradient()
    -- Generate the new gradient output
    local outTotalColors = #tbl - 1
    for key, value in ipairs(tbl) do
        gradient:AddColor(((key - 1) / outTotalColors), Pixel({R = tonumber(value.R), G = tonumber(value.G), B = tonumber(value.B), A = tonumber(value.A)}))
    end

    OutGradient: Set(req, gradient)
end

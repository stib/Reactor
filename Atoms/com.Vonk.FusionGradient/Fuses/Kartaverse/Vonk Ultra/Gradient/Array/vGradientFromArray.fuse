-- JSON Array Input Example:
-- {"size":4,"array":["0.7","0.5","0.23","0.29"]}

-- Gradient Learning Resources:
-- https://www.steakunderwater.com/VFXPedia/96.0.243.189/index7a6b.html?title=Third_Party_Fuses/Gradient_Tutorial
-- https://www.steakunderwater.com/wesuckless/viewtopic.php?t=3288

-- ============================================================================
-- modules
-- ============================================================================
local jsonutils = self and require("vjsonutils") or nil

-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vGradientFromArray"
DATATYPE = "Gradient"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = DATATYPE,
    REGID_InputDataType = "Text",
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\Gradient\\Array",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Create a Gradient from an array.",
    REGS_OpIconString = FUSE_NAME,
    -- Icon shown in the "Select Tool" dialog and the "Tile Picture"
    REGS_IconID = "Icons.Tools.Icons.Background",
})

function Create()
    self:RemoveControlPage("Controls")
    self:AddControlPage("Color", {CTID_DIB_ID  = "Icons.Tools.Tabs.Color"})

    InData = self:AddInput("Input", "Input", {
        LINKID_DataType = "Text",
        INPID_InputControl = "TextEditControl",
        TEC_Lines = 5,
        LINK_Main = 1
    })

    InShowInput = self:AddInput("Show Input", "ShowInput", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 1.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    OutGradient = self:AddOutput("Output", "Output", {
        LINKID_DataType = "Gradient",
        LINK_Main = 1
    })
end

function NotifyChanged(inp, param, time)
    --[[
        Handles all input control events.

        :param inp: Input that triggered a signal.
        :type inp: Input

        :param param: Parameter object holding the (new) value.
        :type param: Parameter

        :param time: Current frame number.
        :type time: float
    ]]
    if inp == InShowInput then
        local visible
        if param.Value == 1.0 then visible = true else visible = false end
        InData:SetAttrs({LINK_Visible = visible})
    end
end

function Process(req)
    local json_string = InData:GetValue(req).Value

    local value = nil
    local gradient = Gradient()
    local position = 0
    local offset = 0
    local size = 0

    local json_table = jsonutils.decode(json_string)
    value = jsonutils.get(json_table, "array")
    size = tonumber(jsonutils.get(json_table, "size"))

    if size > 1 then
        offset = 1 / (size - 1)
    end

    for i = 1, size, 1 do
        gradient:AddColor(position , Pixel({R = tonumber(value[i]), G = tonumber(value[i]), B = tonumber(value[i]) , A = tonumber(value[i])}))
        position = position + offset
    end

    OutGradient:Set(req, gradient)
end

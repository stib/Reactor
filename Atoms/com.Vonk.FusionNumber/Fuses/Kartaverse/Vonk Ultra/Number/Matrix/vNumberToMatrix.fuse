-- ============================================================================
-- modules
-- ============================================================================
local matrixutils = self and require("vmatrixutils") or nil
local matrix = self and require("matrix") or nil

-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vNumberToMatrix"
DATATYPE = "Number"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = DATATYPE,
    REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\Number\\Matrix",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Returns a matrix from a number.",
    REGS_OpIconString = FUSE_NAME
})

function Create()
    -- [[ Creates the user interface. ]]
    InMatrix = self:AddInput("Matrix", "Matrix", {
        LINKID_DataType = "Text",
        INPID_InputControl = "TextEditControl",
        TEC_Wrap = true,
        LINK_Main = 1
    })

    InRow = self:AddInput("Row", "Row", {
        LINKID_DataType = "Number",
        INPID_InputControl = "SliderControl",
        INP_Default = 1,
        INP_Integer = true,
        INP_MinScale = 1,
        INP_MaxScale = 4,
        INP_MinAllowed = 1,
        LINK_Main = 2
    })

    InColumn = self:AddInput("Column", "Column", {
        LINKID_DataType = "Number",
        INPID_InputControl = "SliderControl",
        INP_Default = 1,
        INP_Integer = true,
        INP_MinScale = 1,
        INP_MaxScale = 4,
        INP_MinAllowed = 1,
        LINK_Main = 3
    })

    InNumber = self:AddInput("Number", "Number", {
        LINKID_DataType = "Number",
        INPID_InputControl = "SliderControl",
        INP_Default = 0,
        INP_Integer = false,
        --INP_MinScale = 1,
        --INP_MaxScale = 4,
        --INP_MinAllowed = 1,
        LINK_Main = 4
    })
    
    InShowInput = self:AddInput("Show Input", "ShowInput", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    OutMatrix = self:AddOutput("Output", "Output", {
        LINKID_DataType = "Text",
        LINK_Main = 1
    })
end

function NotifyChanged(inp, param, time)
    --[[
        Handles all input control events.

        :param inp: Input that triggered a signal.
        :type inp: Input

        :param param: Parameter object holding the (new) value.
        :type param: Parameter

        :param time: Current frame number.
        :type time: float
    ]]
    if inp == InShowInput then
        local visible
        if param.Value == 1.0 then visible = true else visible = false end
        InRow:SetAttrs({LINK_Visible = visible})
        InColumn:SetAttrs({LINK_Visible = visible})
        InNumber:SetAttrs({LINK_Visible = visible})
    end
end

function Process(req)
    -- [[ Creates the output. ]]
    local mat_str = InMatrix:GetValue(req).Value
    local row = InRow:GetValue(req).Value
    local col = InColumn:GetValue(req).Value
    local n = InNumber:GetValue(req).Value

    -- build matrix from interchangeable string format
    local mat = matrixutils.matrix_from_string(mat_str)

    -- validate row in matrix
    local mat_rows = table.getn(mat)
    if row > mat_rows then
        error(string.format("row %d not in matrix rows of size %d", row, mat_rows))
    end

    -- validate column in matrix
    local mat_cols = table.getn(mat[row])
    if col > mat_cols then
        error(string.format("column %d not in matrix columns of size %d", row, mat_cols))
    end

    -- set number
    mat[row][col] = n

    local mx_output = matrixutils.matrix_to_string(mat)

    local out = Text(mx_output)
    OutMatrix:Set(req, out)
end

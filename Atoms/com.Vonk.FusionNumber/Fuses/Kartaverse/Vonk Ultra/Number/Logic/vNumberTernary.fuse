-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vNumberTernary"
DATATYPE = "Number"
--BTN_WIDTH = 0.33333
BTN_WIDTH = 0.5

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = DATATYPE,
    REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\Number\\Logic",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Compare a value and return one of two possible results",
    REGS_OpIconString = FUSE_NAME
})

function Create()
    -- [[ Creates the user interface. ]]
    InTerm1 = self:AddInput("Term 1", "Term1", {
        IC_Steps = 201,
        INPID_InputControl = "SliderControl",
        INP_Default = 0,
        INP_MaxScale = 100,
        INP_MinScale = -100,
        INP_MinAllowed = -1e+38,
        INP_MaxAllowed = 1e+38,
        -- INP_MinAllowed = -1000000,
        -- INP_MaxAllowed = 1000000,
        LINKID_DataType = "Number",
        LINK_Visible = 1,
        LINK_Main = 1,
    })

    InTerm1Separator = self:AddInput("Term1Separator", "Term1Separator", {
        IC_Visible = true,
        INPID_InputControl = "SeparatorControl",
        INP_External = false,
    })

    InTrue = self:AddInput("True", "True", {
        IC_Steps = 201,
        INPID_InputControl = "SliderControl",
        INP_Default = 0,
        INP_MaxScale = 100,
        INP_MinScale = -100,
        LINKID_DataType = "Number",
        LINK_Main = 2,
    })

    InFalse = self:AddInput("False", "False", {
        IC_Steps = 201,
        INPID_InputControl = "SliderControl",
        INP_Default = 0,
        INP_MaxScale = 100,
        INP_MinScale = -100,
        LINKID_DataType = "Number",
        LINK_Main = 3,
    })

    InShowInput = self:AddInput("Show Input", "ShowInput", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    Output = self:AddOutput("Output", "Output", {
        LINKID_DataType = "Number",
        LINK_Main = 1,
    })
end

function NotifyChanged(inp, param, time)
    --[[
        Handles all input control events.

        :param inp: Input that triggered a signal.
        :type inp: Input

        :param param: Parameter object holding the (new) value.
        :type param: Parameter

        :param time: Current frame number.
        :type time: float
    ]]

    if inp == InShowInput then
        local visible
        if param.Value == 1.0 then
            visible = true
        else
            visible = false
        end

        -- InTerm1:SetAttrs({LINK_Visible = visible})
        InTrue:SetAttrs({LINK_Visible = visible})
        InFalse:SetAttrs({LINK_Visible = visible})
    end
end

function Process(req)
    -- [[ Creates the output. ]]
    local n1 = tonumber(InTerm1:GetValue(req).Value)

    local result
    if n1 ~= nil and n1 >= 1 then
        -- 1, or any other postive number
        result = tonumber(InTrue:GetValue(req).Value)
    else
        -- 0 or nil
        result = tonumber(InFalse:GetValue(req).Value)
    end

    local out = Number(result)

    Output:Set(req, out)
end

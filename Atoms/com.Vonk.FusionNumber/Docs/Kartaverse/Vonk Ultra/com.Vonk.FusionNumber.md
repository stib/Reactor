# Vonk Ultra - Number

Number is a node based math arithmetic library for Blackmagic Design Fusion.

## Acknowledgements

- Kristof Indeherberge
- Cédric Duriau

## Contents

**Fuses**

- `vNumberCreate.fuse`: Fuse to create a Fusion Number object.
- `vNumberAdd.fuse`: Fuse to calculate the sum of two numbers.
- `vNumberSubtract.fuse`: Fuse to calculate the difference of two numbers.
- `vNumberMultiply.fuse`: Fuse to calculate the product of two numbers.
- `vNumberDivide.fuse`: Fuse to calculate the quotient of two numbers.
- `vNumberPower.fuse`: Fuse to calculate the power of a number.
- `vNumberSquareRoot.fuse`: Fuse to calculate the square root of a number.
- `vNumberCeil.fuse`: Fuse to calculate the integer no greater than a number.
- `vNumberFloor.fuse`: Fuse to calculate the integer no less than a number.
- `vNumberSine.fuse`: Fuse to calculate the sine of a number in radians.
- `vNumberCosine.fuse`: Fuse to calculate the cosine of a number in radians.
- `vNumberTangent.fuse`: Fuse to calculate the sine of a number in radians.
- `vNumberArcSine.fuse`: Fuse to calculate the inverse sine of a number in radians.
- `vNumberArcCosine.fuse`: Fuse to calculate the inverse cosine of a number in radians.
- `vNumberArcTangent.fuse`: Fuse to calculate the inverse tangent of a number in radians.
- `vNumberDegreesToRadians.fuse`: Fuse to calculate the radian value of a number in degrees.
- `vNumberRadiansToDegrees.fuse`: Fuse to calculate the degree value of a number in radians.

--[[-- 
Copyright (c) 2020,  Emilio Sapia
https://emiliosapia.myportfolio.com
Written by : Emilio Sapia 
Written on : Oct, 2020
version 1.1

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
--]]--

{
	Tools = ordered() {
		ml_FastChroma = MacroOperator {
			CtrlWZoom = false,
			Inputs = ordered() {
				CommentsNest = Input { Value = 0, },
				Comments = Input { Value = "FastChroma v 1.0\nMacro by Emilio Sapia - Millolab\nhttps://emiliosapia.myportfolio.com", },
				Position = InstanceInput {
					SourceOp = "CH_1",
					Source = "OffsetX",
					Name = "Position",
					Page = "Controls",
					DefaultX = 0.5,
					DefaultY = 0.5,
				},
				Size = InstanceInput {
					SourceOp = "CH_1",
					Source = "Control",
					Name = "Size",
					MinScale = 1,
					MaxScale = 1.05,
					Page = "Controls",
					Default = 1,
				},
				Edges = InstanceInput {
					SourceOp = "CH_1",
					Source = "Edges",
					Default = 3,
				},
				Blank2 = InstanceInput {
					SourceOp = "Mask",
					Source = "Blank5",
				},
				Process = InstanceInput {
					SourceOp = "CH_1",
					Source = "ProcessRed",
					Name = "Channel Split A",
					ControlGroup = 3,
					Default = 1,
				},
				ProcessGreen = InstanceInput {
					SourceOp = "CH_1",
					Source = "ProcessGreen",
					ControlGroup = 3,
					Default = 0,
				},
				ProcessBlue = InstanceInput {
					SourceOp = "CH_1",
					Source = "ProcessBlue",
					ControlGroup = 3,
					Default = 0,
				},
				ProcessB = InstanceInput {
					SourceOp = "CH_2",
					Source = "ProcessRed",
					Name = "Channel Split B",
					ControlGroup = 4,
					Default = 1,
				},
				ProcessGreenB = InstanceInput {
					SourceOp = "CH_2",
					Source = "ProcessGreen",
					ControlGroup = 4,
					Default = 0,
				},
				ProcessBlueB = InstanceInput {
					SourceOp = "CH_2",
					Source = "ProcessBlue",
					ControlGroup = 4,
					Default = 0,
				},
				Input = InstanceInput {
					SourceOp = "PipeRouter1",
					Source = "Input",
					Page = "Common",
				},
				Input12 = InstanceInput {
					SourceOp = "Mask",
					Source = "Blend",
					Default = 1,
				},
				Input13 = InstanceInput {
					SourceOp = "Mask",
					Source = "Blank1",
				},
				Input14 = InstanceInput {
					SourceOp = "Mask",
					Source = "ApplyMaskInverted",
					Default = 0,
				},
				Input15 = InstanceInput {
					SourceOp = "Mask",
					Source = "MultiplyByMask",
					Default = 0,
				},
				Input16 = InstanceInput {
					SourceOp = "Mask",
					Source = "FitMask",
				},
				Input17 = InstanceInput {
					SourceOp = "Mask",
					Source = "Blank2",
				},
				Input18 = InstanceInput {
					SourceOp = "Mask",
					Source = "MaskChannel",
					Default = 3,
				},
				Input19 = InstanceInput {
					SourceOp = "Mask",
					Source = "MaskLow",
					ControlGroup = 14,
					Default = 0,
				},
				Input20 = InstanceInput {
					SourceOp = "Mask",
					Source = "MaskHigh",
					ControlGroup = 14,
					Default = 1,
				},
				Input21 = InstanceInput {
					SourceOp = "Mask",
					Source = "MaskClipBlack",
					Name = "Black",
					Default = 1,
				},
				Input22 = InstanceInput {
					SourceOp = "Mask",
					Source = "MaskClipWhite",
					Name = "White",
					Default = 1,
				},
				Input23 = InstanceInput {
					SourceOp = "Mask",
					Source = "Blank4",
				},
				Input24 = InstanceInput {
					SourceOp = "Mask",
					Source = "UseObject",
					Default = 0,
				},
				Input25 = InstanceInput {
					SourceOp = "Mask",
					Source = "UseMaterial",
					Default = 0,
				},
				Input26 = InstanceInput {
					SourceOp = "Mask",
					Source = "CorrectEdges",
				},
				Input27 = InstanceInput {
					SourceOp = "Mask",
					Source = "ObjectID",
					ControlGroup = 21,
					Default = 0,
				},
				Input28 = InstanceInput {
					SourceOp = "Mask",
					Source = "MaterialID",
					ControlGroup = 21,
					Default = 0,
				},
				Input29 = InstanceInput {
					SourceOp = "Mask",
					Source = "Blank5",
				},
				EffectMask = InstanceInput {
					SourceOp = "Mask",
					Source = "EffectMask",
				}
			},
			Outputs = {
				MainOutput1 = InstanceOutput {
					SourceOp = "Mask",
					Source = "Output",
				}
			},
			ViewInfo = GroupInfo {
				Pos = { 356.023, 72.8182 },
				Flags = {
					AllowPan = false,
					ConnectedSnap = true,
					AutoSnap = true,
					RemoveRouters = true
				},
				Size = { 279.914, 332.121, 156.914, 24.2424 },
				Direction = "Horizontal",
				PipeStyle = "Direct",
				Scale = 1,
				Offset = { -116.867, 0 }
			},
			Tools = ordered() {
				CH_1 = Transform {
					CtrlWZoom = false,
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						ProcessGreen = Input { Value = 0, },
						ProcessBlue = Input { Value = 0, },
						ProcessAlpha = Input { Value = 0, },
						Center = Input { Expression = "OffsetX", },
						Size = Input { Expression = "Control", },
						Edges = Input { Value = 3, },
						FilterMethod = Input { Value = 1, },
						FlattenTransform = Input { Value = 1, },
						Input = Input {
							SourceOp = "PipeRouter1",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { 33.3956, 75.6289 } },
					UserControls = ordered() {
						Control = {
							INP_MaxAllowed = 1000000,
							INP_Integer = false,
							INPID_InputControl = "SliderControl",
							INP_MaxScale = 1.5,
							INP_Default = 1,
							INP_MinScale = 0.5,
							INP_MinAllowed = 0,
							LINKID_DataType = "Number",
							ICS_ControlPage = "Controls",
							ICD_Center = 1,
							LINKS_Name = "Control"
						},
						OffsetX = {
							INP_DefaultX = 0.5,
							INPID_PreviewControl = "CrosshairControl",
							INP_DefaultY = 0.5,
							LINKID_DataType = "Point",
							ICS_ControlPage = "Controls",
							INPID_InputControl = "OffsetControl",
							CHC_Style = "NormalCross",
							LINKS_Name = "OffsetX"
						}
					}
				},
				PipeRouter1 = PipeRouter {
					CtrlWShown = false,
					ViewInfo = PipeRouterInfo { Pos = { 60, 8.12119 } },
				},
				Mask = Merge {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						Background = Input {
							SourceOp = "BrightnessContrast1",
							Source = "Output",
						},
						Foreground = Input {
							SourceOp = "DirectionalBlur1",
							Source = "Output",
						},
						FlattenTransform = Input { Value = 1, },
						PerformDepthMerge = Input { Value = 0, },
					},
					ViewInfo = OperatorInfo { Pos = { 153.396, 261.659 } },
				},
				DirectionalBlur1 = DirectionalBlur {
					CtrlWShown = false,
					Inputs = {
						Type = Input { Value = 3, },
						Length = Input { Expression = "CH_1.Size-1", },
						Input = Input {
							SourceOp = "CH_2",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { 33.3956, 261.659 } },
				},
				CH_2 = Transform {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						ProcessRed = Input { Value = 0, },
						ProcessGreen = Input { Value = 0, },
						ProcessAlpha = Input { Value = 0, },
						Center = Input { Expression = "CH_1.Center", },
						Pivot = Input { Expression = "CH_1.Pivot", },
						Size = Input { Expression = "CH_1.Size", },
						Angle = Input { Expression = "CH_1.Angle", },
						Edges = Input {
							Value = 3,
							Expression = "CH_1.Edges",
						},
						FilterMethod = Input { Value = 1, },
						InvertTransform = Input { Value = 1, },
						FlattenTransform = Input { Value = 1, },
						Input = Input {
							SourceOp = "CH_1",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { 33.3956, 125.871 } },
				},
				BrightnessContrast1 = BrightnessContrast {
					CtrlWShown = false,
					Inputs = {
						Alpha = Input { Value = 1, },
						Gain = Input { Value = 0, },
						Input = Input {
							SourceOp = "PipeRouter1",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { 153.396, 132.819 } },
				}
			},
		}
	},
	ActiveTool = "ml_FastChroma"
}
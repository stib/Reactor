---@diagnostic disable: param-type-mismatch

ScriptVersion = "<b><i>2.0.6</b></i>"
--ChangeStrings
--last updated on 02.12.2022 by Noah Hähnel
--created by Noah Hähnel


local ui = fu.UIManager
local disp = bmd.UIDispatcher(ui)


	print('_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _')
	print(' ')
	print('Change Strings Opened.')
	print('_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _')
	print(' ')

--checks to see if Fusion or Resolve is used
host = fusion:MapPath('Fusion:/')

local platform = jit.os

if string.lower(host):match('resolve') then
	Fusion_S = false
	print('Working in DaVinci Resolve')
	resolve = Resolve()
	pm = resolve:GetProjectManager()
else
	Fusion_S = true
	print('Working in Fusion Standalone')

end


--retrieve old data for saved settings. 
if fusion:GetPrefs('Comp.ChangeStrings') then
OldData = fusion:GetPrefs('Comp.ChangeStrings')
end

PatternBreaker = 'ïèëï'
Limit = 30
Debugging = false
SearchForTable = {}
ReplaceWithTable = {}


if OldData then
	if OldData.OldDoDebugging == 'Checked' then
		Debugging = true
	else
		Debugging = false
	end

	if OldData.OldPatternBreakerInput ~= '' then
		PatternBreaker = OldData.OldPatternBreakerInput
	end
	if OldData.OldPatternBreakerInput == nil then
		PatternBreaker = 'ïèëï'
	end

	if OldData.OldMaxLimit ~= '' then
		Limit = tonumber(OldData.OldMaxLimit)
	end
	if OldData.OldMaxLimit == nil then
		Limit = 30
	end

	if OldData.OldMaxComboLimit then
		MaxComboLimit = OldData.OldMaxComboLimit
	else
		MaxComboLimit = 4
	end

	if OldData.OldSearchForHistory then
		SearchForTable = OldData.OldSearchForHistory
	else
		SearchForTable = {}
		print("No Search History found.")
	end
	if OldData.OldReplaceWithHistory then
		ReplaceWithTable = OldData.OldReplaceWithHistory
	else
		ReplaceWithTable = {}
		print("No Replace History found.")
	end
end


if Debugging == true then
	print('Prints actions to the console')
else
	print('')
end


if PatternBreaker == 'ïèëï' then
	print('Pattern Breaker is the default: ', PatternBreaker)
else
	print('Pattern Breaker is the custom: ', PatternBreaker)
end

if Limit == 30 then
	print('Limit is the default: ', Limit)
else
	print('Limit is the custom: ', Limit)
end

if MaxComboLimit then
	print("Search and Replace History saves up to: ".. MaxComboLimit+1 .. " items" )
end

if OldData == nil then
	print('No previous Settings found in this Composition file')

end





------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------
--New Search and Replace function. It returns the string with the replaced parts and pattern breaker (if spaceSplitter is true or the search string is found in the replace string)
------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------
function SearchAndReplace(mainString, searchString, replaceString, caseSensitive, spaceSplitter)
	local patternStart
	local patternEnd
	local repeatCounter = 0
	local searchStart = 0
	local previousString = ""
	local shortcodesFound = false
	local uppercaseShortcode = false
	local mainStringShortcodesFound = false
	local advanceNumber = 1
	local breakAtOnce = false

	PrintDebug("Case Sensitiv is:", caseSensitive)
	PrintDebug("Main String: " .. mainString)
	PrintDebug("Search for: " .. searchString)
	PrintDebug("Replace with: " .. replaceString)
	if spaceSplitter == true then
		replaceString = InsertPatternBreaker(replaceString)
	end

	local doubleCheckTest
	doubleCheckTest = string.find(replaceString, searchString)
	if doubleCheckTest then 
		local a
		PrintDebug("Found the replace string in the search string. Inserting the Pattern Breaker to prevent double replacements.")
		replaceString = InsertPatternBreaker(replaceString)
		--replaceString = string.gsub(replaceString, searchString, a)


	end

	if DetectShortcodes(mainString) == true then
		PrintDebug("Found characters that could be interpreted as shortcodes in the main string. Disabling the use of shortcodes in the search strings.")
		searchString = ProcessPercentage(searchString)
		mainStringShortcodesFound = true
	else
		shortcodesFound, uppercaseShortcode = DetectShortcodes(searchString)
		if shortcodesFound == true then
			PrintDebug("Detected at least one shortcode.")
			advanceNumber = 2
			shortcodesFound = true
				if uppercaseShortcode == true and caseSensitive == false then
					caseSensitive = true
					print("An uppercase shortcode was found. Switching to case sensitive mode for this operation.")
				end
		end
	end

	replaceString = ProcessPercentage(replaceString)
	local replaceLength = string.len(replaceString)
	local mainLength = string.len(mainString)

	repeat
		repeatCounter = repeatCounter + 1
		if repeatCounter > 1 then
			PrintDebug("Double checking for:", searchString, "Current Try:", repeatCounter)
			previousString = string.sub(mainString, 1, searchStart-1 )
			mainString = string.sub(mainString,searchStart, -1)
			PrintDebug("Starting the search now at character:", searchStart, "with this string:", mainString)

		end
		local searchStringProcessed = ProcessDots(searchString)

		if caseSensitive == true then
			patternStart, patternEnd = string.find(mainString, searchStringProcessed)
		else
			patternStart, patternEnd = string.find(string.lower(mainString), string.lower(searchStringProcessed)) --turn everything lower case and find out where in the string the current searchString is
		end
		if patternStart then
			local count = 0
			local pattern = string.sub(mainString, patternStart, patternEnd)
			if mainStringShortcodesFound == true then --If the main String contains shortcode like characters then we need to double them in the search string so we can replace them and not treat them as shortcodes
				pattern = ProcessPercentage(pattern)
			else --the else is needed because we don't want to double the percentage signs again when dots are present 
				pattern = ProcessDots(pattern)

			end

			if pattern ~= replaceString then
				if shortcodesFound == true then
					mainString, count = string.gsub(mainString, pattern, replaceString, 1)
				else
					mainString, count = string.gsub(mainString, pattern, replaceString, Limit)

				end
			else
				PrintDebug("Pattern and replace string are the same. Advancing the search.")
			end
			searchStart = patternEnd + string.len(previousString) + replaceLength - string.len(searchString) + advanceNumber
			PrintDebug("Found pattern:", pattern)
			PrintDebug("It was replaced:", count, "times.")
			PrintDebug("Replaced the text. The temporary result is:", mainString)
		else
			if repeatCounter == 1 then
				PrintDebug("Pattern:", searchString, "was not found once in:", mainString)
			else
				PrintDebug("Pattern:", searchString, "was not found again in:", mainString)
			end

		end
		mainString = previousString .. mainString

		PrintDebug("")
	until repeatCounter > mainLength+advanceNumber or patternStart == nil or breakAtOnce == true
	if repeatCounter > 1 or breakAtOnce == true then
		local msg = ""
		if repeatCounter > mainLength+advanceNumber then
			print("!!! LIMIT WAS REACHED !!!")
			print("!!! LIMIT WAS REACHED !!!")
			print("!!! LIMIT WAS REACHED !!!")
			msg = "!!! Limit was reached! You might need to run the search again or the Limit most likely prevented an endless loop. !!!"
		end
		PrintDebug("")
		PrintDebug("")
		if doubleCheckTest then 
			mainString = CleanPatternBreaker(mainString)
		end
		return mainString, msg
	else
		return nil
	end
end

------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------
--Insert the pattern breaker into the middle of a string
------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------
function InsertPatternBreaker(text)
	PrintDebug("Inserting Pattern Breaker to:", text)
	if text then
		local length = string.len(text)
		local a = string.sub(text, 1, length/2)
		local b = string.sub(text, length/2+1, -1)
		local c = a .. PatternBreaker .. b
		PrintDebug("Pattern Breaker was placed:", c)

		return c
	end
end

function SpaceSplitterSearchAndReplace(mainString, searchString, replaceString, caseSensitive, spaceSplitter)
	local searchTable = {}
	local replaceTable = {}
	searchTable = split(searchString, " ")
	replaceTable = split(replaceString, " ")
	local tempReplaceString = nil
	for i, searchInput in pairs(searchTable) do
		if replaceTable[i] then
			replaceString = replaceTable[i]
		else
			replaceString = tempReplaceString
		end
		tempReplaceString = replaceTable[i]
		tempMainString = mainString
		mainString, msg = SearchAndReplace(mainString, searchInput, replaceString, caseSensitive, spaceSplitter)
		if mainString == nil then
			mainString = tempMainString
		end
	end
	return mainString, msg
end
------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------
--Cleanup the pattern breaker
------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------
function CleanPatternBreaker(text)
	PrintDebug("Cleaning Pattern Breakter from:", text)
	if text then
		text = string.gsub(text, " " .. PatternBreaker .. " ", " ") --First we clean Pattern breakers that have spaces in front and the back This ensure that we can delete strings without having double spaces
		text = string.gsub(text, PatternBreaker, "") -- Now we get rid of all pattern breakers that are inbetween words
		PrintDebug("Pattern Breaker was cleaned:", text)

		return text
	end
end

------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------
--Detect shortcodes
------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------
function DetectShortcodes(text)
	PrintDebug("Looking for shortcodes in:", text)
	if text then
		text = string.lower(text)
		local shortcodeD = string.find(text, '%%d')
		local shortcodeL = string.find(text, '%%l')
		local shortcodeU = string.find(text, '%%u')
		local shortcodeA = string.find(text, '%%a')
		local shortcodeC = string.find(text, '%%c')
		local shortcodeP = string.find(text, '%%p')
		local shortcodeS = string.find(text, '%%s')
		local shortcodeW = string.find(text, '%%w')
		local shortcodeX = string.find(text, '%%x')
		local shortcodeZ = string.find(text, '%%s')


		if shortcodeU then
			return true, true
		elseif shortcodeA or shortcodeC or shortcodeD or shortcodeL or shortcodeP or shortcodeS or shortcodeW or shortcodeX or shortcodeZ then
			return true, false
		else
			return false, false
		end

	end
end

------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------
--swap out single dots with %. using string.gsub didn't work reliably for me so we split it at the dots and replace them with %.
------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------

function ProcessDots(text)
	if text then
		if string.find(text, "%.") then
			local splitTable = {}
			local tableLength = 0
			text = string.gsub(text, " ", string.rep(PatternBreaker, 2) ) --spaces seem to get deleted when splitting so we need to manually recover them
			splitTable = split(text, ".")
			text = ""

			for i, input in pairs(splitTable) do
				tableLength = tableLength + 1
			end

			for i, input in pairs(splitTable) do
				if i >= tableLength then
					text = text .. input
					PrintDebug("Preventing double dot placement.")
				else
					text = text .. input .. "%."
					PrintDebug("Processing dots in the string. Round:", i)
				end
			end
			text = string.gsub(text, string.rep(PatternBreaker, 2)," ") -- we don't need to replicate the Pattern Breaker since spaces won't exist in Space Splitter mode, but just to be sure we do.
		end
	end
	return text
end

------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------
--swap out single % with %% using string.gsub didn't work reliably for me so we split it at the percentage signs and replace them with %%
------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------

function ProcessPercentage(text)
	if text then
		if string.find(text, "%%") then
			local splitTable = {}
			local tableLength = 0
			text = string.gsub(text, " ", string.rep(PatternBreaker, 2) )--spaces seem to get deleted when splitting so we need to manually recover them
			splitTable = split(text, "%")
			text = ""

			for i, input in pairs(splitTable) do
				tableLength = tableLength + 1
			end
			for i, input in pairs(splitTable) do
				if i >= tableLength then
					text = text .. input
					PrintDebug("Preventing double %% placement.")
				else
					text = text .. input .. "%%"
					PrintDebug("Processing % in the string. Round:", i)
				end
			end
			text = string.gsub(text, string.rep(PatternBreaker, 2)," ")
		end
	end
	return text
end
------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------
--Change the Notification Text and also print if Debugging is true
------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------
function Notify(text1, text2, text3, text4)
	if text1 then
		text1 = tostring(text1)
	end
	if text2 then
		text2 = tostring(text2)
	end
	if text3 then
		text3 = tostring(text3)
	end
	if text4 then
		text4 = tostring(text4)
	end
	if text1 and text2 and text3 and text4 then
		itm.Notification.Text = text1 .. " " .. text2 .. " " .. text3 .. " " .. text4
	elseif text1 and text2 and text3 then
		itm.Notification.Text = text1 .. " " .. text2 .. " " .. text3
	elseif text1 and text2 then
		itm.Notification.Text = text1 .. " " .. text2
	elseif text1 then
		itm.Notification.Text = text1
	end
	PrintDebug(text1, text2, text3, text4)
end

------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------
--Print a message if Debugging is enabled
------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------
function PrintDebug(text1, text2, text3, text4)
	if Debugging == true then
		if text1 then
			text1 = tostring(text1)
		end
		if text2 then
			text2 = tostring(text2)
		end
		if text3 then
			text3 = tostring(text3)
		end
		if text4 then
			text4 = tostring(text4)
		end
		if text1 and text2 and text3 and text4 then
			print(text1 .. " " .. text2 .. " " .. text3 .. " " .. text4)
		elseif text1 and text2 and text3 then
			print(text1 .. " " .. text2 .. " " .. text3)
		elseif text1 and text2 then
			print(text1 .. " " .. text2)
		elseif text1 then
			print(text1)
		end
	end
end
------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------
--This increases the Version on the Compositions Filepath 
--It assumes the version always to be prefixed with a "v" or "V". It will respect the capital letters.
------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------
function FindVersion(path)
	local version = string.match(path, "[Vv]%d+")
	local v = "v"
	if version ~= nil then
		if string.find(version, "V") ~= nil then
			v = "V"
			PrintDebug("Uppercase V was found")
		else
			PrintDebug("Lowercase v was found")
		end
		version = string.gsub(version, v, "")
		return v, version
	else
		PrintDebug("No Version was found. This script expects versioning to start with a v followed by numbers. For example: Filename.v002.exr")
		return nil, nil 
	end
end


function UpdateVersion(path, bool)
	if path then 
		local oldVersionNumber
		local tempOldVersionNumber
		local v
		local count
		local newPath
		local newVersionNumber
		v, oldVersionNumber = FindVersion(path)
		if oldVersionNumber and v then
			local padding = string.len(oldVersionNumber)
			tempOldVersionNumber = oldVersionNumber
			oldVersionNumber = tonumber(oldVersionNumber)
			local padding = "%0" .. padding .. "d"

			if bool == true then
				PrintDebug("Increasing Version on:", path)
				newVersionNumber = oldVersionNumber + 1
			elseif bool == false then
				PrintDebug("Decreasing Version on:", path)
				newVersionNumber = oldVersionNumber - 1
			end 
			newVersionNumber = string.format(padding, newVersionNumber)
			newVersion = v .. newVersionNumber
			oldVersion = v .. tempOldVersionNumber
			newPath, count = string.gsub(path, oldVersion, newVersion)
		end
		if count ~= nil and count > 0 then 
			return newPath
		else
			PrintDebug("No change was done to:", path)
			return nil
		end
	else
		PrintDebug("No path was found to update.")
	end


end

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--The following two functions are from the bmd.scriptlib:

-- trim(strTrim)
--
-- returns strTrim with leading and trailing spaces
-- removed.
--
-- introduced bmd.dfscriptlib v1.0
-- last updated in v1.3
-----------------------------------------------------
function trim(strTrim)
	strTrim = string.gsub(strTrim, "^(%s+)", "") -- remove leading spaces
	strTrim = string.gsub(strTrim, "(%s+)$", "") -- remove trailing spaces
	return strTrim
end
-- split(strInput, delimit)
--
-- converts string strInput into a table, separating
-- records using the provided delimiter string
--
-----------------------------------------------------
function split(strInput, delimit)
	local strLength
	local strTemp
	local strCollect
	local tblSplit
	local intCount

	tblSplit = {}
	intCount = 0
	strCollect = ""
	if delimit == nil then
		delimit = ","
	end

	strLength = string.len(strInput)
	for i = 1, strLength do
		strTemp = string.sub(strInput, i, i)
		if strTemp == delimit then
			intCount = intCount + 1
			tblSplit[intCount] = trim(strCollect)
			strCollect = ""
		else
			strCollect = strCollect .. strTemp
		end
	end
	intCount = intCount + 1
	tblSplit[intCount] = trim(strCollect)

	return tblSplit
end
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--function mergetables(table1, table2) returns table3 which is both tables combined
--This is currently only used to to combine the selected tools with the modifiers that are connected to those tools
--It does NOT handle nil in tables but this is not an issue for the current usage
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

function mergetables(table1, table2)
if Debugging == true then
print('Merging Selected Tools and their connected Modifiers')
end

table3 = {}
count = 1
for e, element in pairs(table1) do
table3[e] = element
count = count + 1
end

for e, element in pairs(table2) do
table3[e+count] = element
end

if Debugging == true then
dump('Merged Table: ', table3)
end
return(table3)

end
------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------
--Add text to table. If the entry already exists then it will be moved to the top and not duplicated
------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------
function AddToTable(tbl, text)
	local tempTable = {}
	tempTable[0] = text
	local match = false
	local matchNumber
	local count = 0
	PrintDebug("Adding to table")
	for i, entry in pairs(tbl) do
		if entry == text then
			PrintDebug("Found match:", entry)
			match = true
			matchNumber = i
		end
		if match == true or i >= MaxComboLimit then 
			break 
		end
	end
	if match == true then
		for i, entry in pairs(tbl) do
			if i ~= matchNumber then
				count = count + 1
				tempTable[count] = entry
			end
			if i >= MaxComboLimit then 
			break  
			end
		end
	else
		for i, entry in pairs(tbl) do
			count = count + 1
			tempTable[count] = entry
			if i >= MaxComboLimit then 
			break  end
		end
	end
	return tempTable
end

------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------
--Fill the Combobox with a table
------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------

function FillComboBox(tbl, item)
	item:Clear()
	for i, entry in pairs(tbl) do
		item:AddItem(entry)
	end
end

------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------
--This is where the UI is being created.
------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------

win = disp:AddWindow({

	ID = 'ChangeStringsWindow',
	WindowTitle = 'Change Strings',
	MinimumSize = {700, 400}, --We specify a minimum size but no position. This will make ChangeStrings open in the middle of the screen
	Events = { Close = true, KeyPress = true, KeyRelease = true, },

	ui:VGroup{
		ID ='ChangeStringsroot',

		ui:HGroup{
			ui:Button{ID = 'InvisibleButton', Text = 'About', Weight = 0.1, Visible = false},
			ui:Label{ID = 'Title', Alignment = {AlignHCenter = true, AlignVCenter= true}, Text = '<h1> Change Strings</h1>', Font = ui:Font{Family = "Arial", StyleName = "Bold",},},
			ui:Button{ID = 'AboutButton', Text = 'About', Weight = 0.1, ToolTip = "Find out more and access the Advanced Settings."},
		},

		ui:Label{ID = 'AppPage', Alignment = {AlignHCenter = true, AlignVCenter= true},},
		ui:Label{ID = 'Notification', Text="", HTML = true, Alignment = {AlignHCenter = true, AlignVCenter= true}, Font = ui:Font{Family = "Arial", StyleName = "Italic", PixelSize = 10,}, WordWrap = true, TextInteractionFlags = {TextSelectableByMouse = true},},

		ui:HGroup{ --This will force the next elements to be placed horizontally until the next HGroup is opened

			ui:Label{ID = 'LabelSearchfor', Text = 'Search for:', Alignment = {AlignHCenter = true, AlignVCenter = true}, Weight = 0.25,}, --changes the relative length of the label,
			ui:ComboBox{ID = "SearchForHistory", Editable = true, AutoCompletionCaseSensitivity = "CaseSensitive"},
			ui:Button{ID = "ClearSearchFor", Text = "x", Weight = 0, ToolTip = "Click to clear text. Shift + click to recover previously cleared text",},
			ui:Label{ID = 'help0', HTML = true, OpenExternalLinks = true, Weight = 0,1, Text = '<a href="' .. 'https://noahhaehnel.com/blog/manual-changestrings#Search_for_and_Replace_with_Text_field' .. '">?</a>',},
		},


		ui:HGroup{

			ui:HGap(0,0.45),
			ui:Button{ ID = 'Switch', Text = '↕', Weight = 0.1, ToolTip = "Switch the current Search and Replace Inputs."},
			ui:HGap(0,0.45),},

		ui:HGroup{

			ui:Label{ID = 'LabelReplacewith', Text = 'Replace with:', Alignment = {AlignHCenter = true, AlignVCenter = true}, Weight = 0.25,},
			ui:ComboBox{ID = "ReplaceWithHistory", Editable = true, AutoCompletionCaseSensitivity = "CaseSensitive"},
			ui:Button{ID = "ClearReplaceWith", Text = "x", Weight = 0,ToolTip = "Click to clear text. Shift + click to recover previously cleared text",},
			ui:Label{ID = 'help1', HTML = true, OpenExternalLinks = true, Weight = 0,1, Text = '<a href="' .. 'https://noahhaehnel.com/blog/manual-changestrings#Search_for_and_Replace_with_Text_field' .. '">?</a>',},
		},
		ui:TabBar { Weight = 0.0, ID = "PageTabs", BackgroundColor = {R= 0, G =0, B= 0,}, },
		ui:Stack{ID = "PageStack", Weight = 1.0,
			ui:VGroup{

				ui:Label{ID = 'LabelReplaceIn', Text = '<h3>Replace in:</h3>', Alignment = {AlignHCenter = true, AlignVCenter = true},},
				ui:HGroup{

					ui:Label{ID = 'Texts', Text = 'Texts:',},
					ui:CheckBox{ID = 'DoNames', Text = 'Names', ToolTip = "Search and Replace in Node Names. \n Including invisible nodes like Modifiers when the Settings is enabled."},
					ui:CheckBox{ID = 'DoExpressions', Text = 'Expressions',ToolTip = "Search and Replace in Expressions of Controls."},
					ui:CheckBox{ID = 'DoStyledText', Text = 'Styled Text', ToolTip = "Search and Replace in Styled Text boxes in Text+ and Text3D."},
					ui:CheckBox{ID = 'DoObjName', Text = 'FBX/ABC Name', ToolTip = "Search and Replace in the Object Identifier Names of FBX and ABC nodes."},
				},

				ui:HGroup{

					ui:Label{ID = 'Filepaths', Text = 'Filepaths:',},
					ui:CheckBox{ID = 'DoLoader', Text = 'Loaders', ToolTip = "Search and Replace in Loader Filepaths/Clips"},
					ui:CheckBox{ID = 'DoProxy', Text = 'Proxys',ToolTip = "Search and Replace in Loader Proxy Filepaths/Clips"},
					ui:CheckBox{ID = 'DoSaver', Text = 'Savers',ToolTip = "Search and Replace in Saver Filepaths"},
					ui:CheckBox{ID = 'DoFBXFile', Text = 'FBX/ABC Path',ToolTip = "Search and Replace in FBX/ABC nodes Filepaths"},
				},
			},

			ui:VGroup{

				ui:Label{ID = 'LabelSimpleVersions', Text = '<h3>Simple Versioning on selected Filepaths:</h3>', Alignment = {AlignHCenter = true, AlignVCenter = true},},
				ui:Button{ID = "IncreaseVersionButton", Text = "Increase Version ↑", ToolTip = "This performs a simple version increase on selected Loaders, Savers and FBX/ABC nodes.\n This expects a v in the versioning."},
				ui:Button{ID = "DecreaseVersionButton", Text = "Decrease Version ↓",ToolTip = "This performs a simple version decrease on selected Loaders, Savers and FBX/ABC nodes.\n This expects a v in the versioning."},

			},

			ui:VGroup{
				ui:Label{ID = 'LabelTextBoxes', Text = '<h3>Replace in Custom Text:</h3>', Alignment = {AlignHCenter = true, AlignVCenter = true},},
				ui:HGroup{
					ui:TextEdit{ID= "TextBoxIn", Weight = 1},
					ui:VGroup{
						Weight = 0,
						ui:HGroup{
							Weight = 0,
							ui:Button{ ID = 'PasteTextBox', Text = 'Paste', Weight = 0, Enabled = true, ToolTip = "Paste into the Textbox. \n Shift + click to recover previous Text."},-- If bmd.getclipboard() is every fixed for MacOS then we can activate this. The recover functionality for clipboard related buttons is also hooked up but will not be indicated until the bug is fixed
							ui:Button{ ID = 'SwitchTextBox', Text = '←', Weight = 0, ToolTip = "Click to copy text from right to left. \n Shift + click to recover previously cleared text",},
						},
						ui:Button{ ID = 'CopyTextBox', Text = 'Copy', Weight = 0.1, ToolTip = "Copy the result to your clipboard. \n Shift + click to recover previous clipboard contents."},
					},
					ui:TextEdit{ID="TextBoxOut", ReadOnly = true, Weight = 1, PlaceholderText = "Your Text will appear here. \n Press the copy button to copy the whole text to your clipboard"},
				},


			},

		},

			ui:Label{ID = 'LabelBreaker',Text = '_____________________________________________________________________________________________________________', Alignment = {AlignHCenter = true, AlignVCenter = true},},
			ui:VGap(5),

			ui:HGroup{

				ui:Label{ID = 'QuickSelectStrip', Text = 'QuickSelect:',},
				ui:Button{ID = 'SelectLoaders',Text = ' Loaders ', ToolTip = "Click to select Loaders. \n Shift + Click to add Loaders to the selection and \n Ctrl + Click to remove them from the current selection."},
				ui:Button{ID = 'SelectLS',Text = ' L + S ', ToolTip = "Click to select Loaders and Savers.\n Shift + Click to add Loaders and Savers to the selection and \n Ctrl + Click to remove them from the current selection."},
				ui:Button{ID = 'SelectSavers', Text = ' Savers ', ToolTip = "Click to select Savers.\n Shift + Click to add Savers to the selection and \n Ctrl + Click to remove them from the current selection."},
				ui:Button{ID = 'SelectFBX', Text = ' FBX/ABC ', ToolTip = "Click to select FBX and ABC nodes. \n Shift + Click to add FBX and ABC nodes to the selection and \n Ctrl + Click to remove them from the current selection. \n Note that this will work both on import as well as the FBX Export node." },
				ui:Button{ID = 'SelectSearch',Text = ' Search ',ToolTip = "Click to select Tools containing the pattern in the Search for Textbox. \n Shift + Click to add the found nodes to the selection and \n Ctrl + Click to remove them from the current selection. It does factor in case sensitivity if activated." },
				ui:Button{ID = 'SelectPrevious',Text = ' Previous ',ToolTip = "Click to select Previous nodes. \n Shift + Click to add Previous nodes to the selection and \n Ctrl + Click to remove them from the current selection. \n Note that this means nodes selected before using any of the QuickSelect Buttons." },
				ui:VGap(0,0.5)
			},




		ui:Label{ID = 'LabelBreaker', Text = '_____________________________________________________________________________________________________________', Alignment = {AlignHCenter = true, AlignVCenter = true},},

		ui:HGroup{

			ui:Label{ID = 'LabelSettings', Text = 'Settings:', Weight = 0.2, ToolTip = "Additional Settings"},
			ui:CheckBox{ID = 'DoCaseSensitive', Text = 'Be Case Sensitive', ToolTip = "Change Strings will factor in case sensitivity when enabled."},
			ui:CheckBox{ID = 'DoSpaceSplitter', Text = 'Space Splitter', ToolTip = "Enable to use the Change Strings 1 default behaviour of splitting the search and replace operation by spaces. \nThis allows switching patterns and doing more than one search and replace operation at once. \nCheck the manual to find out how this can speed up your workflow."},
			ui:CheckBox{ID = 'DoModifier', Text = 'Include Modifiers', Tristate = true, ToolTip = "Include Modifiers or search only in Modifiers. \nFor example if you want to rename many Modifiers, enable the Names checkbox and this Checkbox. \nMake sure you selected a node that is connected to these Modifiers."},
			ui:CheckBox{ID = 'DoRemeber', Text = 'Remember Inputs', ToolTip = "Saves your current settings. It is generally recommended to enable this. \nThe state of this checkbox is always saved."},
			ui:CheckBox{ID = 'DoOpen', Text = 'Stay Open', ToolTip = "The window will not close after you have pressed Ok."},
		},

		ui:Label{ID = 'LabelBreaker',Text = '_____________________________________________________________________________________________________________',Alignment = {AlignHCenter = true,},},

		ui:HGroup{

			ui:Button{ID = 'OkButton', Text = 'OK',},
			ui:Button{ID = 'CancelButton', Text = 'Cancel',},
		},

	},


})

function AboutWindow()
MouseX = fu:GetMousePos()[1]
MouseY = fu:GetMousePos()[2]
print('Opening About and Settings')


	local width,height = 620, 350
	win = disp:AddWindow({
		ID = 'AboutWindow',
		WindowTitle = 'About Change Strings',
		Window = true,
		Geometry = {MouseX-520, MouseY-50, 650, height},

		ui:VGroup{

			ui:Label{
			ID = 'AboutLabel', Text = '<h2>About</h2>', Alignment = {AlignHCenter = true, AlignTop = true},},
			ui:Label{ID = 'AboutText', HTML = true, OpenExternalLinks = true, Text = '<b><i>ChangeStrings </i></b>' .. ScriptVersion .. ' was made by <a href="' .. 'www.noahhaehnel.com' .. '">Noah Hähnel</a> <br>It is designed to find and replace most strings, meaning text, in Fusion. <br> <b> Change Strings might have more features than you realize.</b> <br> Click on the Questionmarks (?) next to the Text Inputs to open the manual <br> on how to use <i>Change Strings</i> to its fullest and see handy shortcodes. <br> <a href= https://noahhaehnel.com/blog/manual-changestrings/2>You can check for updates here.</a> <br> <a href= https://noahhaehnel.com/blog/manual-changestrings>Or click here for the whole manual.</a>', Alignment = {AlignHCenter = true,},},
			
			ui:VGap(5),

			ui:CheckBox{ ID = 'ACheckbox', Text = 'Show Advanced Settings', AlignCenter = true, Weight = 0.1,},
			ui:Label{ID = 'ASettings', Text = '<h3>Advanced Settings:</h3>', Alignment = {AlignHCenter = true, AlignTop = true}, Visible = false},

			ui:VGroup{
				ID = "AdvancedSettingsGroup", Visible = false,

				ui:HGroup{ 

					ui:Label{ID = 'LabelPatternBreaker', Text = 'Pattern Breaker:',},
					ui:LineEdit{ID = 'PatternBreakerInput', PlaceholderText = '  Default Pattern Breaker is "ïèëï". Only change if you know what you are doing', Weight = 3, ClearButtonEnabled = true,},
					ui:Label{ID = 'help3', HTML = true, OpenExternalLinks = true, Weight = 0.1, Text = '<a href="' .. 'https://noahhaehnel.com/blog/manual-changestrings#Pattern_Breaker' .. '">?</a>',},
				},
				ui:HGroup{

					ui:Label{ID = 'MaxLimit', Text = 'Max Replace Limit:',},
					ui:LineEdit{ID = 'MaxLimitInput', PlaceholderText = '  Default Limit is "30". Only change if you know what you are doing.', Weight = 3, ClearButtonEnabled = true,},
					ui:Label{ID = 'help4', HTML = true, OpenExternalLinks = true, Weight = 0.1, Text = '<a href="' .. 'https://noahhaehnel.com/blog/manual-changestrings#Max_Replace_Limit' .. '">?</a>',},
				},
				--ui:HGroup{
					--When I figure out how to delete entries in the Prefs File then the user can change the History length
					--ui:Label{ID = 'ComboLimit', Text = 'Max Save History:',},
					--ui:SpinBox{ID = 'MaxComboLimit', Value = 5, SingleStep = 1, Weight = 3,},
					--ui:Label{ID = 'help5', HTML = true, OpenExternalLinks = true, Weight = 0.1, Text = '<a href="' .. 'https://noahhaehnel.com/blog/manual-changestrings#Max_Replace_Limit' .. '">?</a>',},
				--},

				ui:HGroup{

					ui:CheckBox{ID = 'DoDebugging', Text = 'Print Actions', Checked = false,},
				},
			},

			ui:HGroup{

				ui:Button{ID = 'OkAbout', Text = 'Ok and Save',},
				ui:Button{ID = 'CancelAbout', Text = 'Cancel',},
			},
}
	}
)

	itm2 = win:GetItems()
	local oldAboutData = fusion:GetPrefs("Comp.ChangeStrings")
	if oldAboutData then	

		if oldAboutData.OldPatternBreakerInput ~= nil then
				PatternBreakerInput = oldAboutData.OldPatternBreakerInput
				itm2.PatternBreakerInput.Text = PatternBreakerInput
		else
				itm2.PatternBreakerInput.Text = nil
		end
		if oldAboutData.OldMaxLimit ~= nil then
			MaxLimitInput = oldAboutData.OldMaxLimit
			itm2.MaxLimitInput.Text = MaxLimitInput
		else
			itm2.MaxLimitInput.Text = nil
		end
		if oldAboutData.OldDoDebugging== 'Checked' then
			itm2.DoDebugging.Checked = true
			Debugging = true
		else
			Debugging = false
			itm2.DoDebugging.Checked = false
		end

		if oldAboutData.OldASettings == 'Checked' then
			itm2.ACheckbox.Checked = true
		else
			itm2.ACheckbox.Checked = false
		end
		if oldAboutData.OldMaxComboLimit then
			itm2.MaxComboLimit.Value = oldAboutData.OldMaxComboLimit + 1
		end
	end

	if itm2.ACheckbox.Checked == true then
		itm2.AdvancedSettingsGroup.Visible = true

	else
		itm2.AdvancedSettingsGroup.Visible = false
	end

	function win.On.AboutWindow.Close(ev)
		disp:ExitLoop()
	end

	function win.On.CancelAbout.Clicked(ev)
		disp:ExitLoop()
	end

	function win.On.ACheckbox.Clicked(ev)

		if itm2.ACheckbox.Checked == true then
		itm2.AdvancedSettingsGroup.Visible = true

		if OldData then
			OldData.OldASettings = 'Checked'
		end
		else
			if OldData then
				OldData.OldASettings = 'Unchecked'
			end
			itm2.AdvancedSettingsGroup.Visible = false
		end

	end



	function win.On.OkAbout.Clicked(ev)

		print('')



		if itm2.PatternBreakerInput.Text == '' then
			fusion:SetPrefs({['Comp.ChangeStrings.OldPatternBreakerInput'] = '' })
			PatternBreaker = 'ïèëï'
			print('Pattern Breaker is default:', PatternBreaker )
			else
			PatternBreakerInput = itm2.PatternBreakerInput.Text
			fusion:SetPrefs({['Comp.ChangeStrings.OldPatternBreakerInput']= PatternBreakerInput})
			PatternBreaker = PatternBreakerInput
			print('New Pattern Breaker is: ', PatternBreaker)
		end

		if itm2.MaxLimitInput.Text == '' or itm2.MaxLimitInput.Text == nil then
			fusion:SetPrefs({['Comp.ChangeStrings.OldMaxLimit'] = '' })
			Limit = 30
			print('Limit is default:', Limit )
		else
			MaxLimitInput = itm2.MaxLimitInput.Text
			fusion:SetPrefs({['Comp.ChangeStrings.OldMaxLimit']=MaxLimitInput})
			Limit = tonumber(MaxLimitInput)
			if Limit == nil then
				Limit = 30
				fusion:SetPrefs({['Comp.ChangeStrings.OldMaxLimit']=''})
				print('Invalid Input. Limit was reset to default: ', Limit)
			else
				Limit = Limit
				print('New Limit is: ', Limit)
			end
		end

		--MaxComboLimit = itm2.MaxComboLimit.Value -1
		--fusion:SetPrefs({['Comp.ChangeStrings.OldMaxComboLimit']= MaxComboLimit})
		--print("Max Combo History is: " .. MaxComboLimit+1)


	if itm2.DoDebugging.Checked == true then
		fusion:SetPrefs({['Comp.ChangeStrings.OldDoDebugging'] = 'Checked' })
		Debugging = true
	else
	fusion:SetPrefs({['Comp.ChangeStrings.OldDoDebugging']= 'Unchecked'})

		Debugging = false

	if Debugging == true then
	print('Prints Actions to the console')
	end
	end

	if itm2.ACheckbox.Checked == true then
		fusion:SetPrefs({['Comp.ChangeStrings.OldASettings'] = 'Checked' })
	else
	fusion:SetPrefs({['Comp.ChangeStrings.OldASettings']= 'Unchecked'})

		end

		if Fusion_S == true then
		print('Saved composition through About window')
		composition:Save()
		else
		pm:SaveProject()
		print('Saved Project through About window.')
		end


	print('')

	disp:ExitLoop()
	end

	win:Show()
	disp:RunLoop()
	win:Hide()

	return win,win:GetItems()
	end
	-- Window was closed

itm = win:GetItems()
local shiftPressed = false
local ctrlPressed = false
local recoverTextBoxIn = nil
local recoverSearchHistory = nil
local recoverReplaceHistory = nil
local recoverPasteTextBox = nil
local recoverClipboard = nil



function win.On.ChangeStringsWindow.KeyPress(ev)

	if ev.modifiers.ShiftModifier == true then
		shiftPressed = true
		itm.SwitchTextBox.Text = "Recover"
		itm.ClearSearchFor.Text = "Recover"
		itm.ClearReplaceWith.Text = "Recover"
		itm.PasteTextBox.Text = "Recover"
		itm.CopyTextBox.Text = "Recover"
		itm.SelectLoaders.Text = "+ Loaders "
		itm.SelectSavers.Text = "+ Savers "
		itm.SelectLS.Text = "+ L + S "
		itm.SelectFBX.Text = "+ FBX/ABC "
		itm.SelectPrevious.Text = "+ Previous  "
		itm.SelectSearch.Text = "+ Search  "


	elseif ev.modifiers.ControlModifier == true then
		ctrlPressed = true
		itm.SelectLoaders.Text = "- Loaders "
		itm.SelectSavers.Text = "- Savers "
		itm.SelectLS.Text = "- L + S "
		itm.SelectFBX.Text = "- FBX/ABC "
		itm.SelectPrevious.Text = "- Previous  "
		itm.SelectSearch.Text = "- Search  "



	end
end

function win.On.ChangeStringsWindow.KeyRelease(ev)
	if ev.modifiers.ShiftModifier == true or ev.modifiers.ControlModifier == true then
		shiftPressed = false
		ctrlPressed = false
		itm.SwitchTextBox.Text = "←"
		itm.ClearSearchFor.Text = "x"
		itm.ClearReplaceWith.Text = "x"
		itm.PasteTextBox.Text = "Paste"
		itm.CopyTextBox.Text = "Copy"
		itm.SelectLoaders.Text = " Loaders "
		itm.SelectSavers.Text = " Savers "
		itm.SelectLS.Text = " L + S "
		itm.SelectFBX.Text = " FBX/ABC "
		itm.SelectPrevious.Text = " Previous  "
		itm.SelectSearch.Text = " Search  "
	end
end

function win.On.SearchForHistory.EditingFinished(ev)
	print(itm.SearchForHistory.CurrentText)
end
-------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------
--Everything related to the tabs is here
-------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------
itm.PageTabs:AddTab("Flow Tools")
itm.PageTabs:AddTab("Versioning")
itm.PageTabs:AddTab("Text Box")
itm.PageTabs.TabTextColor[0] = { R=0.8, G=0.8, B=0.8, A=1 }
itm.PageTabs.TabTextColor[1] = { R=0.8, G=0.8, B=0.8, A=1 }
itm.PageTabs.TabTextColor[2] = { R=0.8, G=0.8, B=0.8, A=1 }
itm.PageTabs.CurrentIndex = 0

function win.On.PageTabs.CurrentChanged(ev)
	itm.PageStack.CurrentIndex = ev.Index
end

-------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------
-- Setting the Focus Policy on everything except the Text inputs. This allows tabbing between the two text inputs
-------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------
PrintDebug("Setting Focus Policy")
for i, widget in pairs(itm) do
	if widget.ClassName ~= "UIComboBox" then
		local focusTable = { ClickFocus = true, NoFocus = true, StrongFocus = false,  WheelFocus = false, TabFocus = false,}
		widget:SetFocusPolicy(focusTable)
	else
		local focusTable = { ClickFocus = true, NoFocus = false, StrongFocus = true,  WheelFocus = false, TabFocus = true,}
		widget:SetFocusPolicy(focusTable)
	end
end
-------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------
--The Old Data was retrieved at opening. Now it applies it since we now know what UI elements exist.
-------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------

if OldData then --If there is OldData available it will use these to load previous settings
	if OldData.OldDoExpressions == 'Checked' then
	itm.DoExpressions.Checked = true
	end
	if OldData.OldDoNames == 'Checked' then
	itm.DoNames.Checked = true
	end
	if OldData.OldDoLoader == 'Checked' then
	itm.DoLoader.Checked = true
	end
	if OldData.OldDoProxy == 'Checked' then
	itm.DoProxy.Checked = true
	end
	if OldData.OldDoSaver == 'Checked' then
	itm.DoSaver.Checked = true
	end
	if OldData.OldDoFBXFile == 'Checked' then
	itm.DoFBXFile.Checked = true
	end
	if OldData.OldDoObjName == 'Checked' then
	itm.DoObjName.Checked = true
	end
	if OldData.OldDoModifier == 'Checked' then
	itm.DoModifier:SetCheckState('Checked')
	itm.DoModifier.Text = 'Include Modifiers'
	end
	if OldData.OldDoModifier == 'Unchecked' then
	itm.DoModifier:SetCheckState('Unchecked')
	itm.DoModifier.Text = 'Include Modifiers'
	end
	if OldData.OldDoModifier == 'PartiallyChecked' then
	itm.DoModifier:SetCheckState('PartiallyChecked')
	itm.DoModifier.Text = 'Only Modifiers'
	Check = 1
	end
	if OldData.OldDoStyledText == 'Checked' then
	itm.DoStyledText.Checked = true
	end
	if OldData.OldDoOpen == 'Checked' then
	itm.DoOpen.Checked = true
	end
	if OldData.OldDoCaseSensitive == 'Checked' then
	itm.DoCaseSensitive.Checked = true
	end
	if OldData.OldDoSpaceSplitter == 'Checked' then
		itm.DoSpaceSplitter.Checked = true
	end
	if OldData.OldDoRemember == 'Checked' then
		itm.DoRemeber.Checked = true
		itm.OkButton.Text = 'Ok and Save'
	else
		itm.DoRemeber.Checked = false
		itm.OkButton.Text = 'Ok'
	end
	if OldData.OldSearchForHistory then
		for i, entry in pairs(OldData.OldSearchForHistory) do
			itm.SearchForHistory:AddItem(entry)
		end
	end
	if OldData.OldReplaceWithHistory then
		for i, entry in pairs(OldData.OldReplaceWithHistory) do
			itm.ReplaceWithHistory:AddItem(entry)
		end
	end
	if OldData.OldTabIndex then
		itm.PageTabs.CurrentIndex = OldData.OldTabIndex
		print("Tab index:" ..OldData.OldTabIndex )
	end
else print('Cannot apply any previous Settings')
	itm.SearchForHistory:AddItem("")
	itm.ReplaceWithHistory:AddItem("")

end


function win.On.ChangeStringsWindow.Close(ev)
	disp:ExitLoop()
end



function win.On.DoModifier.Clicked(ev)

if itm.DoModifier:GetCheckState() == 'PartiallyChecked' then
itm.DoModifier:SetCheckState('Checked')
Check = 0
itm.DoModifier.Text = 'Include Modifiers'
end

if Check == 1 then
itm.DoModifier:SetCheckState('Unchecked')
Check = 2
itm.DoModifier.Text = 'Include Modifiers'
end

if itm.DoModifier:GetCheckState() == 'Unchecked' and Check == 0 then
itm.DoModifier:SetCheckState('PartiallyChecked')
itm.DoModifier.Text = 'Only Modifiers'
Check = 1
end


end

function win.On.SelectLoaders.Clicked(ev)
	local flow = composition.CurrentFrame.FlowView
	local tools = composition:GetToolList(false, 'Loader')

	if PreviousSelected == nil then
		PreviousSelected = composition:GetToolList(true)
	end 

	if shiftPressed == true then
		Notify('Adding Loaders to selection')
		for i, tool in pairs(tools) do
			flow:Select(tool, true)
		end

	elseif ctrlPressed == true then
		Notify('Removing Loaders from selection')
		local selection = composition:GetToolList(true)
		flow:Select()

		for i, tool in pairs(selection) do
			local attrs = tool:GetAttrs()
			if attrs.TOOLS_RegID == "Loader" then
				PrintDebug("Removing:", attrs.TOOLS_Name, "from selection")
			else
				flow:Select(tool, true)
			end
		end
	else
		Notify("Selecting Loaders")
		flow:Select()
		for i, tool in pairs(tools) do
			flow:Select(tool, true)
		end
	end
	Notify("Finished selection changes with Loaders")

end

function win.On.SelectSavers.Clicked(ev)

	local flow = composition.CurrentFrame.FlowView
	local tools = composition:GetToolList(false, 'Saver')

	if PreviousSelected == nil then
		PreviousSelected = composition:GetToolList(true)
	end 

	if shiftPressed == true then
		Notify('Adding Savers to selection')
		for i, tool in pairs(tools) do
			flow:Select(tool, true)
		end

	elseif ctrlPressed == true then
		Notify('Removing Savers from selection')
		local selection = composition:GetToolList(true)
		flow:Select()

		for i, tool in pairs(selection) do
			local attrs = tool:GetAttrs()
			if attrs.TOOLS_RegID == "Saver" then
				PrintDebug("Removing:", attrs.TOOLS_Name, "from selection")
			else
				flow:Select(tool, true)
			end
		end
	else
		Notify("Selecting Savers")
		flow:Select()
		for i, tool in pairs(tools) do
			flow:Select(tool, true)
		end
	end
	Notify("Finished selection changes with Savers")
end

function win.On.SelectLS.Clicked(ev)


	local flow = composition.CurrentFrame.FlowView
	local loaders = composition:GetToolList(false, 'Loader')
	local savers = composition:GetToolList(false, 'Saver')
	local tools = mergetables(loaders, savers)

	if PreviousSelected == nil then
		PreviousSelected = composition:GetToolList(true)
	end 

	if shiftPressed == true then
		Notify('Adding Loaders and Savers to selection')
		for i, tool in pairs(tools) do
			flow:Select(tool, true)
		end

	elseif ctrlPressed == true then
		Notify('Removing Loaders and Savers from selection')
		local selection = composition:GetToolList(true)
		flow:Select()

		for i, tool in pairs(selection) do
			local attrs = tool:GetAttrs()
			if attrs.TOOLS_RegID == "Saver" or attrs.TOOLS_RegID == "Loader" then
				PrintDebug("Removing:", attrs.TOOLS_Name, "from selection")
			else
				flow:Select(tool, true)
			end
		end
	else
		Notify("Selecting Loaders and Savers")
		flow:Select()
		for i, tool in pairs(tools) do
			flow:Select(tool, true)
		end
	end
	Notify("Finished selection changes with Loaders and Savers")


end

function win.On.SelectFBX.Clicked(ev)

	local flow = composition.CurrentFrame.FlowView
	local tools = composition:GetToolList(false)

	if PreviousSelected == nil then
		PreviousSelected = composition:GetToolList(true)
	end 

	if shiftPressed == true then
		Notify('Adding FBX/ABC tools to selection')
		for i, tool in pairs(tools) do
			if 	tool.ID == 'SurfaceFBXMesh' or tool.ID == 'SurfaceAlembicMesh' or tool.ID == 'ExporterFBX' then
				flow:Select(tool, true)
			end
		end

	elseif ctrlPressed == true then
		Notify('Removing FBX/ABC tools from selection')
		local selection = composition:GetToolList(true)
		flow:Select()

		for i, tool in pairs(selection) do
			local attrs = tool:GetAttrs()
			if 	tool.ID == 'SurfaceFBXMesh' or tool.ID == 'SurfaceAlembicMesh' or tool.ID == 'ExporterFBX' then
				PrintDebug("Removing:", attrs.TOOLS_Name, "from selection")
			else
				flow:Select(tool, true)
			end
		end
	else
		Notify("Selecting FBX/ABC Tools")
		flow:Select()
		for i, tool in pairs(tools) do
			if 	tool.ID == 'SurfaceFBXMesh' or tool.ID == 'SurfaceAlembicMesh' or tool.ID == 'ExporterFBX' then
				flow:Select(tool, true)
			end
		end
	end
	Notify("Finished selection changes with FBX/ABC tools")
end

function win.On.SelectSearch.Clicked(ev)
	local flow = composition.CurrentFrame.FlowView
	local tools = composition:GetToolList(false)
	local toolCount = 0
	local caseSensitive = false
	local breaker = false

	if PreviousSelected == nil then
		PreviousSelected = composition:GetToolList(true)
	end 

	local search = itm.SearchForHistory.CurrentText
	if itm.DoCaseSensitive:GetCheckState() == "Checked" then
		caseSensitive = true
		PrintDebug("Search is case sensitive")
	end
	for i, tool in pairs(tools) do
		toolCount = toolCount + 1
	end

	
	if shiftPressed == true then
		Notify('Adding searched tools to selection')
		for i, tool in pairs(tools) do
			local attrs = tool:GetAttrs()
			local name = attrs.TOOLS_Name
				if caseSensitive == false then
					name = string.lower(name)
					search = string.lower(search)
				end
				if string.find(name, search) ~= nil then
					flow:Select(tool, true)
				end
		end

	elseif ctrlPressed == true then
		Notify('Removing searched tools to selection')
		local selection = composition:GetToolList(true)
		flow:Select()
		for i, tool in pairs(selection) do
			local attrs = tool:GetAttrs()
			local name = attrs.TOOLS_Name
			if caseSensitive == false then
				name = string.lower(name)
				search = string.lower(search)
			end
			if string.find(name, search) ~= nil then
				PrintDebug("Removing", name, "from selection.")
			else
				flow:Select(tool, true)
			end
		end
	else
		Notify('Selecting searched tools')
		flow:Select()
		for i, tool in pairs(tools) do
			local attrs = tool:GetAttrs()
			local name = attrs.TOOLS_Name
			if caseSensitive == false then
				name = string.lower(name)
				search = string.lower(search)
				print(name)
				print(search)
			end
			if string.find(name, search) ~= nil then
				flow:Select(tool, true)
			end
		end
	end
	Notify("Finished selection changes through search")
end
			


function win.On.SelectPrevious.Clicked(ev)
	local flow = composition.CurrentFrame.FlowView
	local tools = composition:GetToolList(false)
	local toolCount = 0
	for i, tool in pairs(tools) do
		toolCount = toolCount + 1
	end

	if PreviousSelected ~= nil then	

		if shiftPressed == true then
			Notify('Adding Previous tools to selection')
			for i, tool in pairs(PreviousSelected) do
				flow:Select(tool, true)
			end

		elseif ctrlPressed == true then
			Notify('Removing Previous tools from selection')
			local selection = composition:GetToolList(true)
			flow:Select()
			for i, tool in pairs(selection) do
				local attrs = tool:GetAttrs()
				local found = false
				for x, pTool in pairs(PreviousSelected) do
					if 	tool ~= pTool then
						found = false
					else
						found = true
						
					if found == true then PrintDebug("Breaking search") break end 	
					end
				end
				if found == false then
					flow:Select(tool, true)
				else
					PrintDebug("Removing:", attrs.TOOLS_Name, "from selection")
				end
			end
		else
			Notify("Selecting Previous tools")
			flow:Select()
			for i, tool in pairs(PreviousSelected) do
				flow:Select(tool, true)
			end
		end
	end
	Notify("Finished selection changes with Previous tools")
end



function win.On.Switch.Clicked(ev)

local switch_a = itm.SearchForHistory.CurrentText
local switch_b = itm.ReplaceWithHistory.CurrentText

itm.SearchForHistory.CurrentText = switch_b
itm.ReplaceWithHistory.CurrentText = switch_a

end

function win.On.SwitchTextBox.Clicked(ev)
	if shiftPressed == false then
		recoverTextBoxIn = itm.TextBoxIn.PlainText
		itm.TextBoxIn.PlainText = itm.TextBoxOut.PlainText
	elseif shiftPressed == true and recoverTextBoxIn ~= nil then
		itm.TextBoxIn.PlainText = recoverTextBoxIn
	end

end

function win.On.PasteTextBox.Clicked(ev)
	if shiftPressed == false then
		recoverPasteTextBox = itm.TextBoxIn.PlainText
		itm.TextBoxIn.PlainText = ""
		itm.TextBoxIn:Paste()
	elseif shiftPressed == true and recoverPasteTextBox ~= nil and recoverPasteTextBox ~="" then
		itm.TextBoxIn.PlainText = recoverPasteTextBox
	end
end

function win.On.CopyTextBox.Clicked(ev)
	if shiftPressed == false then
		local temp = itm.TextBoxIn.PlainText
		itm.TextBoxIn.PlainText = ""
		itm.TextBoxIn:Paste()
		recoverClipboard = itm.TextBoxIn.PlainText
		itm.TextBoxIn.PlainText = temp
		bmd.setclipboard(itm.TextBoxOut.PlainText)
	elseif shiftPressed == true and recoverClipboard ~= nil and recoverClipboard ~= "" then
		bmd.setclipboard(recoverClipboard)
	end
end

function win.On.ClearSearchFor.Clicked(ev)
	if shiftPressed == false then
		recoverSearchHistory = itm.SearchForHistory.CurrentText
		itm.SearchForHistory.CurrentText = ""
	elseif shiftPressed == true and recoverSearchHistory ~= nil then
		itm.SearchForHistory.CurrentText = recoverSearchHistory
	end
end

function win.On.ClearReplaceWith.Clicked(ev)
	if shiftPressed == false then
		recoverReplaceHistory = itm.ReplaceWithHistory.CurrentText
		itm.ReplaceWithHistory.CurrentText = ""
	elseif shiftPressed == true and recoverReplaceHistory ~= nil then
		itm.ReplaceWithHistory.CurrentText = recoverReplaceHistory
	end
end

function win.On.AboutButton.Clicked(ev)
				AboutWindow()
end

function win.On.DoRemeber.Clicked(ev)
	if itm.DoRemeber.Checked == true then
	itm.OkButton.Text = 'OK and Save'
	else
	itm.OkButton.Text = 'OK'
	end
end

if Fusion_S == true then
	itm.AppPage.Text = 'in Fusion Studio'
else
	resolve:OpenPage('fusion')
	itm.AppPage.Text = 'in DaVinci Resolve Fusion'
end


--Creates the Tables and Displays the ConsoleTips in the Notification Label
local consoleTip = {}
consoleTip[1] = "You can use shortcodes like %d to search for all digits."
consoleTip[2] = "You can use shortcodes like %l to search for all lower case letters."
consoleTip[3] = "You can use shortcodes like %u to search for all upper case letters."
consoleTip[4] = "You can use shortcodes like %a to search for all letters."
consoleTip[5] = "You can use shortcodes like %p to search for all punctuation characters."
consoleTip[6] = "You can use shortcodes like %s to search for all spaces."
consoleTip[7] = "Use the Space Spiltter setting to search and replace multiple inputs at once."
consoleTip[8] = "The Previous Button on the Quick Select Menu restores the selection you had prior to opening Change Strings."
consoleTip[9] = "You can even replace the Names and Expressions of Modifiers by selecting the Include Modifiers Settings."
consoleTip[10] = "Did something unexpeced happen? Try the Be Case Sensitive Settings. It's more accurate in difficult situations."
consoleTip[11] = "Increase the Max Replace Limit in the advanced settings if Change Strings doesn't fully complete a task."
consoleTip[12] = "Change Strings is capable of switching patterns around."
consoleTip[13] = "Did you drink enough water today?"
consoleTip[14] = "The time between the Stegosaurus and the T-Rex is bigger than the difference of the T-Rex to us (If you're reading this before 20 Million AD)"
consoleTip[15] = "Saturn has at least 83 confirmed moons."
consoleTip[16] = "While Basenji dogs don't bark they yodel."
consoleTip[17] = "Leafcutter Ants are capable of higher agriculture by farming fungi they eat. Some fungi can not survive without the ants and vice versa."
consoleTip[18] = "Hit shift or Ctrl to see which buttons change their function."
consoleTip[19] = "Using the Search QuickSelect button is an easy way to select similar tools."
consoleTip[20] = "Use the versioning Tab to quickly increase or decrease versions on filepaths."
consoleTip[21] = "You can add and remove tools from your selection by hitting Shift or Ctrl before clicking any of the QuickSelect buttons."
consoleTip[22] = "You can use the Text Box Tab for Text fields that aren't supported in Change Strings. Like the Custom tools."
consoleTip[23] = "Accidentally clicked on a button? You can most likely recover the previous state by hitting Shift and clicking on the button or using the Previous QuickSelect button."
consoleTip[24] = "You can use the Text Box Tab for Text fields that aren't supported in Change Strings. Like the Custom tools."
consoleTip[25] = "Remember, licking doorknobs is illegal on other planets."
consoleTip[26] = "Potassium decays, making bananas not only healthy but also slightly radioactive (but no cause for concern. A lot of things are radioactive.)."


math.randomseed(os.time())
local chooseConsoleTip = math.random(26)
--Display a random ConsoleTip in the Notification Label
itm.Notification.Text = consoleTip[chooseConsoleTip]




--Closes the window on Button press 
function win.On.CancelButton.Clicked(ev)
	print('Change Strings was cancelled')
	disp:ExitLoop()

end


function win.On.IncreaseVersionButton.Clicked(ev)
	local SelectedTools = composition:GetToolList(true) --Get all selected Tools
	local toolCount = 0
	composition:Lock()--Locks the comp so there are no pop ups and stuff
		for i, tool in pairs(SelectedTools) do
			toolCount = toolCount + 1
		end
	composition:StartUndo('Increase Versions')

	for i, tool in pairs(SelectedTools) do
		inputs = tool:GetAttrs()
		
		if inputs.TOOLS_RegID == 'Loader' then
			local text = inputs.TOOLST_Clip_Name[1]
			local altText = inputs.TOOLST_AltClip_Name[1]
			local oldText = text
			local oldAltText = altText
			if text then
				PrintDebug('Getting Loader Settings')
				local trimIn = inputs.TOOLIT_Clip_TrimIn
				local trimOut = inputs.TOOLIT_Clip_TrimOut
				local startFrame = inputs.TOOLNT_Clip_Start
				local endFrame = inputs.TOOLNT_Clip_End
				local extendLast = inputs.TOOLIT_Clip_ExtendLast
				local extendFirst = inputs.TOOLIT_Clip_ExtendFirst
				local reverse = inputs.TOOLBT_Clip_Reverse

				text = UpdateVersion(text, true)
					if altText then
						altText = UpdateVersion(altText, true)
					end
				if text then
					if oldText == text or text == "" or text == nil then
						PrintDebug("Loader is identical. No version increase is necessary on: ", oldText)
					else
						PrintDebug("Loader is being increased from: ", oldText, " to: ", text )
						tool.Clip[startFrame[1]] = text
					end
					if altText then
						if oldAltText == altText or altText == "" or altText == nil then
							PrintDebug("Proxy is identical. No version increase is necessary on: ", oldText)
						else
							PrintDebug("Proxy is being increased from: ", oldText, " to: ", text )
							tool.ProxyFilename[startFrame[1]] = altText
						end
					end
					
						
					PrintDebug('Applying previous Loader settings')
					--tool.GlobalIn = startFrame
					--tool.GlobalOut = endFrame
					tool.ClipTimeStart = trimIn[1]
					tool.ClipTimeEnd = trimOut[1]
					tool.HoldFirstFrame = extendFirst[1]
					tool.HoldLastFrame = extendLast[1]
					if reverse[1] == true then
						tool.Reverse = 1
					else
						tool.Reverse = 0
					end
				end
			end
		end

		if inputs.TOOLS_RegID == 'Saver' then
			local text = inputs.TOOLST_Clip_Name[1]
			local oldText = text
			if text then
				text = UpdateVersion(text, true)

				if oldText == text or text == "" or text == nil then
					PrintDebug("Saver is identical. No version increase is necessary on: ", oldText)
				else
					PrintDebug("Saver is being increased from: ", oldText, " to: ", text )
					tool.Clip = text
				end
			end
		end

		local text = nil
		if tool.ImportFile then
			text = tool.ImportFile[1]
		elseif tool.Filename then
			text = tool.Filename[1]
		end
		local oldText = text
		if text then
			text = UpdateVersion(text, true)

			if oldText == text or text == "" or text == nil then
				PrintDebug("ABC/FBX File is identical. No increase is necessary on: ", oldText)
			else
				PrintDebug("ABC/FBX File is being increased from: ", oldText, " to: ", text )
				if tool.ImportFile then
					tool.ImportFile[1] = text
				elseif tool.Filename then
					tool.Filename[1] = text
				end
			end
		end
		Notify("Increasing Filepath on Tool: ", i, "/", toolCount)
	end
	composition:EndUndo('Increase Version')
	Notify("Completed increasing Filepaths")
	fusion:SetPrefs({['Comp.ChangeStrings.OldTabIndex']=itm.PageTabs.CurrentIndex,})
	composition:Unlock()

end

function win.On.DecreaseVersionButton.Clicked(ev)
	local SelectedTools = composition:GetToolList(true) --Get all selected Tools
	local toolCount = 0
	composition:Lock()--Locks the comp so there are no pop ups and stuff
		for i, tool in pairs(SelectedTools) do
			toolCount = toolCount + 1
		end
	composition:StartUndo('Decrease Versions')

	for i, tool in pairs(SelectedTools) do
		inputs = tool:GetAttrs()
		
		if inputs.TOOLS_RegID == 'Loader' then
			local text = inputs.TOOLST_Clip_Name[1]
			local altText = inputs.TOOLST_AltClip_Name[1]
			local oldText = text
			local oldAltText = altText
			if text then
				PrintDebug('Getting Loader Settings')
				local trimIn = inputs.TOOLIT_Clip_TrimIn
				local trimOut = inputs.TOOLIT_Clip_TrimOut
				local startFrame = inputs.TOOLNT_Clip_Start
				local endFrame = inputs.TOOLNT_Clip_End
				local extendLast = inputs.TOOLIT_Clip_ExtendLast
				local extendFirst = inputs.TOOLIT_Clip_ExtendFirst
				local reverse = inputs.TOOLBT_Clip_Reverse

				text = UpdateVersion(text, false)
					if altText then
						altText = UpdateVersion(altText, false)
					end
				if text then
					if oldText == text or text == "" or text == nil then
						PrintDebug("Loader is identical. No version decrease is necessary on: ", oldText)
					else
						PrintDebug("Loader is being decreased from: ", oldText, " to: ", text )
						tool.Clip[startFrame[1]] = text
					end
					if altText then
						if oldAltText == altText or altText == "" or altText == nil then
							PrintDebug("Proxy is identical. No version decrease is necessary on: ", oldText)
						else
							PrintDebug("Proxy is being decreased from: ", oldText, " to: ", text )
							tool.ProxyFilename[startFrame[1]] = altText
						end
					end
					
						
					PrintDebug('Applying previous Loader settings')
					--tool.GlobalIn = startFrame
					--tool.GlobalOut = endFrame
					tool.ClipTimeStart = trimIn[1]
					tool.ClipTimeEnd = trimOut[1]
					tool.HoldFirstFrame = extendFirst[1]
					tool.HoldLastFrame = extendLast[1]
					if reverse[1] == true then
						tool.Reverse = 1
					else
						tool.Reverse = 0
					end
				end
			end
		end

		if inputs.TOOLS_RegID == 'Saver' then
			local text = inputs.TOOLST_Clip_Name[1]
			local oldText = text
			if text then
				text = UpdateVersion(text, false)

				if oldText == text or text == "" or text == nil then
					PrintDebug("Saver is identical. No version decrease is necessary on: ", oldText)
				else
					PrintDebug("Saver is being decreased from: ", oldText, " to: ", text )
					tool.Clip = text
				end
			end
		end

		local text = nil
		if tool.ImportFile then
			text = tool.ImportFile[1]
		elseif tool.Filename then
			text = tool.Filename[1]
		end
		local oldText = text
		if text then
			text = UpdateVersion(text, false)

			if oldText == text or text == "" or text == nil then
				PrintDebug("ABC/FBX File is identical. No decrease is necessary on: ", oldText)
			else
				PrintDebug("ABC/FBX File is being decreased from: ", oldText, " to: ", text )
				if tool.ImportFile then
					tool.ImportFile[1] = text
				elseif tool.Filename then
					tool.Filename[1] = text
				end
			end
		end
		Notify("Decreasing Filepath on Tool: ", i, "/", toolCount)

	end
	composition:EndUndo('Decrease Version')
	Notify("Completed decreasing Filepaths")
	fusion:SetPrefs({['Comp.ChangeStrings.OldTabIndex']=itm.PageTabs.CurrentIndex,})
	composition:Unlock()

end

-----------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------
-- Press Ok and run the main parts of the script
-----------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------
function win.On.OkButton.Clicked(ev)

	Notify("Pressed Ok. Change Strings is starting up...")
	comp:Lock()--Locks the comp so there are no pop ups and stuff

	local SelectedTools = composition:GetToolList(true) --Get all selected Tools
	local toolCount = 0
	local caseSensitive = false  --declare this local here so we have access to it throughout this function without having to create a global
	local spaceSplitter = false

	if itm.DoCaseSensitive:GetCheckState() == "Checked" then
		caseSensitive = true
	end
	if itm.DoSpaceSplitter:GetCheckState() == "Checked" then
		spaceSplitter = true
	end

	if itm.PageTabs.CurrentIndex == 0 then
		Notify("Running on Flow Tools")


		if itm.DoModifier:GetCheckState() == 'Checked' or itm.DoModifier:GetCheckState() == 'PartiallyChecked'  then
		SelectedModifiers = {}
		number = 1
		for o, tool in pairs(SelectedTools)do
		local inputs = tool:GetInputList()
		for q, input in pairs(inputs)do
		local connected = input:GetConnectedOutput()
			if connected ~= nil then
		allconnected = connected:GetTool()
			attrs = allconnected:GetAttrs()
			if attrs.TOOLB_Visible == false then

			SelectedModifiers[number] = allconnected
			number = number+1
		end
			end
			end
		end
		if itm.DoModifier:GetCheckState() == 'Checked' then

		mergetables(SelectedTools, SelectedModifiers)

		SelectedTools = table3
		end
		if itm.DoModifier:GetCheckState() == 'PartiallyChecked' then
		SelectedTools = SelectedModifiers
		PrintDebug('Only doing Modifiers that are connected to currently selected tools.')
		PrintDebug('Those Modifiers are:')
		if debug == true then
			dump(SelectedTools)
		end
		end
		end



		for i, tool in pairs(SelectedTools) do
			toolCount = toolCount + 1
		end


		if itm.DoNames.Checked == true then
			composition:StartUndo("Change Names")
			for i, tool in pairs(SelectedTools) do
				local toolAttrs = tool:GetAttrs()
				local text = toolAttrs.TOOLS_Name
				if spaceSplitter == true then
					PrintDebug("Doing Space Splitter Search and Replace on Names.")
					text, msg = SpaceSplitterSearchAndReplace(text, itm.SearchForHistory.CurrentText, itm.ReplaceWithHistory.CurrentText, caseSensitive, spaceSplitter)
					PrintDebug("Cleaning up Pattern Breaker in Name")
					text = CleanPatternBreaker(text)
				else
					text, msg = SearchAndReplace(text, itm.SearchForHistory.CurrentText, itm.ReplaceWithHistory.CurrentText, caseSensitive)

				end
				if tool.TOOLS_Name == text or text == "" or text == nil then
					PrintDebug("Name is identical. No replacement is necessary on: ", toolAttrs.TOOLS_Name)
				else
					PrintDebug("Name is being changed from: ", toolAttrs.TOOLS_Name, " to: ", text )
					tool:SetAttrs( { TOOLB_NameSet = true, TOOLS_Name = text } )
				end
				Notify("Changing Names: ", i, "/", toolCount)
			end
			Notify("Finished running on Names.", msg)
			composition:EndUndo("Change Names")

		end



		if itm.DoExpressions.Checked == true then


			composition:StartUndo('Change Expressions')
			for i, tool in pairs(SelectedTools) do
				local toolInputs = tool:GetInputList()
				for x, input in pairs(toolInputs) do
						local expression = input:GetExpression()
						if expression then

							local text = expression
							if spaceSplitter == true then
								PrintDebug("Doing Space Splitter Search and Replace in Expressions.")
								text, msg = SpaceSplitterSearchAndReplace(text, itm.SearchForHistory.CurrentText, itm.ReplaceWithHistory.CurrentText, caseSensitive, spaceSplitter)
								PrintDebug("Cleaning up Pattern Breaker in Expression")
								text = CleanPatternBreaker(text)
							else
								text, msg = SearchAndReplace(text, itm.SearchForHistory.CurrentText, itm.ReplaceWithHistory.CurrentText, caseSensitive)

							end
							if tool.TOOLS_Name == text or text == "" or text == nil then
								PrintDebug("Expression is identical. No replacement is necessary on: ", text)
							else
								PrintDebug("Expression is being changed from: ", expression, " to: ", text )
								input:SetExpression(text)
							end
						end
				end
				Notify("Changing Expressions on Tool: ", i, "/", toolCount)

			end
			Notify("Finished running on Expressions.", msg)

		composition:EndUndo('Change Expressions')
		end

		if itm.DoStyledText.Checked == true then
			composition:StartUndo('Change Styled Text')
			for i, tool in pairs(SelectedTools) do
				local currentTime = composition.CurrentTime
				if tool.StyledText then
					local text = tool.StyledText[currentTime]
					if text then
						if spaceSplitter == true then
							PrintDebug("Doing Space Splitter Search and Replace on Styled Text.")
							text, msg = SpaceSplitterSearchAndReplace(text, itm.SearchForHistory.CurrentText, itm.ReplaceWithHistory.CurrentText, caseSensitive, spaceSplitter)
							PrintDebug("Cleaning up Pattern Breaker in Styled Text")
							text = CleanPatternBreaker(text)
						else
							text, msg = SearchAndReplace(text, itm.SearchForHistory.CurrentText, itm.ReplaceWithHistory.CurrentText, caseSensitive)

						end
						if tool.StyledText[currentTime] == text or text == "" or text == nil then
							PrintDebug("Styled Text is identical. No replacement is necessary on: ", tool.StyledText[currentTime])
						else
							PrintDebug("Styled Text is being changed from: ", tool.StyledText[currentTime], " to: ", text )
							tool.StyledText[currentTime] = text
						end
					end
					Notify("Changing Styled Text on Tool: ", i, "/", toolCount)
				end
			end
			Notify("Finished running on Styled Text.", msg)

			composition:EndUndo('Change Styled Text')
		end

		if itm.DoObjName.Checked == true then
			composition:StartUndo('Change FBX/ABC Object Name')
			local currentTime = composition.CurrentTime

			for i, tool in pairs(SelectedTools) do
				if tool.ObjName then
					text = tool.ObjName[currentTime]
					if spaceSplitter == true then
						PrintDebug("Doing Space Splitter Search and Replace on Object Names.")
						text, msg = SpaceSplitterSearchAndReplace(text, itm.SearchForHistory.CurrentText, itm.ReplaceWithHistory.CurrentText, caseSensitive, spaceSplitter)
						PrintDebug("Cleaning up Pattern Breaker in Object Name")
						text = CleanPatternBreaker(text)
					else
						text, msg = SearchAndReplace(text, itm.SearchForHistory.CurrentText, itm.ReplaceWithHistory.CurrentText, caseSensitive)

					end
					if tool.ObjName[currentTime] == text or text == "" or text == nil then
						PrintDebug("Name is identical. No replacement is necessary on: ", tool.ObjName[currentTime])
					else
						PrintDebug("Name is being changed from: ", tool.ObjName[currentTime], " to: ", text )
						tool.ObjName[currentTime] = text
					end
					Notify("Changing Object Names: ", i, "/", toolCount)

				end

			Notify("Finished running on Object Names.", msg)
			composition:EndUndo('Change FBX/ABC Object Name')
			end
		end

		if itm.DoLoader.Checked == true then
			composition:StartUndo('Change Loaders')
			for i, tool in pairs(SelectedTools) do
				local inputs = tool:GetAttrs()
				if inputs.TOOLS_RegID == 'Loader' then
					local text = inputs.TOOLST_Clip_Name[1]
					local oldText = text
					if text then
						PrintDebug('Getting Loader Settings')
						local trimIn = inputs.TOOLIT_Clip_TrimIn
						local trimOut = inputs.TOOLIT_Clip_TrimOut
						local startFrame = inputs.TOOLNT_Clip_Start
						local endFrame = inputs.TOOLNT_Clip_End
						local extendLast = inputs.TOOLIT_Clip_ExtendLast
						local extendFirst = inputs.TOOLIT_Clip_ExtendFirst
						local reverse = inputs.TOOLBT_Clip_Reverse

						if spaceSplitter == true then
							PrintDebug("Doing Space Splitter Search and Replace on Object Names.")
							text, msg = SpaceSplitterSearchAndReplace(text, itm.SearchForHistory.CurrentText, itm.ReplaceWithHistory.CurrentText, caseSensitive, spaceSplitter)
							PrintDebug("Cleaning up Pattern Breaker in Object Name")
							text = CleanPatternBreaker(text)
						else
							text, msg = SearchAndReplace(text, itm.SearchForHistory.CurrentText, itm.ReplaceWithHistory.CurrentText, caseSensitive)

						end
						if oldText == text or text == "" or text == nil then
							PrintDebug("Loader is identical. No replacement is necessary on: ", oldText)
						else
							PrintDebug("Loader is being changed from: ", oldText, " to: ", text )
							tool.Clip[startFrame[1]] = text
						end
						PrintDebug('Applying previous Loader settings')
						--tool.GlobalIn = startFrame
						--tool.GlobalOut = endFrame
						tool.ClipTimeStart = trimIn[1]
						tool.ClipTimeEnd = trimOut[1]
						tool.HoldFirstFrame = extendFirst[1]
						tool.HoldLastFrame = extendLast[1]
						if reverse[1] == true then
							tool.Reverse = 1
						else
							tool.Reverse = 0
						end
					end
				end
				Notify("Changing Loaders: ", i, "/", toolCount)

			end
			composition:EndUndo('Change Loaders')
		end
		if itm.DoProxy.Checked == true then
			composition:StartUndo('Change Proxy')
			for i, tool in pairs(SelectedTools) do
				local inputs = tool:GetAttrs()
				if inputs.TOOLS_RegID == 'Loader' then
					local text = inputs.TOOLST_AltClip_Name[1]
					local oldText = text
					if text then
						PrintDebug('Getting Loader Settings')
						local trimIn = inputs.TOOLIT_Clip_TrimIn
						local trimOut = inputs.TOOLIT_Clip_TrimOut
						local startFrame = inputs.TOOLNT_Clip_Start
						local endFrame = inputs.TOOLNT_Clip_End
						local extendLast = inputs.TOOLIT_Clip_ExtendLast
						local extendFirst = inputs.TOOLIT_Clip_ExtendFirst
						local reverse = inputs.TOOLBT_Clip_Reverse

						if spaceSplitter == true then
							PrintDebug("Doing Space Splitter Search and Replace on Proxy.")
							text, msg = SpaceSplitterSearchAndReplace(text, itm.SearchForHistory.CurrentText, itm.ReplaceWithHistory.CurrentText, caseSensitive, spaceSplitter)
							PrintDebug("Cleaning up Pattern Breaker in Proxy")
							text = CleanPatternBreaker(text)
						else
							text, msg = SearchAndReplace(text, itm.SearchForHistory.CurrentText, itm.ReplaceWithHistory.CurrentText, caseSensitive)

						end
						if oldText == text or text == "" or text == nil then
							PrintDebug("Proxy is identical. No replacement is necessary on: ", oldText)
						else
							PrintDebug("Proxy is being changed from: ", oldText, " to: ", text )
							tool.ProxyFilename[startFrame[1]] = text
						end
						PrintDebug('Applying previous Loader settings')
						--tool.GlobalIn = startFrame
						--tool.GlobalOut = endFrame
						tool.ClipTimeStart = trimIn[1]
						tool.ClipTimeEnd = trimOut[1]
						tool.HoldFirstFrame = extendFirst[1]
						tool.HoldLastFrame = extendLast[1]
						if reverse[1] == true then
							tool.Reverse = 1
						else
							tool.Reverse = 0
						end
					end
				end
			end	Notify("Changing Proxy: ", i, "/", toolCount)
			composition:EndUndo('Change Proxy')
		end

		if itm.DoSaver.Checked == true then
			composition:StartUndo('Change Savers')
			for i, tool in pairs(SelectedTools) do
				inputs = tool:GetAttrs()
				if inputs.TOOLS_RegID == 'Saver' then
					text = inputs.TOOLST_Clip_Name[1]
					local oldText = text
					if text then
						if spaceSplitter == true then
							PrintDebug("Doing Space Splitter Search and Replace on Savers.")
							text, msg = SpaceSplitterSearchAndReplace(text, itm.SearchForHistory.CurrentText, itm.ReplaceWithHistory.CurrentText, caseSensitive, spaceSplitter)
							PrintDebug("Cleaning up Pattern Breaker in Saver")
							text = CleanPatternBreaker(text)
						else
							text, msg = SearchAndReplace(text, itm.SearchForHistory.CurrentText, itm.ReplaceWithHistory.CurrentText, caseSensitive)

						end
						if oldText == text or text == "" or text == nil then
							PrintDebug("Saver is identical. No replacement is necessary on: ", oldText)
						else
							PrintDebug("Saver is being changed from: ", oldText, " to: ", text )
							tool.Clip = text
						end
					end
				end
				Notify("Changing Savers: ", i, "/", toolCount)
			end
			composition:EndUndo('Change Savers')
		end


		if itm.DoFBXFile.Checked == true then
			composition:StartUndo('Change ABC/FBX File')
			for i, tool in pairs(SelectedTools) do
				if tool.ImportFile then
					text = tool.ImportFile[1]
				elseif tool.Filename then
					text = tool.Filename[1]
				end
				local oldText = text
				if text then
					if spaceSplitter == true then
						PrintDebug("Doing Space Splitter Search and Replace on ABC/FBX File.")
						text, msg = SpaceSplitterSearchAndReplace(text, itm.SearchForHistory.CurrentText, itm.ReplaceWithHistory.CurrentText, caseSensitive, spaceSplitter)
						PrintDebug("Cleaning up Pattern Breaker in ABC/FBX File")
						text = CleanPatternBreaker(text)
					else
						text, msg = SearchAndReplace(text, itm.SearchForHistory.CurrentText, itm.ReplaceWithHistory.CurrentText, caseSensitive)

					end
					if oldText == text or text == "" or text == nil then
						PrintDebug("ABC/FBX File is identical. No replacement is necessary on: ", oldText)
					else
						PrintDebug("ABC/FBX File is being changed from: ", oldText, " to: ", text )
						if tool.ImportFile then
							tool.ImportFile[1] = text
						elseif tool.Filename then
							tool.Filename[1] = text
						end
					end
				end
				Notify("Changing ABC/FBX: ", i, "/", toolCount)
			end
			composition:EndUndo('Change ABC/FBX File')
		end


		Notify("Completed running on flow tools")
	end

	if itm.PageTabs.CurrentIndex == 1 then
		Notify("Running On Versioning")
	end

	if itm.PageTabs.CurrentIndex == 2 then
		Notify("Running in Textbox")
		itm.DoOpen.Checked = true
		local text = itm.TextBoxIn.PlainText
		if spaceSplitter == true then
			PrintDebug("Doing Space Splitter Search and Replace.")
			text, msg = SpaceSplitterSearchAndReplace(itm.TextBoxIn.PlainText, itm.SearchForHistory.CurrentText, itm.ReplaceWithHistory.CurrentText, caseSensitive, spaceSplitter)
			Notify("Cleaning up Pattern Breaker in Textbox")
			text = CleanPatternBreaker(text)
		else
			text, msg = SearchAndReplace(itm.TextBoxIn.PlainText, itm.SearchForHistory.CurrentText, itm.ReplaceWithHistory.CurrentText, caseSensitive)

		end
		itm.TextBoxOut.Text = text
		Notify("Finished running in Textbox.", msg)

	end

	if itm.DoRemeber.Checked == true then --Saves the current states of the Checkboxes and Strings in the Fusion preferences file
		SearchForTable = AddToTable(SearchForTable, itm.SearchForHistory.CurrentText)
		ReplaceWithTable = AddToTable(ReplaceWithTable, itm.ReplaceWithHistory.CurrentText)
		FillComboBox(SearchForTable, itm.SearchForHistory )
		FillComboBox(ReplaceWithTable, itm.ReplaceWithHistory )	
		for i, entry in pairs(SearchForTable) do
            s = "Comp.ChangeStrings.OldSearchForHistory." .. i
            fusion:SetPrefs({[s] = entry})
        end
        for i, entry in pairs(ReplaceWithTable) do
            s = "Comp.ChangeStrings.OldReplaceWithHistory." .. i
            fusion:SetPrefs({[s] = entry})
        end
		fusion:SetPrefs({['Comp.ChangeStrings.OldDataSaved']='Yes',
																			['Comp.ChangeStrings.OldDoExpressions']=itm.DoExpressions:GetCheckState(),
																			['Comp.ChangeStrings.OldDoNames']=itm.DoNames:GetCheckState(),
																			['Comp.ChangeStrings.OldDoStyledText']=itm.DoStyledText:GetCheckState(),
																			['Comp.ChangeStrings.OldDoLoader']=itm.DoLoader:GetCheckState(),
																			['Comp.ChangeStrings.OldDoProxy']=itm.DoProxy:GetCheckState(),
																			['Comp.ChangeStrings.OldDoSaver']=itm.DoSaver:GetCheckState(),
																			['Comp.ChangeStrings.OldDoFBXFile']=itm.DoFBXFile:GetCheckState(),
																			['Comp.ChangeStrings.OldDoObjName']=itm.DoObjName:GetCheckState(),
																			['Comp.ChangeStrings.OldDoModifier']=itm.DoModifier:GetCheckState(),
																			['Comp.ChangeStrings.OldDoOpen']=itm.DoOpen:GetCheckState(),
																			['Comp.ChangeStrings.OldDoCaseSensitive']=itm.DoCaseSensitive:GetCheckState(),
																			['Comp.ChangeStrings.OldDoSpaceSplitter']=itm.DoSpaceSplitter:GetCheckState(),
																			['Comp.ChangeStrings.OldDoRemember']=itm.DoRemeber:GetCheckState(),
																			['Comp.ChangeStrings.OldTabIndex']=itm.PageTabs.CurrentIndex,
																			})
	else
		fusion:SetPrefs({['Comp.ChangeStrings.OldDataSaved']='Yes',
																			['Comp.ChangeStrings.OldDoRemember']=itm.DoRemeber:GetCheckState(),
																			})

	end




	comp:Unlock() --unlocks the comp
	if itm.DoOpen.Checked == true then
	print('Change Strings stays open')
	else


				print('_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _')
				print(' ')
				print('Change Strings was closed')
				print('_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _')
				print(' ')

	disp:ExitLoop()
	end


end
win:Show()
disp:RunLoop()
win:Hide()


{
	Tools = ordered() {
		kas_Xray = GroupOperator {
			CtrlWZoom = false,
			NameSet = true,
			CustomData = {
				Settings = {
				},
				HelpPage = "https://www.steakunderwater.com/wesuckless/viewtopic.php?p=25255#p25255"
			},
			Inputs = ordered() {
				Comments = Input { Value = "\"kas_Xray\" creates a stylized and colourful NPR rendering effect of a medical X-ray material.\n\nCopyright Credits \nkas_Parkland.exr\n\nCreative Commons:\nEgg mountain at afternoon \n\nBy:\nhttps://hdrmaps.com \n\nIs licensed under:\nCC BY 2.0\n", },
				TextureMap = InstanceInput {
					SourceOp = "TextureInputSwitchElseFuse",
					Source = "Input1",
					Name = "TextureMap",
				},
				EnvironmentMap = InstanceInput {
					SourceOp = "EnvironmentMapInputSwitchElseFuse",
					Source = "Input1",
					Name = "EnvironmentMap",
				}
			},
			Outputs = {
				Material = InstanceOutput {
					SourceOp = "ShaderPipeRouter",
					Source = "Output",
					Name = "Material Output",
				}
			},
			ViewInfo = GroupInfo {
				Pos = { 54.3333, 17.7121 },
				Flags = {
					AllowPan = false,
					AutoSnap = true
				},
				Size = { 325.642, 223.327, 367.036, 0.991373 },
				Direction = "Horizontal",
				PipeStyle = "Direct",
				Scale = 1,
				Offset = { -1381.99, -285.673 }
			},
			Tools = ordered() {
				ParklandLoader = Loader {
					Clips = {
						Clip {
							ID = "Clip1",
							Filename = "Macros:/KickAss ShaderZ/Assets/kas_Parkland.exr",
							FormatID = "OpenEXRFormat",
							StartFrame = -1,
							LengthSetManually = true,
							TrimIn = 0,
							TrimOut = 0,
							ExtendFirst = 0,
							ExtendLast = 0,
							Loop = 0,
							AspectMode = 0,
							Depth = 0,
							TimeCode = 0,
							GlobalStart = 0,
							GlobalEnd = 0
						}
					},
					NameSet = true,
					Inputs = {
						["Gamut.SLogVersion"] = Input { Value = FuID { "SLog2" }, },
						["Clip1.OpenEXRFormat.RedName"] = Input { Value = FuID { "R" }, },
						["Clip1.OpenEXRFormat.GreenName"] = Input { Value = FuID { "G" }, },
						["Clip1.OpenEXRFormat.BlueName"] = Input { Value = FuID { "B" }, },
						["Clip1.OpenEXRFormat.AlphaName"] = Input { Value = FuID { "A" }, },
						Comments = Input { Value = "Copyright Credits \nkas_Parkland.exr\n\nCreative Commons:\nEgg mountain at afternoon \n\nBy:\nhttps://hdrmaps.com \n\nIs licensed under:\nCC BY 2.0\n", },
					},
					ViewInfo = OperatorInfo { Pos = { 1210.67, 314.106 } },
				},
				AgedSteelPlateLoader = Loader {
					Clips = {
						Clip {
							ID = "Clip1",
							Filename = "Macros:/KickAss ShaderZ/Assets/kas_AgedSteelPlate.jpg",
							FormatID = "JpegFormat",
							StartFrame = -1,
							LengthSetManually = true,
							TrimIn = 0,
							TrimOut = 0,
							ExtendFirst = 0,
							ExtendLast = 0,
							Loop = 0,
							AspectMode = 0,
							Depth = 0,
							TimeCode = 0,
							GlobalStart = 0,
							GlobalEnd = 0
						}
					},
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						["Gamut.SLogVersion"] = Input { Value = FuID { "SLog2" }, },
					},
					ViewInfo = OperatorInfo { Pos = { 1100, 313.5 } },
				},
				ShaderPipeRouter = PipeRouter {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						Input = Input {
							SourceOp = "XrayReflect",
							Source = "MaterialOutput",
						},
					},
					ViewInfo = PipeRouterInfo { Pos = { 1320, 478.5 } },
				},
				TextureInputSwitchElseFuse = Fuse.SwitchElse {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						Switch = Input { Value = 1, },
						Else = Input { Value = 2, },
						Input2 = Input {
							SourceOp = "AgedSteelPlateLoader",
							Source = "Output",
						},
						Comments = Input { Value = "The \"SwitchElse.fuse\" is used to automatically switch the surface material between user supplied imagery, and the fallback option of the internal \"default\" imagery. This input switching happens when nothing is connected to the  surface material's \"GroupOperator\" based \"TextureMap\" and \"EnvironmentMap\" input connections.", },
					},
					ViewInfo = OperatorInfo { Pos = { 1100, 346.5 } },
					Version = 100
				},
				EnvironmentMapInputSwitchElseFuse = Fuse.SwitchElse {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						Switch = Input { Value = 1, },
						Else = Input { Value = 2, },
						Input2 = Input {
							SourceOp = "ParklandLoader",
							Source = "Output",
						},
						Comments = Input { Value = "The \"SwitchElse.fuse\" is used to automatically switch the surface material between user supplied imagery, and the fallback option of the internal \"default\" imagery. This input switching happens when nothing is connected to the  surface material's \"GroupOperator\" based \"TextureMap\" and \"EnvironmentMap\" input connections.", },
					},
					ViewInfo = OperatorInfo { Pos = { 1210, 346.5 } },
					Version = 100
				},
				XrayReflect = MtlReflect {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						BackgroundMaterial = Input {
							SourceOp = "AgedFalloff",
							Source = "MaterialOutput",
						},
						["Reflection.GlancingStrength"] = Input { Value = 0, },
						["Reflection.FaceOnStrength"] = Input { Value = 0.712990936555891, },
						["Reflection.Falloff"] = Input { Value = 1.87311178247734, },
						["Reflection.Color.Material"] = Input {
							SourceOp = "GreenFuzzyDetailsEdgeFalloff",
							Source = "MaterialOutput",
						},
						["Reflection.Intensity.Material"] = Input {
							SourceOp = "RedToAlphaChannelBooleans",
							Source = "Output",
						},
						["Refraction.RefractiveIndex.SeparateRGBIndices"] = Input { Value = 1, },
						["Refraction.RefractiveIndex.RGB"] = Input { Value = 0.537222222222222, },
						["Refraction.Tint.Red"] = Input { Value = 0, },
						["Refraction.Tint.Blue"] = Input { Value = 0.774791156070233, },
						MaterialID = Input { Value = 82, },
					},
					ViewInfo = OperatorInfo { Pos = { 1210, 478.5 } },
				},
				GreenFuzzyDetailsEdgeFalloff = FalloffOperator {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						["FaceOn.Nest"] = Input { Value = 0, },
						["FaceOn.Red"] = Input { Value = 0.91228, },
						["FaceOn.Blue"] = Input { Value = 0.745, },
						["FaceOn.Opacity"] = Input { Value = 1, },
						["Glancing.Red"] = Input { Value = 0.0473280000000003, },
						["Glancing.Green"] = Input { Value = 0.102, },
						["Glancing.Blue"] = Input { Value = 0, },
						Falloff = Input { Value = 2.734, },
						FaceOnMaterial = Input {
							SourceOp = "ParklandSphereMap",
							Source = "MaterialOutput",
						},
						MaterialID = Input { Value = 7, },
					},
					ViewInfo = OperatorInfo { Pos = { 1210, 445.5 } },
				},
				ParklandSphereMap = SphereMap {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						Rotation = Input { Value = 1, },
						["Rotate.Y"] = Input { Value = -65, },
						Image = Input {
							SourceOp = "ParklandBlur",
							Source = "Output",
						},
						MaterialID = Input { Value = 15, },
					},
					ViewInfo = OperatorInfo { Pos = { 1210, 412.5 } },
				},
				ParklandBlur = Blur {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						Filter = Input { Value = FuID { "Gaussian" }, },
						XBlurSize = Input { Value = 15, },
						Input = Input {
							SourceOp = "EnvironmentMapInputSwitchElseFuse",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { 1210, 380.712 } },
				},
				AgedCyanWard = MtlWard {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						["Diffuse.Color.Red"] = Input { Value = 0.521244444444444, },
						["Diffuse.Color.Green"] = Input { Value = 0.799851500270601, },
						["Diffuse.Color.Material"] = Input {
							SourceOp = "RedToAlphaChannelBooleans",
							Source = "Output",
						},
						["Diffuse.Opacity"] = Input { Value = 0.504531722054381, },
						["Specular.Nest"] = Input { Value = 1, },
						["Specular.Intensity"] = Input { Value = 0, },
						["Transmittance.Nest"] = Input { Value = 1, },
						MaterialID = Input { Value = 4, },
					},
					ViewInfo = OperatorInfo { Pos = { 1100, 445.5 } },
				},
				AgedFalloff = FalloffOperator {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						ColorVariation = Input { Value = FuID { "Gradient" }, },
						["FaceOn.Opacity"] = Input { Value = 0.5105263, },
						["Glancing.Red"] = Input { Value = 0, },
						["Glancing.Green"] = Input { Value = 0, },
						["Glancing.Blue"] = Input { Value = 0, },
						["Glancing.Alpha"] = Input { Value = 0, },
						Gradient = Input {
							Value = Gradient {
								Colors = {
									[0] = { 1, 0.96, 0, 0 },
									[1] = { 0, 0.671689643756996, 1, 0 }
								}
							},
						},
						Falloff = Input { Value = 0.462, },
						FaceOnMaterial = Input {
							SourceOp = "AgedCyanWard",
							Source = "MaterialOutput",
						},
						MaterialID = Input { Value = 9, },
					},
					ViewInfo = OperatorInfo { Pos = { 1100, 478.5 } },
				},
				RedToAlphaChannelBooleans = ChannelBoolean {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						ToAlpha = Input { Value = 5, },
						Background = Input {
							SourceOp = "AgedHighContrastBrightnessContrast",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { 1100, 412.5 } },
				},
				AgedHighContrastBrightnessContrast = BrightnessContrast {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						Saturation = Input { Value = 0, },
						Low = Input { Value = 0.2985507, },
						High = Input { Value = 0.4985507, },
						Input = Input {
							SourceOp = "TextureInputSwitchElseFuse",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { 1100, 379.5 } },
				}
			},
		}
	},
	ActiveTool = "kas_Xray"
}
{
	Tools = ordered() {
		kas_HeatShield = GroupOperator {
			CtrlWZoom = false,
			NameSet = true,
			CustomData = {
				Settings = {
				},
				HelpPage = "https://www.steakunderwater.com/wesuckless/viewtopic.php?p=25255#p25255"
			},
			Inputs = ordered() {
				Comments = Input { Value = "\"kas_HeatShield\" generates a super-heated \"plasma\" like fresnel shading effect that can be used to create earth atmosphere re-entry shading effects for spacecraft or metorites. The material can also be modified to augment the look of tracer bullets, incendiary rounds, or futuristic ballistic weaponry that need a plasma like heat bubble that wraps around the projectile.\n\nWhen you render the \"kas_HeatShield\" surface material, don't forget to explore adding an exponential glow effects to the final frame to increase the incandescent luminoous feel of the material. The \"TextureMap\" input connection on the node supports images with alpha channels which can punch holes through the final material for a patch-work shading effect.\n\nCopyright Credits \nkas_Parkland.exr\n\nCreative Commons:\nEgg mountain at afternoon \n\nBy:\nhttps://hdrmaps.com \n\nIs licensed under:\nCC BY 2.0", },
				TextureMap = InstanceInput {
					SourceOp = "TextureInputSwitchElseFuse",
					Source = "Input1",
					Name = "TextureMap",
				},
				EnvironmentMap = InstanceInput {
					SourceOp = "EnvironmentMapInputSwitchElseFuse",
					Source = "Input1",
					Name = "EnvironmentMap",
				}
			},
			Outputs = {
				Material = InstanceOutput {
					SourceOp = "HeatShieldPipeRouter",
					Source = "Output",
					Name = "Material Output",
				}
			},
			ViewInfo = GroupInfo {
				Pos = { -55, 478.5 },
				Flags = {
					AllowPan = false,
					AutoSnap = true
				},
				Size = { 467.09, 307.916, 64.3233, -7.7 },
				Direction = "Horizontal",
				PipeStyle = "Direct",
				Scale = 1,
				Offset = { -1381.99, -269.8 }
			},
			Tools = ordered() {
				ParklandLoader = Loader {
					Clips = {
						Clip {
							ID = "Clip1",
							Filename = "Macros:/KickAss ShaderZ/Assets/kas_Parkland.exr",
							FormatID = "OpenEXRFormat",
							StartFrame = -1,
							LengthSetManually = true,
							TrimIn = 0,
							TrimOut = 0,
							ExtendFirst = 0,
							ExtendLast = 0,
							Loop = 0,
							AspectMode = 0,
							Depth = 0,
							TimeCode = 0,
							GlobalStart = 0,
							GlobalEnd = 0
						}
					},
					NameSet = true,
					Inputs = {
						["Gamut.SLogVersion"] = Input { Value = FuID { "SLog2" }, },
						["Clip1.OpenEXRFormat.RedName"] = Input { Value = FuID { "R" }, },
						["Clip1.OpenEXRFormat.GreenName"] = Input { Value = FuID { "G" }, },
						["Clip1.OpenEXRFormat.BlueName"] = Input { Value = FuID { "B" }, },
						["Clip1.OpenEXRFormat.AlphaName"] = Input { Value = FuID { "A" }, },
						Comments = Input { Value = "Copyright Credits \nkas_Parkland.exr\n\nCreative Commons:\nEgg mountain at afternoon \n\nBy:\nhttps://hdrmaps.com \n\nIs licensed under:\nCC BY 2.0", },
					},
					ViewInfo = OperatorInfo { Pos = { 1650, 313.5 } },
				},
				AgedSteelPlateLoader = Loader {
					Clips = {
						Clip {
							ID = "Clip1",
							Filename = "Macros:/KickAss ShaderZ/Assets/kas_AgedSteelPlate.jpg",
							FormatID = "JpegFormat",
							StartFrame = -1,
							LengthSetManually = true,
							TrimIn = 0,
							TrimOut = 0,
							ExtendFirst = 0,
							ExtendLast = 0,
							Loop = 0,
							AspectMode = 0,
							Depth = 0,
							TimeCode = 0,
							GlobalStart = 0,
							GlobalEnd = 0
						}
					},
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						["Gamut.SLogVersion"] = Input { Value = FuID { "SLog2" }, },
					},
					ViewInfo = OperatorInfo { Pos = { 1430, 313.5 } },
				},
				EnvironmentMapInputSwitchElseFuse = Fuse.SwitchElse {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						Switch = Input { Value = 1, },
						Else = Input { Value = 2, },
						Input2 = Input {
							SourceOp = "ParklandLoader",
							Source = "Output",
						},
						Comments = Input { Value = "The \"SwitchElse.fuse\" is used to automatically switch the surface material between user supplied imagery, and the fallback option of the internal \"default\" imagery. This input switching happens when nothing is connected to the  surface material's \"GroupOperator\" based \"TextureMap\" and \"EnvironmentMap\" input connections.", },
					},
					ViewInfo = OperatorInfo { Pos = { 1650, 347.298 } },
					Version = 100
				},
				TextureInputSwitchElseFuse = Fuse.SwitchElse {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						Switch = Input { Value = 1, },
						Else = Input { Value = 2, },
						Input2 = Input {
							SourceOp = "AgedSteelPlateLoader",
							Source = "Output",
						},
						Comments = Input { Value = "The \"SwitchElse.fuse\" is used to automatically switch the surface material between user supplied imagery, and the fallback option of the internal \"default\" imagery. This input switching happens when nothing is connected to the  surface material's \"GroupOperator\" based \"TextureMap\" and \"EnvironmentMap\" input connections.", },
					},
					ViewInfo = OperatorInfo { Pos = { 1430, 346.5 } },
					Version = 100
				},
				HeatShieldPipeRouter = PipeRouter {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						Input = Input {
							SourceOp = "FireyOrangeReflect",
							Source = "MaterialOutput",
						},
					},
					ViewInfo = PipeRouterInfo { Pos = { 1760, 544.5 } },
				},
				ParklandBlur = Blur {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						Filter = Input { Value = FuID { "Gaussian" }, },
						XBlurSize = Input { Value = 15, },
						Input = Input {
							SourceOp = "ParklandBrightnessContrast",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { 1650, 413.106 } },
				},
				ParklandBrightnessContrast = BrightnessContrast {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						Input = Input {
							SourceOp = "EnvironmentMapInputSwitchElseFuse",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { 1650, 379.5 } },
				},
				FireyOrangeReflect = MtlReflect {
					CtrlWShown = false,
					NameSet = true,
					CurrentSettings = 2,
					CustomData = {
						Settings = {
							[1] = {
								Tools = ordered() {
									Reflect1_3_6_1_1_1_1 = MtlReflect {
										Inputs = {
											["Reflection.Color.Material"] = Input {
												SourceOp = "Falloff2_1_1_1_1",
												Source = "MaterialOutput"
											},
											["Refraction.RefractiveIndex.Blue"] = Input { Value = 0.519069767441861 },
											["Refraction.Tint.Blue"] = Input { Value = 0.615686274509804 },
											["Refraction.RefractiveIndex.Green"] = Input { Value = 0.981860465116279 },
											MaterialID = Input { Value = 82 },
											["Refraction.Tint.Green"] = Input { Value = 0.827450980392157 },
											["Refraction.RefractiveIndex.SeparateRGBIndices"] = Input { Value = 1 },
											["Reflection.Intensity.Material"] = Input {
												SourceOp = "ChannelBooleans10_2_1_1_1_1",
												Source = "Output"
											},
											["Reflection.Falloff"] = Input { Value = 1.13488372093023 },
											["Refraction.RefractiveIndex.RGB"] = Input { Value = 0.537222222222222 },
											["Reflection.StrengthVariability"] = Input { Value = 0 },
											["Reflection.FaceOnStrength"] = Input { Value = 0.530232558139535 },
											["Reflection.GlancingStrength"] = Input { Value = 0.604651162790698 },
											BackgroundMaterial = Input {
												SourceOp = "Falloff1_1_1_1_1",
												Source = "MaterialOutput"
											},
											["Refraction.RefractiveIndex.Red"] = Input { Value = 2 },
											["Bumpmap.Material"] = Input {
												SourceOp = "BumpMap1_3",
												Source = "MaterialOutput"
											}
										},
										CtrlWZoom = false,
										ViewInfo = OperatorInfo { Pos = { 55, -49.5 } },
										CustomData = {
										}
									}
								}
							}
						}
					},
					Inputs = {
						BackgroundMaterial = Input {
							SourceOp = "FireyOrangeFalloff",
							Source = "MaterialOutput",
						},
						["Reflection.ConstantStrength"] = Input { Value = 0.92093023255814, },
						["Reflection.GlancingStrength"] = Input { Value = 0, },
						["Reflection.FaceOnStrength"] = Input { Value = 0.293023255813953, },
						["Reflection.Falloff"] = Input { Value = 4, },
						["Reflection.Color.Material"] = Input {
							SourceOp = "DarkCoreFalloff",
							Source = "MaterialOutput",
						},
						["Reflection.Intensity.Material"] = Input {
							SourceOp = "RedToAlphaChannelBooleans",
							Source = "Output",
						},
						["Refraction.RefractiveIndex.SeparateRGBIndices"] = Input { Value = 1, },
						["Refraction.RefractiveIndex.RGB"] = Input { Value = 0.537222222222222, },
						["Refraction.RefractiveIndex.Red"] = Input { Value = 0.46353488372093, },
						["Refraction.RefractiveIndex.Green"] = Input { Value = 0.01, },
						["Refraction.RefractiveIndex.Blue"] = Input { Value = 0.452046511627907, },
						["Refraction.Tint.Green"] = Input { Value = 0.827450980392157, },
						["Refraction.Tint.Blue"] = Input { Value = 0.615686274509804, },
						MaterialID = Input { Value = 82, },
					},
					ViewInfo = OperatorInfo { Pos = { 1650, 544.5 } },
				},
				FireyOrangeFalloff = FalloffOperator {
					CtrlWShown = false,
					NameSet = true,
					CurrentSettings = 2,
					CustomData = {
						Settings = {
							[1] = {
								Tools = ordered() {
									Falloff1_1_1_1_1 = FalloffOperator {
										Inputs = {
											["Glancing.Alpha"] = Input { Value = 0 },
											FaceOnMaterial = Input {
												SourceOp = "Ward1_2_1_1_1",
												Source = "MaterialOutput"
											},
											["Glancing.Green"] = Input { Value = 0 },
											["FaceOn.Opacity"] = Input { Value = 0.5105263 },
											["Glancing.Blue"] = Input { Value = 0 },
											["Glancing.Red"] = Input { Value = 0 },
											ColorVariation = Input { Value = FuID { "Gradient" } },
											Gradient = Input {
												Value = Gradient {
													Colors = {
														[0] = { 0, 0.671689643756996, 1, 0 },
														[1] = { 0, 0.671689643756996, 1, 1 }
													}
												}
											},
											MaterialID = Input { Value = 9 },
											Falloff = Input { Value = 1.52706552706553 }
										},
										CtrlWZoom = false,
										ViewInfo = OperatorInfo { Pos = { -165, -49.5 } },
										CustomData = {
										}
									}
								}
							}
						}
					},
					Inputs = {
						["FaceOn.Nest"] = Input { Value = 0, },
						["FaceOn.Opacity"] = Input { Value = 1, },
						["Glancing.Green"] = Input { Value = 0.432, },
						["Glancing.Blue"] = Input { Value = 0, },
						["Glancing.Alpha"] = Input { Value = 0, },
						Gradient = Input {
							Value = Gradient {
								Colors = {
									[0] = { 0, 0.112, 1, 1 },
									[1] = { 0.986666666666667, 1, 1, 1 }
								}
							},
						},
						Falloff = Input { Value = 0.27, },
						FaceOnMaterial = Input {
							SourceOp = "AgedGreyWard",
							Source = "MaterialOutput",
						},
						MaterialID = Input { Value = 9, },
					},
					ViewInfo = OperatorInfo { Pos = { 1540, 543.894 } },
				},
				DarkCoreFalloff = FalloffOperator {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						["FaceOn.Nest"] = Input { Value = 0, },
						["FaceOn.Opacity"] = Input { Value = 1, },
						["Glancing.Red"] = Input { Value = 0, },
						["Glancing.Green"] = Input { Value = 0.88, },
						["Glancing.Opacity"] = Input { Value = 0, },
						Falloff = Input { Value = 0, },
						FaceOnMaterial = Input {
							SourceOp = "ParklandSphereMap",
							Source = "MaterialOutput",
						},
						MaterialID = Input { Value = 7, },
					},
					ViewInfo = OperatorInfo { Pos = { 1650, 478.5 } },
				},
				ParklandSphereMap = SphereMap {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						Rotation = Input { Value = 1, },
						["Rotate.Y"] = Input { Value = -65, },
						Image = Input {
							SourceOp = "ParklandBlur",
							Source = "Output",
						},
						MaterialID = Input { Value = 15, },
					},
					ViewInfo = OperatorInfo { Pos = { 1650, 445.5 } },
				},
				AgedHighContrastBrightnessContrast = BrightnessContrast {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						Saturation = Input { Value = 0, },
						Low = Input { Value = 0.2985507, },
						High = Input { Value = 0.4985507, },
						Input = Input {
							SourceOp = "TextureInputSwitchElseFuse",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { 1430, 381.018 } },
				},
				RedToAlphaChannelBooleans = ChannelBoolean {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						ToAlpha = Input { Value = 5, },
						Background = Input {
							SourceOp = "AgedHighContrastBrightnessContrast",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { 1430, 414.018 } },
				},
				AgedGreyWard = MtlWard {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						["Diffuse.Color.Red"] = Input { Value = 0.938009082816474, },
						["Diffuse.Color.Green"] = Input { Value = 0.92819780257154, },
						["Diffuse.Color.Material"] = Input {
							SourceOp = "RedToAlphaChannelBooleans",
							Source = "Output",
						},
						["Specular.Nest"] = Input { Value = 1, },
						["Specular.Intensity"] = Input { Value = 0, },
						["Specular.SpreadU"] = Input { Value = 0.01, },
						["Specular.SpreadV"] = Input { Value = 0.01, },
						["Transmittance.Nest"] = Input { Value = 1, },
						["Transmittance.Color.Red"] = Input { Value = 0.94008432457342, },
						["Transmittance.Color.Green"] = Input { Value = 1, },
						["Transmittance.Color.Blue"] = Input { Value = 0.905719095841794, },
						["Transmittance.ColorDetail"] = Input { Value = 0.655813953488372, },
						["Transmittance.Saturation"] = Input { Value = 0.52093023255814, },
						MaterialID = Input { Value = 4, },
					},
					ViewInfo = OperatorInfo { Pos = { 1430, 545.106 } },
				}
			},
		}
	},
	ActiveTool = "kas_HeatShield"
}
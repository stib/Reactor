--[[--
----------------------------------------------------------------------------
View Help Documentation - v5 2023-12-25
by Andrew Hazelden
www.andrewhazelden.com
andrew@andrewhazelden.com

KartaVR
https://www.andrewhazelden.com/projects/kartavr/docs/
----------------------------------------------------------------------------
Overview:
The View Help Documentation script is a module from [KartaVR](http://www.andrewhazelden.com/projects/kartavr/docs/) that will open a web browser window to and display the HTML formatted help documentation.

How to use the Script:
Step 1. Start Fusion and open a new comp. Then run the Script > KartaVR > View Help Documentation menu item.

--]]--

-- --------------------------------------------------------
-- --------------------------------------------------------
-- --------------------------------------------------------
-- Print out extra debugging information
local printStatus = false


-- Open a web browser window up with the help documentation
function openBrowser()
	webpage = "https://andrewhazelden.com/projects/kartavr/docs/"
	bmd.openurl(webpage)
end

-- Open a web browser window up with the help documentation
openBrowser()

-- End of the script
print('[Done]')
return

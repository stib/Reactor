--[[--
--]]--


-- ============================================================================
-- modules
-- ============================================================================

-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vImageToFile"
DATATYPE = "Image"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_SinkTool, {
	REGID_DataType = DATATYPE,
	REGID_InputDataType = DATATYPE,
	REGS_Name = FUSE_NAME,
	REGS_Category = "Kartaverse\\Vonk Ultra\\Image\\IO",
	REGS_OpDescription = "Save a jpg/exr/png/bmp/raw/fusepic image to disk.",
	REG_SupportsDoD = true,
	REG_OpNoMask = true,
	REG_NoMotionBlurCtrls = true,

	-- Should the current time setting be cached?
	REG_TimeVariant = true,
	REG_Unpredictable = true,

	-- Should the Edit and Reload buttons be hidden?
	REG_Fuse_NoEdit = false,
	REG_Fuse_NoReload = false,
	
	-- Sets the fuse version number (100 means v1.0) so newer fuses override older versions
	REG_Version = 100,
	})

-- Find out the current directory from a file path
-- Example: print(Dirname('/Users/Shared/image.0000.exr'))
function Dirname(mediaDirName)
	if mediaDirName then
		return mediaDirName:match("(.*[/\\])")
	else
		return nil
	end
end

function Create()
	InFile = self:AddInput("File", "File", {
		LINKID_DataType = "Text",
		INPID_InputControl = "FileControl",
		FC_IsSaver = true,
		FC_ClipBrowse = true,
		-- ICS_ControlPage = "File",
		LINK_Main = 2,
	})

--	InViewFile = self:AddInput('View File', 'View File', {
--		LINKID_DataType = 'Number',
--		INPID_InputControl = 'ButtonControl',
--		INP_DoNotifyChanged = true,
--		INP_External = false,
--		ICD_Width = 1,
--		INP_Passive = true,
--		IC_Visible = true,
--		BTNCS_Execute = [[
---- check if a tool is selected
--local selectedNode = tool or comp.ActiveTool
--if selectedNode then
--	local filename = comp:MapPath(selectedNode:GetInput('File'))
--	if filename then
--		if bmd.fileexists(filename) then
--			bmd.openfileexternal('Open', filename)
--		else
--			print('[View File] File does not exist yet:', filename)
--		end
--	else
--		print('[View File] Filename is nil.')
--	end
--else
--	print('[View File] Please select the node.')
--end
--		]],
--	})

	InShowInput = self:AddInput("Show Input", "ShowInput", {
		LINKID_DataType = "Number",
		INPID_InputControl = "CheckboxControl",
		INP_Integer = true,
		INP_Default = 0.0,
		INP_External = false,
		INP_DoNotifyChanged = true
	})

	InImage = self:AddInput("Input", "Input", {
		LINKID_DataType = "Image",
		LINK_Main = 1,
	})

	OutImage = self:AddOutput("Output", "Output", {
		LINKID_DataType = "Image",
		LINK_Main = 1,
	})
end


function NotifyChanged(inp, param, time)
	--[[
			Handles all input control events.

			:param inp: Input that triggered a signal.
			:type inp: Input

			:param param: Parameter object holding the (new) value.
			:type param: Parameter

			:param time: Current frame number.
			:type time: float
	]]
	if inp == InShowInput then
		local visible
		if param.Value == 1.0 then visible = true else visible = false end

		InFile:SetAttrs({LINK_Visible = visible})
	end
end


-- The OnAddToFlow() function is automatically run when the a new fuse node is added to the comp, or when the .comp file is opened up.
function OnAddToFlow()
end


function Process(req)
	-- Grab an image from the input connection on the node
	local img = InImage:GetValue(req)
	
	-- Crop (with no offset, ie. Copy) handles images having no data, so we don't need to put this within if/then/end
	local result = Image({IMG_Like = img, IMG_NoData = req:IsPreCalc()})
	img:Crop(result, {})
	
	-- Read the filename
	local filename = self.Comp:MapPath(InFile:GetValue(req).Value)
	
	-- Print a warning message when the filename field is empty
	if filename == nil or filename == '' then
		error('[vImageToFile] The "Filename" field is empty!\n\n')
	else
		-- Get the 'final' frame number that will be written into the filename
		local currentFrame = tonumber(req.Time)
		
		-- Create the folder if it doesn't exist yet
		local folder = Dirname(self.Comp:MapPath(filename))
		if bmd.fileexists(folder) == false then
			bmd.createdir(folder)
			-- print('[Created Directory]', folder)
		end
		-- Reverify if the folder was able to be created
		if bmd.fileexists(folder) == false then
			-- print('[Directory Missing]', folder)
		end
		
		-- Create a new clip with a filename
		local saveBool = true
		local clip = Clip(filename, saveBool)
		
		-- Write an image to disk if the Clip:Open() and Clip:Close() functions exist in Fusion
		clip:Open()
		local putResult = clip:PutFrame(currentFrame, result)
		clip:Close()

		-- print('[PutFrame Filename]', clip.Filename)
		-- print('[PutFrame Result]', putResult)
	end
	
	-- Pass the result image to the output connection
	OutImage:Set(req, result)
end


-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vImageDelay"
DATATYPE = "Image"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = DATATYPE,
    REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\Image\\Utility",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Creates a Delay while passing a Fusion Image object.",
    REGS_OpIconString = FUSE_NAME,
    REG_TimeVariant = true, -- required to disable caching of the current time parameter
    REGB_Temporal = true, -- ensures reliability in Resolve 15
})

function Create()
    InImage = self:AddInput("Input", "Input", {
        LINKID_DataType = "Image",
        LINK_Main = 1,
    })

    InDelay = self:AddInput("Delay", "Delay", {
        LINKID_DataType    = "Number",
        INPID_InputControl = "ScrewControl",
        INP_MinAllowed     = 0,
        INP_MaxAllowed     = 60,
        INP_MaxScale       = 60,
        INP_Integer        = false,
        IC_Steps           = 1.0,
        LINK_Main       = 2,
    })

    InShowInput = self:AddInput("Show Input", "ShowInput", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 1.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    OutImage = self:AddOutput("Output", "Output", {
        LINKID_DataType = "Image",
        LINK_Main = 1,
    })
end


function NotifyChanged(inp, param, time)
    --[[
        Handles all input control events.

        :param inp: Input that triggered a signal.
        :type inp: Input

        :param param: Parameter object holding the (new) value.
        :type param: Parameter

        :param time: Current frame number.
        :type time: float
    ]]
    if inp == InShowInput then
        local visible
        if param.Value == 1.0 then visible = true else visible = false end

        InDelay:SetAttrs({LINK_Visible = visible})
    end
end

function Process(req)
    -- [[ Creates the output. ]]
    local img = InImage:GetValue(req)
    local delay = InDelay:GetValue(req).Value

    bmd.wait(tonumber(delay))
    
    -- Crop (with no offset, ie. Copy) handles images having no data, so we don't need to put this within if/then/end
    local result = Image({IMG_Like = img, IMG_NoData = req:IsPreCalc()})
    img:Crop(result, {})
    --dump(result)

    OutImage:Set(req, result)
end

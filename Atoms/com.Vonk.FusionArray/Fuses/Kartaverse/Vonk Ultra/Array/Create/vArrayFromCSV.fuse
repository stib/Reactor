-- ============================================================================
-- modules
-- ============================================================================
local textutils = self and require("vtextutils") or nil
local jsonutils = self and require("vjsonutils") or nil
local arrayutils = self and require("varrayutils") or nil

-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vArrayFromCSV"
DATATYPE = "Text" 

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = DATATYPE,
    REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\Array\\Create",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Creates a JSON array from a CSV row or column",
    REGS_OpIconString = FUSE_NAME,
    -- Should the current time setting be cached?
    REG_TimeVariant = true,
    REG_Unpredictable = true,
})

function Create()
    -- [[ Creates the user interface. ]]
    InMode = self:AddInput("Array Mode", "Mode", {
        LINKID_DataType = "Number",
        INPID_InputControl = "MultiButtonControl",
        MBTNC_ForceButtons = true,
        --MBTNC_ShowBasicButton = false,
        MBTNC_ShowName = true,
        INP_External = false,
        INP_Default = 0,
        {MBTNC_AddButton = "Extract Row", MBTNCD_ButtonWidth = 0.5},
        {MBTNC_AddButton = "Extract Column", MBTNCD_ButtonWidth = 0.5},
        {MBTNC_AddButton = "Grid Row", MBTNCD_ButtonWidth = 0.5},
        {MBTNC_AddButton = "Grid Column", MBTNCD_ButtonWidth = 0.5},
        INP_DoNotifyChanged = true
    })
    InIndex = self:AddInput("Index", "index", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ScrewControl",
        INP_MinScale = 1,
        INP_MaxScale = 144,
        INP_Default = 1,
        INP_MinAllowed = 1,
        INP_MaxAllowed = 1e+38,
        -- INP_MaxAllowed = 1000000,
        INP_Integer = true,
        LINK_Main = 2
    })

    InIgnoreHeaderRow = self:AddInput("Ignore Header Row", "IgnoreHeaderRow", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    InData = self:AddInput("Input", "Input", {
        LINKID_DataType = "Text",
        INPID_InputControl = "TextEditControl",
        TEC_Lines = 10,
        LINK_Main = 1
    })

    InDisplayLines = self:AddInput("Display Lines", "DisplayLines", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ScrewControl",
        INP_Integer = true,
        INP_MaxScale = 100,
        INP_MinAllowed = 1,
        INP_Default = 10,
        LINK_Visible = true,
        INP_Passive = true,
        INP_DoNotifyChanged  = true,
    })

    InWrapLines = self:AddInput("Wrap Lines", "WrapLines", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        INP_Passive = true,
        INP_DoNotifyChanged = true
    })

    InShowInput = self:AddInput("Show Input", "ShowInput", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 1.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    OutData = self:AddOutput("Output", "Output", {
        LINKID_DataType = "Text",
        LINK_Main = 1
    })
end

function NotifyChanged(inp, param, time)
    --[[
        Handles all input control events.

        :param inp: Input that triggered a signal.
        :type inp: Input

        :param param: Parameter object holding the (new) value.
        :type param: Parameter

        :param time: Current frame number.
        :type time: float
    ]]
    if inp == InShowInput then
        local visible
        if param.Value == 1.0 then visible = true else visible = false end

        InIndex:SetAttrs({LINK_Visible = visible})
        -- InData:SetAttrs({LINK_Visible = visible})
    elseif inp == InWrapLines then
        local wrap
        if param.Value == 1.0 then wrap = true else wrap = false end
        InData:SetAttrs({TEC_Wrap = wrap})

        -- Toggle the visibility to refresh the inspector view
        InData:SetAttrs({IC_Visible = false})
        InData:SetAttrs({IC_Visible = true})
    elseif inp == InDisplayLines then
        -- Update the TEC Lines value dynamically
        -- Inspired by vNumberXSheet.fuse
        local lines = InDisplayLines:GetSource(time, REQF_SecondaryTime).Value
        InData:SetAttrs({TEC_Lines = lines})
    
        -- Toggle the visibility to refresh the inspector view
        InData:SetAttrs({IC_Visible = false})
        InData:SetAttrs({IC_Visible = true})
    elseif inp == InMode then
        if param.Value <= 1.0 then visible = true else visible = false end

        -- Toggle the index UI control visibility when in grid column/row mode
        InIndex:SetAttrs({IC_Visible = visible})
    end
end


function Process(req)
    -- [[ Creates the output. ]]
    local data = InData:GetValue(req).Value

    local mode = InMode:GetValue(req).Value

    local header = tonumber(InIgnoreHeaderRow:GetValue(req).Value)
    local index = tonumber(InIndex:GetValue(req).Value)

    local pattern = "(.-),"

    local result

    if mode == 0 then
        -- Row
        -- A lazy workaround for greedy Lua pattern matching of the last comma in a CSV line
        local line = tostring(textutils.ReadLineIgnoreComments(data, index + header, "#")) .. ","

        local row = textutils.split(line, pattern)

        -- print("[Data]\n", data)
        -- print("[Index]", index)
        -- print("[Ignore Header Row]", header)
        -- print("[Line]", line)
        -- print("[Split]")
        -- dump(row)

        result = row
    elseif mode == 1 then
        -- Column
        local column = {}
        local currentLine = 0

        for l in string.gmatch(data, "[^\r\n]+") do
            if not string.match(l, "^%s-[#]+") then
                currentLine = currentLine + 1;
                -- print(currentLine, ":", l)

                if currentLine == 1 and header == 1 then
                    -- print("[Skipping Header Row]")
                else
                    -- A lazy workaround for greedy Lua pattern matching of the last comma in a CSV line
                    local line = l .. ","

                    local replaced = textutils.split(line, pattern)
                    table.insert(column, replaced[index]) 
                end
            end
        end

        -- print("[Column Lua Table")
        -- dump(column)

        result = column
    elseif mode == 2 then
        -- Grid Row
        local grid = {}
        local currentLine = 0

        for l in string.gmatch(data, "[^\r\n]+") do
            if not string.match(l, "^%s-[#]+") then
                currentLine = currentLine + 1;

                if currentLine == 1 and header == 1 then
                    -- print("[Skipping Header Row]")
                else
                    -- A lazy workaround for greedy Lua pattern matching of the last comma in a CSV line
                    local line = tostring(l) .. ","

                    local row = textutils.split(line, pattern)

                    -- print("[Data]\n", data)
                    -- print("[Index]", index)
                    -- print("[Ignore Header Row]", header)
                    -- print("[Line]", line)
                    -- print("[Split]")
                    -- dump(row)

                    table.insert(grid, row)
                end
            end
        end

        result = grid
    elseif mode == 3 then
        -- Grid Column
        local grid = {}
        local currentLine = 0

        -- Build the max column count value across the whole file
        local maxColumnCount = 0
        for l in string.gmatch(data, "[^\r\n]+") do
            if not string.match(l, "^%s-[#]+") then
                currentLine = currentLine + 1;

                -- print(currentLine, ":", l)

                if currentLine == 1 and header == 1 then
                    -- print("[Skipping Header Row]")
                else
                    -- A lazy workaround for greedy Lua pattern matching of the last comma in a CSV line
                    local line = l .. ","

                    local replaced = textutils.split(line, pattern)
                    if #replaced > maxColumnCount then
                        maxColumnCount = #replaced
                    end
                end
            end
        end

        -- print("[Max Columns]", maxColumnCount)

        -- Process the data
        currentLine = 0
        for i = 1, maxColumnCount, 1 do
            local column = {}

            for l in string.gmatch(data, "[^\r\n]+") do
                if not string.match(l, "^%s-[#]+") then
                    currentLine = currentLine + 1;

                    -- print(currentLine, ":", l)

                    if currentLine == 1 and header == 1 then
                        -- print("[Skipping Header Row]")
                    else
                        -- A lazy workaround for greedy Lua pattern matching of the last comma in a CSV line
                        local line = l .. ","

                        local replaced = textutils.split(line, pattern)
                        table.insert(column, replaced[i]) 
                    end
                end
            end

            -- print("[Column Lua Table")
            -- dump(column)

            table.insert(grid, column)
        end

        result = grid
    end


    local array = {}

    array["array"] = result
    array["size"] = arrayutils.Length(result)
    local value_str = jsonutils.encode(array)
    -- dump(array)

    local out = Text(value_str)
    OutData:Set(req, out)
end

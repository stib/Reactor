-- ============================================================================
-- modules
-- ============================================================================
local yaml = self and require("yaml") or nil

-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vScriptValFromYAML"
DATATYPE = "Text"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = "ScriptVal",
    -- REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\ScriptVal\\YAML",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Casts YAML text into a ScriptVal object.",
    REGS_OpIconString = FUSE_NAME,
    -- Icon shown in the "Select Tool" dialog and the "Tile Picture"
    REGS_IconID = "Icons.Tools.Icons.RunCommand",
})

function Create()
    -- [[ Creates the user interface. ]]
    InYAML = self:AddInput("Text", "Text", {
        LINKID_DataType = DATATYPE,
        LINK_Main = 1
    })

    InSort = self:AddInput("Sort List", "Sort", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    OutScriptVal = self:AddOutput("ScriptVal", "ScriptVal", {
        LINKID_DataType = "ScriptVal",
        LINK_Main = 1
    })
end

function NotifyChanged(inp, param, time)
    --[[
        Handles all input control events.

        :param inp: Input that triggered a signal.
        :type inp: Input

        :param param: Parameter object holding the (new) value.
        :type param: Parameter

        :param time: Current frame number.
        :type time: float
    ]]
end

function Process(req)
    -- [[ Creates the output. ]]
    local yaml_str = InYAML:GetValue(req).Value
    local sort = InSort:GetValue(req).Value

    local tbl = {}
    if yaml_str ~= "" then
        tbl = yaml.eval(yaml_str)
    else
        error("[YAML] The YAML string was empty.")
        -- print("[YAML] The YAML string was empty.")
    end

    -- Sort the array alphabetically
    if sort == 1.0 then
        table.sort(tbl)
    end

--    print("[ScriptVal Lua Table]")
--    dump(tbl)

    OutScriptVal:Set(req, ScriptValParam(tbl))
end

--[[--
Vonk vTextFromFile DragDrop.fu - v1.2 2022-10-24
By Andrew Hazelden <andrew@andrewhazelden.com>

Overview
----------
The "Vonk vTextFromFile DragDrop.fu" file allows you to import plain-text formatted .txt, .csv (Comma Separated Value), or .tsv (Tab Separated Value) files by dragging them into the Nodes view from a desktop Explorer/Finder/Linux folder browsing window. This is a quick way to bring external data records and spreadsheets into your Resolve/Fusion composite. The DragDrop file supports dragging in multiple text file elements at the same time, and each item will be imported into a separate vTextFromFile node.

Windows URL (.url) files will be drag-drop imported as vTextCreate node based URI addresses.

Common shell scripting file extensions (.exe, .bat, .ps1, .au3, .sh, .command, .scpt, .app) will be drag-drop imported as vTextCreateBrowse node based filepaths.

Usage
Step 1. After you install the "Vonk vTextFromFile DragDrop.fu" package from Reactor, you will need to restart the Fusion program once so the new .fu file is loaded during Fusion's startup phase.

Step 2. Select a plain-text formatted .txt, .csv (Comma Separated Value), .tsv (Tab Separated Value), XML data (.xml, .kml, .gpx), or YAML (.yaml, .yml), or Windows URL (.url) files by dragging them into the Nodes view from a desktop Explorer/Finder/Linux folder browsing window.

Step 3. Drag the plain-text file to the Fusion/Resolve Nodes view. The data will be automatically imported into your foreground composite.


1-Click Installation
---------------------
Install the "Vonk DragDrop | vTextFromFile" atom package via the Reactor package manager.

This will install the "Vonk vTextFromFile DragDrop.fu" file into the "Config:/DragDrop/" PathMap folder (Reactor:/Deploy/Config/DragDrop/).


Fusion Standalone Manual "Config:/" Install:
	On Windows this PathMap works out to:
		%AppData%\Blackmagic Design\Fusion\Config\DragDrop\

	On Linux this PathMap works out to:
		$HOME/.fusion/BlackmagicDesign/Fusion/Config/DragDrop/

	On MacOS this works out to:
		$HOME/Library/Application Support/Blackmagic Design/Fusion/Config/DragDrop/

*Note: In a manual install of this tool you will have to create the final "DragDrop" folder manually as it won't exist in advance.

Resolve Fusion Page Manual "Config:/" Install:
	On Windows this PathMap works out to:
		%AppData%\Blackmagic Design\DaVinci Resolve\Support\Fusion\Config\DragDrop\

	On Linux this PathMap works out to:
		$HOME/.fusion/BlackmagicDesign/DaVinci Resolve/Support/Fusion/Config/DragDrop/

	On MacOS this PathMap works out to:
		$HOME/Library/Application Support/Blackmagic Design/DaVinci Resolve/Fusion/Config/DragDrop/

*Note: In a manual install of this tool you will have to create the final "DragDrop" folder manually as it won't exist in advance.


Todo
------
- Figure out the "args.ShiftModifier" equivalent variable name for detecting hotkeys from .fu events.
- Add bmd.parseFilename() code here to start tokenizing the filename string and frame number element
- Add support for handling IFL filenames in the DragDrop module, and in the vTextFromFile fuse

--]]--

{
	Event{
		Action = 'Drag_Drop',
		Targets = {
			FuView = {
				Execute = _Lua [=[
-- Check if the file extension matches
-- Example: isIFL = MatchExt('/Example.txt', '.txt')
function MatchExt(file, fileType)
	-- Get the file extension
	local ext = string.match(tostring(file), '^.+(%..+)$')

	-- Compare the results
	if (ext ~= nil) and (fileType ~= nil) and (string.lower(ext) == string.lower(tostring(fileType))) then
		return true
	else
		return false
	end
end


-- Get the current comp object
-- Example: comp = GetCompObject()
function GetCompObject()
	local cmp = app:GetAttrs().FUSIONH_CurrentComp
	return cmp
end


-- Grab a .url file's internet shortcut
function find_url(path)
	-- "URL=https://start.duckduckgo.com/"
	searchString = "^URL="
	urlString = ""

	-- Scan through the input textfile line by line
	urlCounter = 0
	lineCounter = 0
	for oneLine in io.lines(path) do
		lineCounter = lineCounter + 1
		-- comp:Print('[Line ' .. lineCounter .. '] ' .. oneLine, '\n')

		-- Check if we have found a match with the searchString
		if oneLine:match(searchString) then
			-- Track the number of URLs found
			urlCounter = urlCounter + 1

			urlString = string.match(oneLine, 'URL=(.*)')
			if urlString ~= nil then
				-- comp:Print('[URL ' .. urlCounter .. '] ' .. urlString, '\n')
				return urlString
			end
		end
	end

	return nil
end


-- Process a file dropped into Fusion
-- Example: ProcessFile('/Example.txt', 1)
function ProcessFile(file, fileNum)
	-- comp:Print('[Vonk][vTextFromFile]['.. fileNum .. '][File Drop] ', file, '\n')

	-- Check if the file extension matches
	if MatchExt(file, '.txt') or MatchExt(file, '.csv') or MatchExt(file, '.tsv') or MatchExt(file, '.xml') or MatchExt(file, '.gpx') or MatchExt(file, '.kml') or MatchExt(file, '.yaml') or MatchExt(file, '.yml') then
		-- Accept the Drag and Drop event
		rets.handled = true

		-- Get the current comp object
		comp = GetCompObject()
		if not comp then
			-- The comp pointer is undefined
			comp:Print('[Vonk][vTextFromFile] Please open a Fusion composite before dragging in a .txt file again.')
			return
		end

		---- Todo: Get the shift hotkey modifier working
		--if args.shiftModifier then
		-- Check for a shift key press to enable the alternative exr import option
		-- ...
		--end

		-- Lock the comp to suppress any file dialog opening for nodes that have empty filename fields.
		-- comp:Print('[Locking Comp]')
		comp:Lock()

		-- Add the vTextFromFile node to the comp
		local read = comp:AddTool('Fuse.vTextFromFile', -32768, -32768)

		-- Todo: Add bmd.parseFilename() code here to start tokenizing the filename string and frame number element

		-- Check for a nil on the node creation and the filename string
		if read and file then
			-- read.Filename = tostring(file)
			read.Input = tostring(file)
		else
			comp:Print('[Vonk][vTextFromFile] Warning: Failed to update the vTextFromFile node filename.')
		end
		
		-- Unlock the comp to restore "normal" file dialog opening operations
		-- comp:Print('[Unlock Comp]')
		comp:Unlock()
	elseif MatchExt(file, '.exe') or MatchExt(file, '.bat') or MatchExt(file, '.ps1') or MatchExt(file, '.au3') or MatchExt(file, '.sh') or MatchExt(file, '.command') or MatchExt(file, '.scpt') or MatchExt(file, '.app') or MatchExt(file, '.app/') then
		-- Accept the Drag and Drop event
		rets.handled = true

		-- Get the current comp object
		comp = GetCompObject()
		if not comp then
			-- The comp pointer is undefined
			comp:Print('[Vonk][vTextCreateBrowse] Please open a Fusion composite before dragging in a .txt file again.')
			return
		end

		-- Lock the comp to suppress any file dialog opening for nodes that have empty filename fields.
		-- comp:Print('[Locking Comp]')
		comp:Lock()
		
		-- Add the vTextCreateBrowse node to the comp
		local read = comp:AddTool('Fuse.vTextCreateBrowse', -32768, -32768)

		-- Check for a nil on the node creation and the string
		if read and file then
			read.Text = tostring(file)
		else
			comp:Print('[Vonk][vTextCreateBrowse] Warning: Failed to update the vTextCreateBrowse node string.')
		end
		
		-- Unlock the comp to restore "normal" file dialog opening operations
		-- comp:Print('[Unlock Comp]')
		comp:Unlock()
	elseif MatchExt(file, '.url') then
		-- Accept the Drag and Drop event
		rets.handled = true

		-- Get the current comp object
		comp = GetCompObject()
		if not comp then
			-- The comp pointer is undefined
			comp:Print('[Vonk][vTextFromFile] Please open a Fusion composite before dragging in a .url file again.')
			return
		end

		url_str = find_url(file)
		
		-- Add the vTextCreate node to the comp
		local read = comp:AddTool('Fuse.vTextCreate', -32768, -32768)

		-- Check for a nil on the node creation and the string
		if read and url_str then
			read.Text = tostring(url_str)
		else
			comp:Print('[Vonk][vTextCreate] Warning: Failed to update the vTextCreate node string.')
		end
		
		-- Unlock the comp to restore "normal" file dialog opening operations
		-- comp:Print('[Unlock Comp]')
		comp:Unlock()
	end
end


-- Where the magic begins
function Main()
	-- Call other chained events and default action
	rets = self:Default(ctx, args)

	-- Debug print the args
	-- dump(args)

	-- Drop zone screen coordinates
	mousex = args._sxpos
	mousey = args._sypos

	-- No one else handled this?
	if not rets.handled then
		-- Get the list of files dropped onto Fusion
		files = args.urilist

		-- Scan through each of the files
		for i, file in ipairs(files) do
			-- Process a .txt file dropped into Fusion
			ProcessFile(file, i)
		end
	end

	-- Debug print where the file was dropped onscreen in the window (using screen coordinates)
	-- print('[Drop Zone Coords] [X] ' .. tostring(mousex) .. ' [Y] ' .. tostring(mousey) .. 'px')
	-- print('\n')
end

-- Run the main function
Main()
]=],
			},
		},
	},
}

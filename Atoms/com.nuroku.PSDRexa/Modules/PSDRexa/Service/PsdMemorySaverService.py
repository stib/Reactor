from Domain.Psd.Psd import Psd


class PsdMemorySaverService:
    psd = None

    @staticmethod
    def set_psd(psd: Psd):
        PsdMemorySaverService.psd = psd

    @staticmethod
    def get_psd() -> Psd:
        if PsdMemorySaverService.psd is None:
            # raise ServiceError("Failed to reload psd")
            return None
        return PsdMemorySaverService.psd

    # NOTE I wonder if I can handle multiple items if I make the psd a list type, I want to be able to do that.

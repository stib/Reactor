-- The code you run is expected to return a Lua string object:
-- return "Hello"
-- or
-- return tbl
-- or
-- return tbl[1]
-- or
-- return tbl[2]
-- or
-- return tbl[3]
-- or
-- return bmd.writestring(tbl)

-- ============================================================================
-- modules
-- ============================================================================
local textutils = self and require("vtextutils") or nil

-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vTextDoString"
DATATYPE = "Text"
MAX_INPUTS = 64

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = DATATYPE,
    REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\Text\\Script",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Return a Text object from running a string of Lua code.",
    REGS_OpIconString = FUSE_NAME,
    -- Should the current time setting be cached?
    REG_TimeVariant = true,
    REG_Unpredictable = true,
    -- Icon shown in the "Select Tool" dialog and the "Tile Picture"
    REGS_IconID = "Icons.Tools.Icons.RunCommand",
})

function Create()
    -- [[ Creates the user interface. ]]
    GetSource = [[comp:SetActiveTool(tool.Header:GetConnectedOutput():GetTool())]]

    InWhich = self:AddInput("Which", "Which", {
        LINKID_DataType = "Number",
        INPID_InputControl = "SliderControl",
        INP_MinAllowed = 1,
        INP_MaxAllowed = MAX_INPUTS,
        INP_MaxScale = 1,
        INP_Integer = true,
        IC_Steps = 1.0,
        IC_Visible = false
    })

    InText1 = self:AddInput("Text1", "Text1", {
        LINKID_DataType = "Text",
        LINK_Main = 1,
        INP_Required = false
    })

    InHeader = self:AddInput("Script Header Wire", "Header", {
        LINKID_DataType = "Text",
        INPID_InputControl = "ImageControl",
        LINK_Visible = false,
        ICD_Width = 0.8,
        -- LINK_Main = 2, -- if set, you lose the ability to instance the imagecontrol
    })

    InSource = self:AddInput(">", "InSource", {
        INPID_InputControl = "ButtonControl",
        INP_External = false,
        IC_Visible = true,
        BTNCS_Execute = GetSource,
        ICD_Width = 0.2,
        INP_Passive = true, -- don't want this to trigger a re-render, so on it goes.
    })

    InUISeparator1 = self:AddInput("UISeparator1", "UISeparator1", {
        IC_Visible = true,
        INPID_InputControl = "SeparatorControl",
        INP_External = false,
    })

    self:AddInput("Lua Script", "LuaScript", {
        LINKID_DataType = "Text",
        INPID_InputControl = "LabelControl",
        INP_External = false,
        INP_Passive = true
    })

    InScript = self:AddInput("Script", "Script" , {
        LINKID_DataType = "Text",
        INPID_InputControl = "TextEditControl",
        TECS_Language = "lua",
        IC_NoLabel = true,
        INPS_DefaultText = "return tbl",
        TEC_Lines = 25,
        -- TEC_FontSize = 18,
        -- TEC_Wrap = true,
        INP_Passive = true,
    })

--    InFontSize = self:AddInput("Font Size", "FontSize", {
--        LINKID_DataType      = "Number",
--        INPID_InputControl   = "MultiButtonControl",
--        INP_Integer          = true,
--        INP_Default          = 3.0,
--        MBTNC_StretchToFit = true,
--        {MBTNC_AddButton     = "12"},
--        {MBTNC_AddButton     = "14"},
--        {MBTNC_AddButton     = "16"},
--        {MBTNC_AddButton     = "18"},
--        {MBTNC_AddButton     = "24"},
--        {MBTNC_AddButton     = "36"},
--        {MBTNC_AddButton     = "48"},
--        {MBTNC_AddButton     = "72"},
--        INP_DoNotifyChanged  = true,
--        INP_Passive = true,
--    })

    InDisplayLines = self:AddInput("Display Lines", "DisplayLines", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ScrewControl",
        INP_Integer = true,
        INP_MaxScale = 100,
        INP_MinAllowed = 1,
        INP_Default = 25,
        INP_Passive = true,
        LINK_Visible = true,
        INP_DoNotifyChanged = true,
    })

    InWrapLines = self:AddInput("Wrap Lines", "WrapLines", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        INP_Passive = true,
        INP_DoNotifyChanged = true
    })

    InUISeparator2 = self:AddInput("UISeparator2", "UISeparator2", {
        IC_Visible = true,
        INPID_InputControl = "SeparatorControl",
        INP_External = false,
    })

    InSeparator = self:AddInput("Return Table Separator", "Separator", {
        LINKID_DataType = "Text",
        INPID_InputControl = "TextEditControl",
        INPS_DefaultText = [[\n]],
        TEC_Lines = 1,
        -- LINK_Main = 2,
        -- INP_Required = false
    })

    self:AddInput("Newline = \\n Return = \\r Tab = \\t", "help1", {
        LINKID_DataType = "Text",
        INPID_InputControl = "LabelControl",
        INP_External = false,
        INP_Passive = true
    })

    InUISeparator3 = self:AddInput("UISeparator3", "UISeparator3", {
        IC_Visible = true,
        INPID_InputControl = "SeparatorControl",
        INP_External = false,
    })

    InShowInput = self:AddInput("Show Input", "ShowInput", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 1.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })


    InShowCode = self:AddInput("Show Code", "ShowCode", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    InShowDump = self:AddInput("Show Dump", "ShowDump", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    InUISeparator4 = self:AddInput("UISeparator4", "UISeparator4", {
        IC_Visible = true,
        INPID_InputControl = "SeparatorControl",
        INP_External = false,
    })

    OutText = self:AddOutput("Output", "Output", {
        LINKID_DataType = "Text",
        LINK_Main = 1
    })
end

function OnAddToFlow()
    --[[ Callback triggered when adding the Fuse to the flow view. ]]
    -- find highest existing input
    local highest_input = 0

    for i = 1, MAX_INPUTS do
        local input = self:FindInput("Text" .. tostring(i))

        if input == nil then
            break
        end

        highest_input = i
    end

    -- add inputs
    -- NOTE: start at 2, inputs 1 always exists
    for i = 2, highest_input do
        self:AddInput("Text" .. i, "Text" .. i, {
            LINKID_DataType = "Text",
            LINK_Main = i,
            INP_Required = false,
            INP_DoNotifyChanged = true
        })
    end

    -- set slider maximum
    InWhich:SetAttrs({INP_MaxScale = highest_input, INP_MaxAllowed = highest_input})
end

function OnConnected(inp, old, new)
    --[[ Callback triggered when connecting a Fuse to the input of this Fuse. ]]
    local inp_nr = tonumber(string.match(inp:GetAttr("LINKS_Name"), "Text(%d+)"))
    local max_nr = tonumber(InWhich:GetAttr("INP_MaxAllowed"))

    if inp_nr then
        -- add input if maximum inputs is not exceeded and connection is not empty
        if inp_nr >= max_nr and max_nr < MAX_INPUTS and new ~= nil then
            -- set slider maximum
            InWhich:SetAttrs({INP_MaxScale = inp_nr, INP_MaxAllowed = inp_nr})

            -- add extra input
            self:AddInput("Text" .. (inp_nr + 1), "Text" .. (inp_nr + 1), {
                LINKID_DataType = "Text",
                LINK_Main = (inp_nr + 1),
                INP_Required = false,
                INP_DoNotifyChanged = true
            })
        end
    end
end


function NotifyChanged(inp, param, time)
    --[[
        Handles all input control events.

        :param inp: Input that triggered a signal.
        :type inp: Input

        :param param: Parameter object holding the (new) value.
        :type param: Parameter

        :param time: Current frame number.
        :type time: float
    ]]
    if inp == InShowInput then
        local visible
        if param.Value == 1.0 then visible = true else visible = false end

        InScript:SetAttrs({LINK_Visible = visible})
    elseif inp == InWrapLines then
        local wrap
        if param.Value == 1.0 then wrap = true else wrap = false end
        InScript:SetAttrs({TEC_Wrap = wrap})

        -- Toggle the visibility to refresh the inspector view
        InScript:SetAttrs({IC_Visible = false})
        InScript:SetAttrs({IC_Visible = true})
    elseif inp == InDisplayLines then
        -- Update the TEC Lines value dynamically
        -- Inspired by vNumberXSheet.fuse
        local lines = InDisplayLines:GetSource(time, REQF_SecondaryTime).Value
        InScript:SetAttrs({TEC_Lines = lines})

        -- Toggle the visibility to refresh the inspector view
        InScript:SetAttrs({IC_Visible = false})
        InScript:SetAttrs({IC_Visible = true})
--    elseif inp == InFontSize then
--        -- Change the TextEditControl font size in points
--        local fontSizeTbl = {12, 14, 16, 18, 24, 36, 48, 72}
--        local fontSizeSelect = fontSizeTbl[param.Value + 1]
--        InScript:SetAttrs({TEC_FontSize = fontSizeSelect})
--        -- print("[Font Size Pt]", fontSizeSelect)
--
--        -- Toggle the visibility to refresh the inspector view
--        InScript:SetAttrs({IC_Visible = false})
--        InScript:SetAttrs({IC_Visible = true})
    end
end

function Process(req)
    -- [[ Creates the output. ]]
    -- local separator = InSeparator:GetValue(req).Value or ""
    local input_count = tonumber(InWhich:GetAttr("INP_MaxAllowed"))

    -- Replace newlines and tabs: https://stackoverflow.com/questions/57099292/replace-n-string-with-real-n-in-lua
    local seperator = InSeparator:GetValue(req).Value:gsub("\\([ntr])", {n = "\n", t = "\t", r = "\r"})

    local values = {}

    for i = 1, input_count do
        -- get input from index
        local input = self:FindInput("Text" .. tostring(i))

        -- get text from input
        if input ~= nil and input:GetSource(req.Time, req:GetFlags()) then
            local inp_text = input:GetSource(req.Time, req:GetFlags()).Value

            if inp_text == nil then
                inp_text = ""
            end

            table.insert(values, inp_text)
        end
    end

    local show_code = InShowCode:GetValue(req).Value
    local show_dump = InShowDump:GetValue(req).Value

    local txt_str = InScript:GetValue(req).Value or ""
    local header_str = InHeader:GetValue(req).Value or ""

    -- Generate the block of Lua code to run
    local tbl_str = "local tbl = " .. bmd.writestring(values or {}) .. "\n"
    local script_str = header_str .. "\n" .. tbl_str .. "\n" .. txt_str
    local tbl = dostring(script_str) or {}

    if show_dump == 1 or show_code == 1 then
        print("\n----------------------")
        print("[" .. tostring(self.Name) .. "]")
    end

    if show_code == 1 then
        print("\n[Lua Script]")
        print(script_str)
    end

    if show_dump == 1 then
        print("\n[ScriptVal Return Type]")
        dump(type(tbl))

        print("\n[ScriptVal Lua Table]")
        dump(tbl)
    end

    local text = ""
    if type(tbl) == "table" then
        for key, value in pairs(tbl) do
            if type(value) == "string" or type(value) == "number" then
                if key == 1 then
                    text = tostring(value)
                else
                    text = text .. seperator .. tostring(value)
                end
            end
        end
    elseif type(tbl) == "string" then
        text = tostring(tbl)
    elseif type(tbl) == "number" then
        text = tostring(tbl)
    end

    local out = Text(text)
    OutText:Set(req, out)
end

-- Use the function 'OutText("Hello World")' to return a value from the executed script.
-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vTextExecute"
DATATYPE = "Text"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_SinkTool, {
    REGID_DataType = DATATYPE,
    REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\Text\\Script",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Execute code sourced from a Fusion text object.",
    REGS_OpIconString = FUSE_NAME,
    -- Should the current time setting be cached?
    REG_TimeVariant = true,
    REG_Unpredictable = true,
    -- Icon shown in the "Select Tool" dialog and the "Tile Picture"
    REGS_IconID = "Icons.Tools.Icons.RunCommand",
})

function Create()
    -- [[ Creates the user interface. ]]

    InScriptLanguage = self:AddInput('Script Language', 'ScriptLanguage',{
        LINKID_DataType = 'Number',
        INPID_InputControl = 'ComboControl',
        INP_Default = 0,
        INP_Integer = true,
        ICD_Width = 1,
        { CCS_AddString = 'Lua'},
        { CCS_AddString = 'Python'},
        { CCS_AddString = 'Python 2'},
        { CCS_AddString = 'Python 3'},
        CC_LabelPosition = 'Vertical',
    })

    InText = self:AddInput("Input" , "Input" , {
        LINKID_DataType = "Text",
        INPID_InputControl = "TextEditControl",
        IC_NoLabel = true,
        TEC_Lines = 25,
        -- TEC_Wrap = true,
        INP_Passive = true,
        LINK_Main = 1
    })

    InDisplayLines = self:AddInput("Display Lines", "DisplayLines", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ScrewControl",
        INP_Integer = true,
        INP_MaxScale = 100,
        INP_MinAllowed = 1,
        INP_Default = 25,
        INP_Passive = true,
        LINK_Visible = true,
        INP_DoNotifyChanged  = true,
    })

    InWrapLines = self:AddInput("Wrap Lines", "WrapLines", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        INP_Passive = true,
        INP_DoNotifyChanged = true
    })

    OutText = self:AddOutput("Output" , "Output" , {
        LINKID_DataType = "Text",
        LINK_Main = 1
    })
end

function NotifyChanged(inp, param, time)
    --[[
        Handles all input control events.

        :param inp: Input that triggered a signal.
        :type inp: Input

        :param param: Parameter object holding the (new) value.
        :type param: Parameter

        :param time: Current frame number.
        :type time: float
    ]]
    if inp == InWrapLines then
        local wrap
        if param.Value == 1.0 then wrap = true else wrap = false end
        InText:SetAttrs({TEC_Wrap = wrap})

        -- Toggle the visibility to refresh the inspector view
        InText:SetAttrs({IC_Visible = false})
        InText:SetAttrs({IC_Visible = true})
    elseif inp == InDisplayLines then
        -- Update the TEC Lines value dynamically
        -- Inspired by vNumberXSheet.fuse
        local lines = InDisplayLines:GetSource(time, REQF_SecondaryTime).Value
        InText:SetAttrs({TEC_Lines = lines})

        -- Toggle the visibility to refresh the inspector view
        InText:SetAttrs({IC_Visible = false})
        InText:SetAttrs({IC_Visible = true})
    end
end

function Process(req)
    -- [[ Creates the output. ]]
    local txt_str = InText:GetValue(req).Value or ""
    local lang = InScriptLanguage:GetValue(req).Value

    local lang_str = ""
    local prefix = ""
    local out = ""

    -- Log file creation
    local nodeName = self.Name
    local folder = self.Comp:MapPath('Temp:/Vonk/')
    local log_path = self.Comp:MapPath(folder .. tostring(nodeName)  .. '_' .. tostring(bmd.createuuid()) .. '.log')
    if bmd.fileexists(folder) == false then
        bmd.createdir(folder)
    end

    -- Language Customization
    if lang == 0 then
        lang_str = ""

        prefix = [[

function OutText(text)
    log_fp, err = io.open([=[]] .. log_path .. [[]=], "wb")
    if log_fp then
        log_fp:write(text)
        log_fp:close()
    end
end

]]
    elseif lang == 1 then
        lang_str = "!Py: "

        prefix = [[

def OutText(text):
	with open(r"]] .. log_path .. [[", "w", encoding="utf-8") as f:
		f.write(text)

]]
    elseif lang == 2 then
        lang_str = "!Py2: "

        prefix = [[

def OutText(text):
	with open(r"]] .. log_path .. [[", "w", encoding="utf-8") as f:
		f.write(text)

]]
    elseif lang == 3 then
        lang_str = "!Py3: "

        prefix = [[

def OutText(text):
	with open(r"]] .. log_path .. [[", "w", encoding="utf-8") as f:
		f.write(text)

]]
    end

    -- Run the script
    local script_str = lang_str .. prefix .. txt_str
    -- dump(script_str)
    self.Comp:Execute(script_str)

    out = log_path

    OutText:Set(req, Text(out))
end

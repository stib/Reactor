-- ============================================================================
-- modules
-- ============================================================================
local textutils = self and require("vtextutils") or nil

-- ============================================================================
-- constants
-- ============================================================================
-- FUSE_NAME = "vTextFromFilename"
FUSE_NAME = "vTextParseFilenameOutputs"
DATATYPE = "Text"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = DATATYPE,
    REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\Text\\Utility",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Creates a Fusion Text object by parsing a filepath.",
    REGS_OpIconString = FUSE_NAME,
    -- Icon shown in the "Select Tool" dialog and the "Tile Picture"
    REGS_IconID = "Icons.Tools.Icons.RunCommand",
})

function Create()
    -- [[ Creates the user interface. ]]
    InText = self:AddInput("Text" , "Text" , {
        LINKID_DataType = "Text",
        INPID_InputControl = "TextEditControl",
        TEC_Lines = 1,
        LINK_Main = 1,
    })

--    InViewFile = self:AddInput('View File', 'View File', {
--        LINKID_DataType = 'Number',
--        INPID_InputControl = 'ButtonControl',
--        INP_DoNotifyChanged = true,
--        INP_External = false,
--        ICD_Width = 1,
--        INP_Passive = true,
--        IC_Visible = true,
--        BTNCS_Execute = [[
---- check if a tool is selected
--local selectedNode = tool or comp.ActiveTool
--if selectedNode then
--    local filename = comp:MapPath(selectedNode:GetInput('Text'))
--    if filename then
--        if bmd.fileexists(filename) then
--            bmd.openfileexternal('Open', filename)
--        else
--            print('[View File] File does not exist yet:', filename)
--        end
--    else
--        print('[View File] Filename is nil. Possibly the text is sourced from an external text input connection.')
--    end
--else
--    print('[View File] Please select the node.')
--end
--        ]],
--    })

    InShowInput = self:AddInput("Show Input", "ShowInput", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 1.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    OutTextFullPath = self:AddOutput("FullPath", "FullPath", {
        LINKID_DataType = "Text",
        LINK_Main = 1
    })

    OutTextFullPathMap = self:AddOutput("FullPathMap", "FullPathMap", {
        LINKID_DataType = "Text",
        LINK_Main = 2
    })

    OutTextPath = self:AddOutput("Path", "Path", {
        LINKID_DataType = "Text",
        LINK_Main = 3
    })

    OutTextPathMap = self:AddOutput("PathMap", "PathMap", {
        LINKID_DataType = "Text",
        LINK_Main = 4
    })

    OutTextFullName = self:AddOutput("FullName", "FullName", {
        LINKID_DataType = "Text",
        LINK_Main = 5
    })

    OutTextName = self:AddOutput("Name", "Name", {
        LINKID_DataType = "Text",
        LINK_Main = 6
    })

    OutTextCleanName = self:AddOutput("CleanName", "CleanName", {
        LINKID_DataType = "Text",
        LINK_Main = 7
    })

    OutTextSNum = self:AddOutput("SNum", "SNum", {
        LINKID_DataType = "Text",
        LINK_Main = 8
    })

    OutTextNumber = self:AddOutput("Number", "Number", {
        LINKID_DataType = "Text",
        LINK_Main = 9
    })

    OutTextExtension = self:AddOutput("Extension", "Extension", {
        LINKID_DataType = "Text",
        LINK_Main = 10
    })

    OutTextPadding = self:AddOutput("Padding", "Padding", {
        LINKID_DataType = "Text",
        LINK_Main = 11
    })

    OutTextUNC = self:AddOutput("UNC", "UNC", {
        LINKID_DataType = "Text",
        LINK_Main = 12
    })
end

function NotifyChanged(inp, param, time)
    --[[
        Handles all input control events.

        :param inp: Input that triggered a signal.
        :type inp: Input

        :param param: Parameter object holding the (new) value.
        :type param: Parameter

        :param time: Current frame number.
        :type time: float
    ]]
    if inp == InShowInput then
        local visible
        if param.Value == 1.0 then visible = true else visible = false end
        InText:SetAttrs({LINK_Visible = visible})
    end
end

function Process(req)
    -- [[ Creates the output. ]]
    local str = InText:GetValue(req).Value
    local tbl = textutils.parse_filename(str)

--    dump("FullPath:", tbl.FullPath)
--    dump("FullPathMap:", tbl.FullPathMap)
--    dump("Path:", tbl.Path)
--    dump("PathMap:", tbl.PathMap)
--    dump("FullName:", tbl.FullName)
--    dump("Name:", tbl.Name)
--    dump("CleanName:", tbl.CleanName)
--    dump("SNum:", tbl.SNum)
--    dump("Number:", tbl.Number)
--    dump("Extension:", tbl.Extension)
--    dump("Padding:", tbl.Padding)
--    dump("UNC:", tbl.UNC)

    OutTextFullPath:Set(req, Text(tbl.FullPath or ""))
    OutTextFullPathMap:Set(req, Text(tbl.FullPathMap or ""))
    OutTextPath:Set(req, Text(tbl.Path or ""))
    OutTextPathMap:Set(req, Text(tbl.PathMap or ""))
    OutTextFullName:Set(req, Text(tbl.FullName or ""))
    OutTextName:Set(req, Text(tbl.Name or ""))
    OutTextCleanName:Set(req, Text(tbl.CleanName or ""))
    OutTextSNum:Set(req, Text(tbl.SNum))
    OutTextNumber:Set(req, Number(tbl.Number))
    OutTextExtension:Set(req, Text(tbl.Extension))
    OutTextPadding:Set(req, Text(tbl.Padding or 0))
    OutTextUNC:Set(req, Number(tbl.UNC))
end

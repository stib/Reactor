-- ============================================================================
-- modules
-- ============================================================================
local json = self and require("dkjson") or nil

-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "ocaText"
DATATYPE = "Text"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = "ScriptVal",
    REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\OCA\\Create",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Create an OCA object from JSON text.",
    REGS_OpIconString = FUSE_NAME,
    REGS_IconID = "Icons.Tools.Icons.TextPlus"
})

function Create()
    -- [[ Creates the user interface. ]]
    InJSON = self:AddInput("JSON" , "JSON" , {
        LINKID_DataType = "Text",
        INPID_InputControl = "TextEditControl",
        IC_NoLabel = true,
        TECS_Language = "lua",
        TEC_Lines = 15
    })

    InDisplayLines = self:AddInput("Display Lines", "DisplayLines", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ScrewControl",
        INP_Integer = true,
        INP_MaxScale = 100,
        INP_MinAllowed = 1,
        INP_Default = 15,
        LINK_Visible = true,
        INP_Passive = true,
        INP_DoNotifyChanged  = true,
    })

    InWrapLines = self:AddInput("Wrap Lines", "WrapLines", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 1.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    OutScriptVal = self:AddOutput("Output", "Output", {
        LINKID_DataType = "ScriptVal",
        LINK_Main = 1
    })
end

function NotifyChanged(inp, param, time)
    --[[
        Handles all input control events.

        :param inp: Input that triggered a signal.
        :type inp: Input

        :param param: Parameter object holding the (new) value.
        :type param: Parameter

        :param time: Current frame number.
        :type time: float
    ]]
    if inp == InWrapLines then
        local wrap
        if param.Value == 1.0 then wrap = true else wrap = false end
        InJSON:SetAttrs({TEC_Wrap = wrap})

        -- Toggle the visibility to refresh the inspector view
        InJSON:SetAttrs({IC_Visible = false})
        InJSON:SetAttrs({IC_Visible = true})
    elseif inp == InDisplayLines then
        -- Update the TEC Lines value dynamically
        -- Inspired by vNumberXSheet.fuse
        local lines = InDisplayLines:GetSource(time, REQF_SecondaryTime).Value
        InJSON:SetAttrs({TEC_Lines = lines})
    
        -- Toggle the visibility to refresh the inspector view
        InJSON:SetAttrs({IC_Visible = false})
        InJSON:SetAttrs({IC_Visible = true})
    end
end

function Process(req)
    -- [[ Creates the output. ]]
    local json_str = InJSON:GetValue(req).Value
    local tbl = json.decode(json_str)

--    print("[ScriptVal Lua Table]")
--    dump(tbl)

    OutScriptVal:Set(req, ScriptValParam(tbl))
end

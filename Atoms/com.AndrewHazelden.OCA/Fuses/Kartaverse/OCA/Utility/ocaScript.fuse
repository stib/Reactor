--[[--
Usage Tip: The Lua code you run is expected to return a Lua table structure:

return tbl[1]

or

return tbl

or

return {}

--]]--

-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "ocaScript"
DATATYPE = "ScriptVal"
MAX_INPUTS = 64

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_SinkTool, {
    REGID_DataType = DATATYPE,
    REGID_InputDataType = "Text",
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\OCA\\Utility",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Use a Lua script to batch edit the contents of an OCA datastream (Fusion ScriptVal object).",
    REGS_OpIconString = FUSE_NAME,
    -- Should the current time setting be cached?
    REG_TimeVariant = true,
    REG_Unpredictable = true,
    -- Icon shown in the "Select Tool" dialog and the "Tile Picture"
    REGS_IconID = "Icons.Tools.Icons.RunCommand",
})

function Create()
    -- [[ Creates the user interface. ]]
    GetSource = [[comp:SetActiveTool(tool.Header:GetConnectedOutput():GetTool())]]

    InWhich = self:AddInput("Which", "Which", {
        LINKID_DataType = "Number",
        INPID_InputControl = "SliderControl",
        INP_MinAllowed = 1,
        INP_MaxAllowed = MAX_INPUTS,
        INP_MaxScale = 1,
        INP_Integer = true,
        IC_Steps = 1.0,
        IC_Visible = false
    })

   InHeader = self:AddInput("Script Header Wire", "Header",{
        LINKID_DataType = "Text",
        INPID_InputControl = "ImageControl",
        LINK_Visible = false,
        ICD_Width = 0.8,
        LINK_Main = 1, -- if set, you lose the ability to instance the imagecontrol
    })

    InSource = self:AddInput(">", "InSource", {
        INPID_InputControl = "ButtonControl",
        INP_External = false,
        IC_Visible = true,
        BTNCS_Execute = GetSource,
        ICD_Width = 0.2,
        INP_Passive = true, -- don't want this to trigger a re-render, so on it goes.
    })

    InSeparator = self:AddInput("UISeparator1", "UISeparator1", {
        IC_Visible = true,
        INPID_InputControl = "SeparatorControl",
        INP_External = false,
    })

    InText = self:AddInput("Text" , "Text" , {
        LINKID_DataType = "Text",
        INPID_InputControl = "TextEditControl",
        IC_NoLabel = true,
        TECS_Language = "lua",
        INPS_DefaultText = "return tbl[1]",
        TEC_Lines = 15,
        -- TEC_FontSize = 18,
        -- TEC_Wrap = true,
        INP_Passive = true,
        LINK_Main = 2
    })

    InDisplayLines = self:AddInput("Display Lines", "DisplayLines", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ScrewControl",
        INP_Integer = true,
        INP_MaxScale = 100,
        INP_MinAllowed = 1,
        INP_Default = 15,
        INP_Passive = true,
        LINK_Visible = true,
        INP_DoNotifyChanged  = true,
    })

    InWrapLines = self:AddInput("Wrap Lines", "WrapLines", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        INP_Passive = true,
        INP_DoNotifyChanged = true
    })

    InSeparator2 = self:AddInput("UISeparator2", "UISeparator2", {
        IC_Visible = true,
        INPID_InputControl = "SeparatorControl",
        INP_External = false,
    })

    InShowInput = self:AddInput("Show Input", "ShowInput", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    InShowCode = self:AddInput("Show Code", "ShowCode", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    InShowDump = self:AddInput("Show Dump", "ShowDump", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    InSeparator3 = self:AddInput("UISeparator3", "UISeparator3", {
        IC_Visible = true,
        INPID_InputControl = "SeparatorControl",
        INP_External = false,
    })

    InScriptVal1 = self:AddInput("ScriptVal1", "ScriptVal1", {
        INPID_InputControl = "ImageControl",
        LINKID_DataType = DATATYPE,
        INP_Required = false,
        INP_External = true,
        LINK_Main = 3,
    })

    OutScriptVal = self:AddOutput("Output" , "Output" , {
        LINKID_DataType = "ScriptVal",
        LINK_Main = 1
    })
end

function OnAddToFlow()
    --[[ Callback triggered when adding the Fuse to the flow view. ]]
    -- find highest existing input
    local highest_input = 1

    for i = 1, MAX_INPUTS do
        local input = self:FindInput("ScriptVal" .. tostring(i))

        if input == nil then
            break
        end

        highest_input = i
    end

    -- add inputs
    -- NOTE: start at 2, input 1 always exists
    for i = 2, highest_input do
        self:AddInput("ScriptVal" .. i, "ScriptVal" .. i, {
            LINKID_DataType = "ScriptVal",
            INPID_InputControl = "ImageControl",
            LINK_Main = i + 2,
            INP_Required = false,
            INP_DoNotifyChanged = true
        })
    end

    -- set slider maximum
    InWhich:SetAttrs({INP_MaxScale = highest_input, INP_MaxAllowed = highest_input})
end

function OnConnected(inp, old, new)
    --[[ Callback triggered when connecting a Fuse to the input of this Fuse. ]]
    local inp_nr = tonumber(string.match(inp:GetAttr("LINKS_Name"), "ScriptVal(%d+)"))
    local max_nr = tonumber(InWhich:GetAttr("INP_MaxAllowed"))

    if inp_nr then
        -- add input if maximum inputs is not exceeded and connection is not empty
        if inp_nr >= max_nr and max_nr < MAX_INPUTS and new ~= nil then
            -- set slider maximum
            InWhich:SetAttrs({INP_MaxScale = inp_nr, INP_MaxAllowed = inp_nr})

            -- add extra input
            self:AddInput("ScriptVal" .. (inp_nr + 1), "ScriptVal" .. (inp_nr + 1), {
                LINKID_DataType = "ScriptVal",
                INPID_InputControl = "ImageControl",
                LINK_Main = (inp_nr + 3),
                INP_Required = false,
                INP_External = true,
                INP_DoNotifyChanged = true
            })
        end
    end
end

function NotifyChanged(inp, param, time)
    --[[
        Handles all input control events.

        :param inp: Input that triggered a signal.
        :type inp: Input

        :param param: Parameter object holding the (new) value.
        :type param: Parameter

        :param time: Current frame number.
        :type time: float
    ]]
    if inp == InShowInput then
        local visible
        if param.Value == 1.0 then visible = true else visible = false end

        InText:SetAttrs({LINK_Visible = visible})
        InHeader:SetAttrs({LINK_Visible = visible})
    elseif inp == InWrapLines then
        local wrap
        if param.Value == 1.0 then wrap = true else wrap = false end
        InText:SetAttrs({TEC_Wrap = wrap})

        -- Toggle the visibility to refresh the inspector view
        InText:SetAttrs({IC_Visible = false})
        InText:SetAttrs({IC_Visible = true})
    elseif inp == InDisplayLines then
        -- Update the TEC Lines value dynamically
        -- Inspired by vNumberXSheet.fuse
        local lines = InDisplayLines:GetSource(time, REQF_SecondaryTime).Value
        InText:SetAttrs({TEC_Lines = lines})

        -- Toggle the visibility to refresh the inspector view
        InText:SetAttrs({IC_Visible = false})
        InText:SetAttrs({IC_Visible = true})
    end
end

function Process(req)
    -- [[ Creates the output. ]]
    local header_str = InHeader:GetValue(req).Value or ""
    local show_code = InShowCode:GetValue(req).Value
    local show_dump = InShowDump:GetValue(req).Value
    
    local input_count = tonumber(InWhich:GetAttr("INP_MaxAllowed"))
    local values_tbl = {}

    for i = 1, input_count do
        -- get input from index
        local input = self:FindInput("ScriptVal" .. tostring(i))

        -- get table from input
        if input ~= nil then
            if input:GetSource(req.Time, req:GetFlags()) ~= nil and input:GetSource(req.Time, req:GetFlags()):GetValue() ~= nil then
                local inp_tbl = input:GetSource(req.Time, req:GetFlags()):GetValue() or {}
                if inp_tbl ~= nil then
                    table.insert(values_tbl, inp_tbl)
                end
            end
        end
    end

    local tbl_str_in = "local tbl = " .. bmd.writestring(values_tbl or {}) .. "\n"
    local txt_str = InText:GetValue(req).Value or ""

    local script_str = header_str .. "\n" .. tbl_str_in .. txt_str
    local tbl = dostring(script_str) or {}

    if show_dump == 1 or show_code == 1 then
        print("\n----------------------")
        print("[" .. tostring(self.Name) .. "]")
    end
    if show_code == 1  then
        print("\n[Lua Script]")
        print(script_str)
    end
    if show_dump == 1 then
        print("\n[ScriptVal Lua Table]")
        dump(tbl)
    end

    OutScriptVal:Set(req, ScriptValParam(tbl))
end
